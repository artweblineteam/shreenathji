<?php
/*
Template Name: page-13 (library)
*/


 get_header(); ?>

<div class="library banner-inner">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Библиотека</span></div>
            <div class="title__subtext">Книги, аудио, видео по ведической астрологии <br>Джйотиш, Аюрведе, Йоге и Васту</div>
     </div>
    <div class="library-description"><span>Представлены фундаментальные Ведические писания и научно-популярные работы
    по Астрологии Джйотиш, Йоге, Васту и Аюрведе. Библиотека разделена на четыре группы
    в соответствии с программой четырёх курсов академии.</span></div>
    <div class="library-description"><span>Мы рекомендуем изучить все представленные материалы перед обучением
    на соответствующем курсе академии. Для студентов, прошедших курс, представлены
    аудиозаписи соответствующей программы.</span></div>
    </div>
  </div>
</div>

<div class="lib-section">
  <div class="container">
    <div class="lib-section__list grid grig-top">
      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
        <div class="lib-section__icon"><img src="<?php bloginfo("template_directory"); ?>/assets/img/lib_icon_list.png);"></div>
        <div class="lib-section__name">Литература</div>
      </div>
      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
        <div class="lib-section__icon"><img src="<?php bloginfo("template_directory"); ?>/assets/img/lib_icon_sound.png);"></div>
        <div class="lib-section__name">Аудио</div>
      </div>
      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
        <div class="lib-section__icon"><img src="<?php bloginfo("template_directory"); ?>/assets/img/lib_icon_video.png);"></div>
        <div class="lib-section__name">Видео</div>
      </div>
      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
        <div class="lib-section__icon">Только для студентов</div>
        <div class="lib-section__name">Спец-материалы</div>
      </div>
    </div>
  </div>
</div>

<div class="lib-course lib-course-first">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__subtext title__light--pink title-24">материалы для подготовки</div>
            <div class="title__subtext with-border with-border__long title__light--pink title-24"><span>к I курсу</span></div>
          </div>
          <div class="lib-course__list grid grig-top">
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Величие Сатурна</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Р. Свобода. Книга, наделяющая <br>новым образным мышлением</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Robert-Svoboda-Velichie-Saturna-2003.doc" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Махабхарата</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Вьяса <br>Исторический труд</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Рамаяна</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Валмики <br>Исторический труд</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Ramayana.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title sound">
                      <span>Бхагават Гита</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Аудио. «Песнь Бога», самое полное духовное писание всех времён. 3 перевода. </span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title movie">
                      <span>Махабхарата</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Видео-сериал. Экранизация. <br>Суть Дхармы</span>
                    </div>
                </div>
                <div class="lib-course__link no">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title special">
                      <span>Специальные материалы</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Только для студентов</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
          </div>
          <div class="lib-course__recomend">
            <h3 class="title__light title__light--pink">Рекомендации</h3>
            <span>Благоприятно:</span>
            <ul>
              <li>Обстоятельно ответить на вопрос:<br> «Что такое счастье для Вас лично?»</li>
              <li>Засыпать до 22:00, вставать к 5:00;</li>
              <li>По субботам анализировать свой путь Дхармы;</li>
              <li>Каждое воскресенье планировать неделю;</li>
              <li>Каждое утро планировать день</li>
            </ul>
          </div>
          
  </div>
</div>

<div class="lib-course lib-course-second">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__subtext title__light--pink title-24">материалы для подготовки</div>
            <div class="title__subtext with-border with-border__long title__light--pink title-24"><span>к II курсу</span></div>
          </div>
          <div class="lib-course__list grid grig-top">
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Астрология <br>Вселенских мудрецов</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Комментарии к древнейшему астро-трактату «Бхригу—сутра»</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Astrologiya-vselenskih-mudrecov.docx" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Ведическая Астрология</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Для начинающих астрологов. <br>Содержит знание о Накшатрах</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Vedicheskaya-astrologiya-2002.doc" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Планеты и дети</span>
                    </div>
                    <div class="lib-course__info">
                      <span>К. Н Рао <br>Популярная литература</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Rao-K.-Planety-i-Deti.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Брак в современной Индии</span>
                    </div>
                    <div class="lib-course__info">
                      <span>К. Н Рао <br>Популярная литература</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Brak-v-sovremennoj-Indii.-Astrologicheskij-analiz-2010.-Dipak-B.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Взлеты и падения в карьере</span>
                    </div>
                    <div class="lib-course__info">
                      <span>К. Н Рао <br>Популярная литература</span>
                    </div>
                </div>
                <div class="lib-course__link">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Vzlety-i-padeniya-v-karere.-K.-Rao.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title sound">
                      <span>Сурийа Намаскар. 12 имен</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Звуковой файл для выполнения <br>практики поклонения Солнцу</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Surija-Namaskar-12-imen-Solnca-Ratan-Mohan-Sharma.mp3" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title sound">
                      <span>Йога Сутры. Часть 1</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Патанджали Основа Йоги</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Joga-sutra.-Patanzhali-gl.-1.mp3" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title movie">
                      <span>КДМД</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Видео-сериал <br>Суть отношений мужа и жён</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://indiakino.net/3593-bog-bogov-mahadev.html" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title special">
                      <span>Специальные материалы</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Только для студентов</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="http://vedicschool.net/wp-content/uploads/2020/11/Spec-materialy-dlya-studentov-II-kursa.7z" class="yes"></a>
                </div>
              </div>
            </div>
          </div>
          <div class="lib-course__recomend">
            <h3 class="title__light title__light--pink">Рекомендации</h3>
            <span>Благоприятно:</span>
            <ul>
              <li>Засыпать до 22:00, вставать к 5:00, практиковать;</li>
              <li>По субботам анализировать свой путь Дхармы;</li>
              <li>Каждое воскресенье планировать неделю;</li>
              <li>Каждое утро планировать день;</li>
              <li>Регулярно, до и после деятельности, делать «Паузу»</li>
            </ul>
          </div>
          
  </div>
</div>

<div class="lib-course lib-course-third">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__subtext title__light--pink title-24">материалы для подготовки</div>
            <div class="title__subtext with-border with-border__long title__light--pink title-24"><span>к III курсу</span></div>
          </div>
          <div class="lib-course__list grid grig-top">
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Сатурн – путь <br>к системному росту</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Парвати Кумар</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="http://vedicschool.net/wp-content/uploads/2020/11/Saturn-put-k-sistemnomu-rostu.-P.-Kumar.7z" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Предсказание <br>по Чара Даша Джаимини</span>
                    </div>
                    <div class="lib-course__info">
                      <span>К. Рао</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Predskazaniya-po-Chara-Dasha-Dzhajmini.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Аюрведа, Йога и Астрология</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Д. Фроули</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Ajurvedicheskaya-astrologiya.-D.-Frouli.doc" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Аюрведическая терапия</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Д. Фроули</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/David-Frouli-Ajurvedicheskaya-terapiya.doc" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Основы Васту</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Т. Кришна</span>
                    </div>
                </div>
                <div class="lib-course__link">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Filosovskaya-kniga-Vastu.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Сурийа Сиддханта</span>
                    </div>
                    <div class="lib-course__info">
                      <span></span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Suriya-Siddhanta-i-sovremennaya-astrologiya.-Tomson.doc" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>БПХШ. Часть 1</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Парашара. Основа Джйотиш. Суть</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Brihat-Parashara-Hora-Shastra-T1.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Веданта</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Веданта</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Vedanta-sutra.-Vyasa.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title sound">
                      <span>Йога Сутры. Части 2, 3</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Аудио <br>Патанджали Основа Йоги</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
             <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title movie">
                      <span>Шани Дев</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Видео-сериал <br>Отношения планет</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://indiakino.net/3862-shani-djev.html" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title special">
                      <span>Специальные материалы</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Только для студентов</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
          </div>
          <div class="lib-course__recomend">
            <h3 class="title__light title__light--pink">Рекомендации</h3>
            <span>Благоприятно:</span>
            <ul>
              <li>Засыпать до 22:00, вставать к 5:00, практиковать;</li>
              <li>По субботам анализировать свой путь Дхармы;</li>
              <li>Каждое воскресенье планировать неделю;</li>
              <li>Каждое утро планировать день;</li>
              <li>Регулярно, до и после деятельности, делать «Паузу»;</li>
              <li>Регулярно, до и после дел, выполнять «Осознанность»</li>
            </ul>
          </div>
          
  </div>
</div>

<div class="lib-course lib-course-fourth">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__subtext title__light--pink title-24">материалы для подготовки</div>
            <div class="title__subtext with-border with-border__long title__light--pink title-24"><span>к IV курсу</span></div>
          </div>
          <div class="lib-course__list grid grig-top">
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Трудности <br>Ведической астрологии</span>
                    </div>
                    <div class="lib-course__info">
                      <span>С. Ратх</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Trudnosti-vedicheskoj-astrologii.-S.-Rath.doc" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Мухурта – <br>астрология выбора</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Б. В. Раман, пер. Теплицын</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Muhurta.-B-Raman.doc" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Аштанга Хридая Самхита</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Шримад Вагбхата. Основа Аюрведы</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Ashtanga-Hridaya-Samhita.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Ведическая Архитектура</span>
                    </div>
                    <div class="lib-course__info">
                      <span>С. Балута</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>БПХШ. Часть 2</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Парашара. Предсказания в Джйотиш</span>
                    </div>
                </div>
                <div class="lib-course__link">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Brihat-Parashara-Hora-Shastra-T2.pdf" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Шримат-Бхагаватам</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Вьяса</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Shrimat-Bhagavatam.7z" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title list">
                      <span>Иша Упанишада</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Вьяса</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title sound">
                      <span>Йога Сутры. Часть 4</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Аудио <br>Патанджали. Основа Йоги</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://vedicschool.net/wp-content/uploads/2020/11/Joga-sutra.-Gl.-4.-Patanzhali.mp3" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title movie">
                      <span>Вишну Пураны</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Видео-сериал <br>История сотворения мира</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="https://indiakino.net/3399-vishnu-purana.html" class="yes"></a>
                </div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="lib-course__item">
                <div class="lib-course__description">
                    <div class="lib-course__title special">
                      <span>Специальные материалы</span>
                    </div>
                    <div class="lib-course__info">
                      <span>Только для студентов</span>
                    </div>
                </div>
                <div class="lib-course__link ">
                  <a href="" class="no"></a>
                </div>
              </div>
            </div>
          </div>
          <div class="lib-course__recomend">
            <h3 class="title__light title__light--pink">Рекомендации</h3>
            <span>Благоприятно:</span>
            <ul>
              <li>Засыпать до 22:00, вставать к 5:00, практиковать;</li>
              <li>По субботам анализировать свой путь Дхармы;</li>
              <li>Каждое воскресенье планировать неделю;</li>
              <li>Каждое утро планировать день;</li>
              <li>Регулярно, до и после деятельности, делать «Паузу»</li>
              <li>Регулярно, до и после дел, выполнять «Осознанность»</li>
              <li>Регулярно, до и после дел, выполнять «Единение»</li>
            </ul>
          </div>
  </div>
</div>


<script type="text/javascript">

</script>

<?php get_footer(); ?>
