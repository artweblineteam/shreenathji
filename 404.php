<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package shreenathji
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				
				<div class="container">
					<img src="<?php bloginfo("template_directory"); ?>/assets/img/404.png" alt="Shreenathji Govardhan">
					<h2 class="title__wrapper wow animated fadeIn title__h2" data-wow-duration="1s" data-wow-delay=".1s">Ом Намо Нараяна
          </h2>
          <p>По вашему запросу страница не найдена</p>
          <a href="/">Попробуйте перейти на главную и попробовать еще раз.</a>
          
				</div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
