<?php
/*
Template Name: page-40 (intro-aurveda)
*/


 get_header(); ?>

<div class="banner banner-inner page-34 page-38">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-logotype wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">СИЛА ВЕДИЧЕСКОЙ МЕДИЦИНЫ АЮРВЕДА</h1>
      
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s" style="color: #00c0f3">Видео курс</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>Аюрведа. Аюр, с санскрита - жизненная сила. Веда - Знание. <br>Аюрведа - наука о том, как прожить жизнь здоровым, от первого до последнего вздоха.</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>ВЕДУЩИЙ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro">
    <div class="container">
      <div class="teacher-intro">
          <div class="teacher-name">
            <h2>
              <div class="title__subtext">Уджджвала Нииламани</div>
            </h2>
            <span>&nbsp;</span>
          </div>
          
          <span class="teacher-descr">Доктор Аюрведы (Вайдья) с 25-летней практикой. <br>Специализируется на Расаяне, ведической алхимии и астрологии Джйотиш. <br>Санскритолог, Ведазнавец, йог, философ, теолог.</span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <div class="course-info__desc"><span class="course-info__user">АЮРВЕДА  ДАЁТ ВОЗМОЖНОСТЬ:</span>
              <div class="divider"></div>
              <ul>
                <li>Глубже понять свою природу;</li>
                <li>Раскрыть пути повышения здоровья;</li>
                <li>Избавиться от недугов;</li>
                <li>Показать способы изменения Кармы и судьбы (Упаи);</li>
                <li>Ответить на любой вопрос.</li>

              </ul> 
              <span class="course-info__user course-info__user-into">БАЗОВЫЕ ПРАКТИКИ</span>
              <div class="divider"></div>
              <p>Для мастера Аюрведы жизнь — часть единого бытия, это позволяет не только быть здоровым самому, но и исцелять нуждающихся. </p>
            </div>
          </div>
        </div>
      </div>
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">НА КУРСЕ ОБ АЮРВЕДЕ ВЫ УЗНАЕТЕ:</span>
              <div class="divider"></div>
              <ul>
                <li>Кто такой Доктор Аюрведы?</li>
                <li>Древнюю философию Аюрведы и её источники – ведические тексты.</li>
                <li>Методы, используемые в аюрведе и актуальные и в наше время. Сравнение аюрведы с современной западной медициной.</li>
                <li>Принципы диагностики заболеваний, легко и понятно применимые на практике.</li>
                <li>Методы определения индивидуальной конституции тела и подбор методик для восстановления здоровья.</li>
                <li>Правила питания для здоровья и долголетия каждого человека.</li>
                <li>Практика  Расаяны ( очно)</li>
                <li>Аюрведические составы: свойства и противопоказания.</li>
                <li>Основы праноямы, хатха йоги и  медитации, применяемые Целителями. (очно)</li>
                <li>Познакомитесь с Мантравидьей. ( очно)</li>

              </ul>  
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full " href="/pricelist-single-courses/#love-yourself">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img1.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img2.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img3.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Видео<br>&nbsp;</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">1 тема — 1 видео <br>Всего: 20  минут</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Нет<br>&nbsp;</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Особенность курса —<br>только суть</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Тренинг для женщин  «Полюби себя»</span><br>
                              <span class="extra-light-subtitle">Онлайн марафон</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Видео, 20 минут, 1 тема</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>Бесплатно <p><a href="/cart/?add-to-cart=3834">Изучить</a></p></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody>
          </table>
    </div>
  </div>
</div>


<div class="course-result-end course-result-end_intro">
  <div class="container">
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><a class="btn btn--full" href="/cart/?add-to-cart=3834">Добро пожаловать</a></div>
  </div>
</div>


<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

var boxes = document.getElementsByClassName('course-info__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>

<?php get_footer(); ?>
