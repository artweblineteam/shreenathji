<?php
/*
Template Name: home
*/


 get_header(); ?>
<style type="text/css">
</style>
<div class="banner">
  <video autoplay muted loop>
    <source src="<?php bloginfo("template_directory"); ?>/assets/img/custom/video-top-1.7Mb.mp4" type="video/mp4"><span>Your browser does not support the video tag.</span>
  </video>
  <div class="container">
    <div class="banner__logo">
      <div class="logo wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <!-- <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">Академия</h1> -->
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">астрология, Йога, Васту и Аюрведа</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <ul class="start how__list">
              <li class="how__item">Познай все Законы мироздания</li>
              <li class="how__item">Научись управлять Сознанием</li>
              <li class="how__item">Вспомни свое Предназначение</li>
              <li class="how__item">Обрети благородную Профессию</li>
            </ul>
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a class="btn btn--full" href="/kyrs-intro/">ПОЛУЧИТЬ БЕСПЛАТНЫЙ КУРС</a>
    </div>
  </div>
</div>

<div class="new-about">
  <div class="container">
      <h1 class="title__wrapper wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
        <div class="title__subtext title-40">КОМАНДА АКАДЕМИИ</div>
        <div class="title__main with-border title-70"><span>ШРИНАТДЖИ</span></div>
        <div class="title__subtext title-40">ПРИВЕТСТВУЕТ ВАС</div>
      </h1>
      <div class="faculties-about__text">
        <p>Вы можете найти сотни курсов и школ, но большинство  <br>из них направлено на решение отдельных проблем.</p>
          <p>В то же время в мире существует всеохватывающее знание — <br>это Веды (на санскрите Знание). Это не религия, а точная наука.</p>
          <div class="line-divider-short"></div>
          <p>Но даже Ведические Знания за последние 5000 лет были <br>рассеяны на множество философий, школ, течений и наук – <br>от Логики и Материализма до Дао, Буддизма, Йоги и Веданты.</p>
          <div class="line-divider-short"></div>
          <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink title-divider">Наши наставники посвящены в Знание хранителями древнейших<br>
Ведических школ и являются проводниками исконных Знаний,<br>
переданных по цепям преемственности. Мы объединили все части<br>
и создали фундаментальную четырехуровневую программу на основе</h3>
          </div>
          <div class="list-in-oval list-in-oval__wrapper wow  fadeIn animated" data-wow-duration="1s" data-wow-delay=".4s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-name: fadeIn;">
            <ul class="list-in-oval__list-left">
              <li>Веданты</li>
              <li>Классической Йоги</li>
              <li>Астрологии Джйотиш</li>
              <li>Архитектуры Васту</li>
              <li>Аюрведы</li>
            </ul>
            <ul class="list-in-oval__list-right">
              <li>Дао</li>
              <li>Буддизма</li>
              <li>Психологии</li>
              <li>Нейрофизиологии</li>
              <li>Антропологии</li>
            </ul>
          </div>
  </div>
</div>

<div class="how">
    <div class="container">
      <div class="title__wrapper title__wrapper--experience wow  fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="title__subtext">миссия ВЕДИЧЕСКОЙ АКАДЕМИИ</div>
        <div class="title__main"><span>ШРИНАТДЖИ</span></div>
      </div>
      <div class="how__text">
        <p>Передать человечеству объединенное Знание древнего Ведического<br> и постведического наследия.</p>
        <div class="line-divider-white"></div>
        <p>Предоставить каждому студенту Академии возможность обрести <br>полное мировоззрение и получить благородную и востребованную профессию.</p>
      </div>
      <div class="how__bg">
        <div class="how__wrapper ">
          <div class="title__wrapper title__wrapper--how">
            <div class="title__subtext" style="color: #ffffff;">для вас В АКАДЕМИИ</div>
            <div class="title__subtext">доступны ДВЕ ПРОГРАММЫ</div>
            <div class="title__subtext" style="color: #ffffff;">ОБразования</div>
          </div>
          <div class="grid grid-top">
          <div class="col-1-2 col-sm-1-2 col-xs-1-1">
            <div class="how_inner">
              <img src="">
              <a href="/fakultety/#faculties-main" class="how_subtitle_link how_subtitle_link_budha">Профессиональное <br>образование</a>
              <div class="line-divider-white"></div>
              <p>Программа профессионального образования включает в себя 5 факультетов по 4 курса обучения</p>
              <a class="btn btn--full" href="/fakultety/#faculties-main">Подробнее</a>
            </div>
          </div>
           <div class="col-1-2 col-sm-1-2 col-xs-1-1  ">
            <div class="how_inner">
              <img src="">
              <a href="/fakultety/#faculties-courses" class="how_subtitle_link how_subtitle_link_lotos">ДОПОЛНИТЕЛЬНОЕ <br>ОБРАЗОВАНИЕ</a>
              <div class="line-divider-white"></div>
              <p>Программа дополнительного образования включает в себя десятки курсов и практик для решения конкретных проблем</p>
              <a class="btn btn--full" href="/fakultety/#faculties-courses">Подробнее</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
      
<div class="about">
  <div class="container">
    <div class="about__wrapper">
      <div class="mega-title wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
      <div class="title__wrapper">
        <div class="title__subtext">Изучая фундаментальные ведически</div>
        <div class="title__subtext with-border">знания, вы получите</div>
        <div class="title__main"><span>Ответы на все вопросы</span></div>
      </div>
      </div>
      <div class="about__list grid grid-top">
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
          <div class="about__item">
            <div class="about__icon about__icon--1 wow animated flipInX" data-wow-duration="1s" data-wow-delay=".3s"></div>
            <div class="about__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".4s">О СЕМЬЕ И ОТНОШЕНИЯХ</div>
            <div class="about__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Как построить гармоничные отношения? Как воспитывать ребенка счастливым и уверенным, без крика?</div>
          </div>
        </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
          <div class="about__item">
            <div class="about__icon about__icon--2 wow animated flipInX" data-wow-duration="1s" data-wow-delay=".6s"></div>
            <div class="about__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".4s">О ЗДОРОВЬЕ И СЧАСТЬЕ</div>
            <div class="about__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Как быть здоровым и энергичным? Как излечить любую болезнь?</div>
          </div>
        </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
          <div class="about__item">
            <div class="about__icon about__icon--3 wow animated flipInX" data-wow-duration="1s" data-wow-delay=".9s"></div>
            <div class="about__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".4s">О ПРЕДНАЗНАЧЕНИИ,  ПРОФЕССИИ И УСПЕХЕ </div>
            <div class="about__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Как найти предназначение,  обрести профессию,  зарабатывать и стать успешным лидером?</div>
          </div>
        </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
          <div class="about__item">
            <div class="about__icon about__icon--4 wow animated flipInX" data-wow-duration="1s" data-wow-delay="1.2s"></div>
            <div class="about__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".4s">О САМООСОЗНАНИИ  И ЦЕЛИ ЖИЗНИ </div>
            <div class="about__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Как найти смысл жизни  и осознать себя человеком? Как стать наставником? </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="experience">
    <div class="container">
      <div class="title__wrapper title__wrapper--experience wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
        <div class="title__subtext">Получив Ведические знания</div>
        <div class="title__main with-border"><span>вы сможете</span></div>
      </div>
      <div class="experience__list grid grid-top">
        <div class="col-1-3 col-sm-1-2 col-xs-1-1">
          <div class="experience__item experience__item--1">
            <div class="experience__icon experience__icon--1 wow animated flipInX" data-wow-duration="1s" data-wow-delay=".3s"></div>
            <div class="experience__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Преодолеть страх и лень, научиться концентрации, обрести уверенность в силах</div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-2 col-xs-1-1">
          <div class="experience__item experience__item--2">
            <div class="experience__icon experience__icon--2 wow animated flipInX" data-wow-duration="1s" data-wow-delay=".6s"></div>
            <div class="experience__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Найти смысл жизни, своё призвание, любимую работу или открыть своё дело</div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-2 col-xs-1-1">
          <div class="experience__item experience__item--3">
            <div class="experience__icon experience__icon--3 wow animated flipInX" data-wow-duration="1s" data-wow-delay=".9s"></div>
            <div class="experience__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Получить востребованную профессию</div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-2 col-xs-1-1">
          <div class="experience__item experience__item--4">
            <div class="experience__icon experience__icon--4 wow animated flipInX" data-wow-duration="1s" data-wow-delay="1.1s"></div>
            <div class="experience__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Обрести посвящение в знание от наставников древних школ</div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-2 col-xs-1-1">
          <div class="experience__item experience__item--5">
            <div class="experience__icon experience__icon--5 wow animated flipInX" data-wow-duration="1s" data-wow-delay="1.4s"></div>
            <div class="experience__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Разрабатывать и рекомендовать индивидуальные программы оздоровления и развития</div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-2 col-xs-1-1">
          <div class="experience__item experience__item--6">
            <div class="experience__icon experience__icon--6 wow animated flipInX" data-wow-duration="1s" data-wow-delay="1.7s"></div>
            <div class="experience__desc wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">Подбирать благоприятное время для начала дел</div>
          </div>
        </div>
      </div>
      <div class="btn__wrapper btn__wrapper--center">
        <a class="btn btn--full" href="/kyrs-intro/">ПОЛУЧИТЬ БЕСПЛАТНЫЙ КУРС</a>
      </div>
    </div>
</div>  

<div class="teachers">
    <div class="container">
      <h1 class="title__wrapper wow  fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="title__subtext title-40">наставники ведической академии</div>
        <div class="title__main with-border title-70"><span>ШРИНАТДЖИ</span></div>
      </h1>
      <div class="teachers__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="teachers__item ">
            <div class="teachers__img teachers_one" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/custom/teacher1.jpg);">
                <div class="teachers__bg">
                    <a class="teachers__icon" href="https://www.facebook.com/chetanah.bhavatu" target="_blank" rel="noopener">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               viewBox="0 0 48.605 48.605" style="enable-background:new 0 0 48.605 48.605;" xml:space="preserve">
              <g>
                <path d="M34.094,8.688h4.756V0.005h-8.643c-0.721-0.03-9.51-0.198-11.788,8.489c-0.033,0.091-0.761,2.157-0.761,6.983l-7.903,0.024
                  v9.107l7.913-0.023v24.021h12.087v-24h8v-9.131h-8v-2.873C29.755,10.816,30.508,8.688,34.094,8.688z M35.755,17.474v5.131h-8v24
                  h-8.087V22.579l-7.913,0.023v-5.107l7.934-0.023l-0.021-1.017c-0.104-5.112,0.625-7.262,0.658-7.365
                  c1.966-7.482,9.473-7.106,9.795-7.086l6.729,0.002v4.683h-2.756c-4.673,0-6.338,3.054-6.338,5.912v4.873L35.755,17.474
                  L35.755,17.474z"/>
              </g>
            </svg>
          </a>
        </div>
            </div>

            <div class="teachers__desc"><a class="teachers__user" href="#null">Алакх Ниранжан</a>
              <div class="teachers__skills">Ведическая астрология (Джйотиш), психология, Фундаментальные знания</div>
              <div class="teachers__info">
                <p>Философ, Физик, Психолог, Антрополог, Вайшнав. Астролог Джйотиш (оказал помощь тысячам искателей). Ученик Джйотиш Ачария Шри Раджендра Бушан Госвами Махарадж.</p>
                <p>В прошлом мастер боевых искусств, судья, член сборной страны (каратэ, кунг-фу). Инженер и лектор, организатор.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1">
          <div class="teachers__item">
            <div class="teachers__img teachers_two" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/custom/teacher2.jpg);">
                <div class="teachers__bg">
                    <a class="teachers__icon" href="https://www.facebook.com/veena.acharya.902" target="_blank" rel="noopener">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               viewBox="0 0 48.605 48.605" style="enable-background:new 0 0 48.605 48.605;" xml:space="preserve">
              <g>
                <path d="M34.094,8.688h4.756V0.005h-8.643c-0.721-0.03-9.51-0.198-11.788,8.489c-0.033,0.091-0.761,2.157-0.761,6.983l-7.903,0.024
                  v9.107l7.913-0.023v24.021h12.087v-24h8v-9.131h-8v-2.873C29.755,10.816,30.508,8.688,34.094,8.688z M35.755,17.474v5.131h-8v24
                  h-8.087V22.579l-7.913,0.023v-5.107l7.934-0.023l-0.021-1.017c-0.104-5.112,0.625-7.262,0.658-7.365
                  c1.966-7.482,9.473-7.106,9.795-7.086l6.729,0.002v4.683h-2.756c-4.673,0-6.338,3.054-6.338,5.912v4.873L35.755,17.474
                  L35.755,17.474z"/>
              </g>
            </svg>
          </a>
        </div>
            </div>

            <div class="teachers__desc"><a class="teachers__user" href="/">ВИНА АЧАРИЯ</a>
              <div class="teachers__skills">Наставник, Классическая Йога, Мантра-видья, медитация</div>
              <div class="teachers__info">
                <p>Йогин с 20-летним стажем. Окончила Бихарскую Школу Йоги и Джайн Вишва Бхарати Институт. Степень мастера Science of Living (Наука жизни). Провела сотни семинаров по всему миру. Основала Йога ашрам Yoga Dham Ashram (yogadham.in) в Наггаре, Гималаи, Индия.</p>
              </div>
            </div>
          </div>
        </div>

<!--         <div class="col-1-2 col-sm-1-1">
          <div class="teachers__item">
            <div class="teachers__img teachers_three" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/custom/teacher3.jpg);">
                <div class="teachers__bg">
                    <a class="teachers__icon" href="https://www.facebook.com/samvel.saakyan" target="_blank" rel="noopener">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               viewBox="0 0 48.605 48.605" style="enable-background:new 0 0 48.605 48.605;" xml:space="preserve">
              <g>
                <path d="M34.094,8.688h4.756V0.005h-8.643c-0.721-0.03-9.51-0.198-11.788,8.489c-0.033,0.091-0.761,2.157-0.761,6.983l-7.903,0.024
                  v9.107l7.913-0.023v24.021h12.087v-24h8v-9.131h-8v-2.873C29.755,10.816,30.508,8.688,34.094,8.688z M35.755,17.474v5.131h-8v24
                  h-8.087V22.579l-7.913,0.023v-5.107l7.934-0.023l-0.021-1.017c-0.104-5.112,0.625-7.262,0.658-7.365
                  c1.966-7.482,9.473-7.106,9.795-7.086l6.729,0.002v4.683h-2.756c-4.673,0-6.338,3.054-6.338,5.912v4.873L35.755,17.474
                  L35.755,17.474z"/>
              </g>
            </svg>
          </a>
        </div>
            </div>

            <div class="teachers__desc"><a class="teachers__user" href="/">Шиям</a>
              <div class="teachers__skills">Гьяна-йога, раджа-йога, теология, энергосистема человека</div>
              <div class="teachers__info">
                <p>Монах, отрешенный (Садху).</p>
                <p>Мастер йоги знания и концентрации.</p>
                <p>Более 15 лет живет в святых местах Индии (Бриджабаси). Изучает йогу любви к Всевышнему (Раджа-йогу), восточную философию, психологию. Доступно поможет осознать суть духовного пути и место Всевышнего в жизни каждого.</p>
              </div>
            </div>
          </div>
        </div> -->
<!--         <div class="col-1-2 col-sm-1-1">
          <div class="teachers__item">
            <div class="teachers__img teachers_four" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/custom/teacher4.jpg);">
                <div class="teachers__bg">
                    <a class="teachers__icon" href="https://www.facebook.com/pg/clubgoodtrip/about/?ref=page_internal" target="_blank" rel="noopener">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 48.605 48.605" style="enable-background:new 0 0 48.605 48.605;" xml:space="preserve">
              <g>
                <path d="M34.094,8.688h4.756V0.005h-8.643c-0.721-0.03-9.51-0.198-11.788,8.489c-0.033,0.091-0.761,2.157-0.761,6.983l-7.903,0.024
                  v9.107l7.913-0.023v24.021h12.087v-24h8v-9.131h-8v-2.873C29.755,10.816,30.508,8.688,34.094,8.688z M35.755,17.474v5.131h-8v24
                  h-8.087V22.579l-7.913,0.023v-5.107l7.934-0.023l-0.021-1.017c-0.104-5.112,0.625-7.262,0.658-7.365
                  c1.966-7.482,9.473-7.106,9.795-7.086l6.729,0.002v4.683h-2.756c-4.673,0-6.338,3.054-6.338,5.912v4.873L35.755,17.474
                  L35.755,17.474z"/>
              </g>
            </svg>
          </a>
        </div>
            </div>

            <div class="teachers__desc"><a class="teachers__user" href="/">АЛАК ПУРИ МАХАРАДЖ САДХУ БАБА</a>
              <div class="teachers__skills">Раджа-Йога</div>
              <div class="teachers__info">
                <p>20 лет практикует йогу и медитацию. Обрёл статус Махарадж. Имеет степень по философии. Живёт в Индии, в Гималаях. Принял посвящение в садху (монахи) священного ордена йогинов Juna Akhara. Использует средства йоги для помощи в самореализации. Основатель Good trip club.</p>
              </div>
            </div>
          </div>
        </div> -->
      </div>
      <div class="btn__wrapper btn__wrapper--center">
        <a class="btn btn--full" href="/o-nas/">ВСЕ НАСТАВНИКИ</a>
      </div>
    </div>
</div>

<div class="supervisors">
    <div class="container">
      <div class="text-border__wrapper text-border__wrapper--title">
        <div class="text-border__title text-border__title--title"><span>Руководители И НАСТАВНИКИ </span></div>
        <div class="text-border__text text-border__text--title">филиалов академии</div>
      </div>
      <div class="supervisors__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <div class="supervisors__item">
            <div class="supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_tk.png);">
              <a class="supervisors__icon" href="https://www.facebook.com/profile.php?id=100039220523082" target="_blank" rel="noopener">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 310 310" style="enable-background:new 0 0 310 310;" xml:space="preserve">
            <g id="XMLID_834_">
              <path id="XMLID_835_" d="M81.703,165.106h33.981V305c0,2.762,2.238,5,5,5h57.616c2.762,0,5-2.238,5-5V165.765h39.064
                c2.54,0,4.677-1.906,4.967-4.429l5.933-51.502c0.163-1.417-0.286-2.836-1.234-3.899c-0.949-1.064-2.307-1.673-3.732-1.673h-44.996
                V71.978c0-9.732,5.24-14.667,15.576-14.667c1.473,0,29.42,0,29.42,0c2.762,0,5-2.239,5-5V5.037c0-2.762-2.238-5-5-5h-40.545
                C187.467,0.023,186.832,0,185.896,0c-7.035,0-31.488,1.381-50.804,19.151c-21.402,19.692-18.427,43.27-17.716,47.358v37.752H81.703
                c-2.762,0-5,2.238-5,5v50.844C76.703,162.867,78.941,165.106,81.703,165.106z"/>
            </g>
          </svg>
        </a>
      </div>

            <div class="supervisors__desc"><a class="supervisors__user" href="#null">ТАТЬЯНА КАЧИНА</a>
              <div class="supervisors__info">Коуч, Женский тренер. Выпускник академии «Shreenathji». Училась у Садхгуру, Кехо, Диспенза, Тальписа, Зелинского, Роббинса, Правдиной. Очные тренинги в России, Испании, Франции, Италии</div>
            </div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <div class="supervisors__item">
            <div class="supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_mn.png);">
              <a class="supervisors__icon" href="https://www.facebook.com/profile.php?id=100013569254981" target="_blank" rel="noopener">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 310 310" style="enable-background:new 0 0 310 310;" xml:space="preserve">
            <g id="XMLID_834_">
              <path id="XMLID_835_" d="M81.703,165.106h33.981V305c0,2.762,2.238,5,5,5h57.616c2.762,0,5-2.238,5-5V165.765h39.064
                c2.54,0,4.677-1.906,4.967-4.429l5.933-51.502c0.163-1.417-0.286-2.836-1.234-3.899c-0.949-1.064-2.307-1.673-3.732-1.673h-44.996
                V71.978c0-9.732,5.24-14.667,15.576-14.667c1.473,0,29.42,0,29.42,0c2.762,0,5-2.239,5-5V5.037c0-2.762-2.238-5-5-5h-40.545
                C187.467,0.023,186.832,0,185.896,0c-7.035,0-31.488,1.381-50.804,19.151c-21.402,19.692-18.427,43.27-17.716,47.358v37.752H81.703
                c-2.762,0-5,2.238-5,5v50.844C76.703,162.867,78.941,165.106,81.703,165.106z"/>
            </g>
          </svg>
        </a>
      </div>

            <div class="supervisors__desc"><a class="supervisors__user" href="/">МАРИНА НИКИТИНА</a>
              <div class="supervisors__info">Лектор и наставник йоги. Ведический Астролог. Выпускник Ведической академии «Shreenathji». Мама троих детей. Организатор Клуба ЗОЖ в Атырау, Казахстан. Основатель компании «Хорошее настроение»</div>
            </div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <div class="supervisors__item">
            <div class="supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_kk.png);">
              <a class="supervisors__icon" href="https://www.facebook.com/kristina.dharma.7" target="_blank" rel="noopener">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 310 310" style="enable-background:new 0 0 310 310;" xml:space="preserve">
            <g id="XMLID_834_">
              <path id="XMLID_835_" d="M81.703,165.106h33.981V305c0,2.762,2.238,5,5,5h57.616c2.762,0,5-2.238,5-5V165.765h39.064
                c2.54,0,4.677-1.906,4.967-4.429l5.933-51.502c0.163-1.417-0.286-2.836-1.234-3.899c-0.949-1.064-2.307-1.673-3.732-1.673h-44.996
                V71.978c0-9.732,5.24-14.667,15.576-14.667c1.473,0,29.42,0,29.42,0c2.762,0,5-2.239,5-5V5.037c0-2.762-2.238-5-5-5h-40.545
                C187.467,0.023,186.832,0,185.896,0c-7.035,0-31.488,1.381-50.804,19.151c-21.402,19.692-18.427,43.27-17.716,47.358v37.752H81.703
                c-2.762,0-5,2.238-5,5v50.844C76.703,162.867,78.941,165.106,81.703,165.106z"/>
            </g>
          </svg>
        </a>
      </div>

            <div class="supervisors__desc"><a class="supervisors__user" href="#null">КРИСТИНА КЛИМЕНЧУК</a>
              <div class="supervisors__info">Трансперсональный ведический психолог, гипнолог, энерготерапевт, коуч, <br>гештальт-терапевт, выпускник академии «Shreenathji», мастер Кундалини йоги<br>&nbsp;</div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="reviews">
    <div class="container">
      <div class="text-border__wrapper text-border__wrapper--title">
        <div class="text-border__title text-border__title--title"><span>ОБРАТНАЯ СВЯЗЬ</span></div>
        <div class="text-border__text text-border__text--title">ОТЗЫВЫ БОЛЕЕ СТА СТУДЕНТОВ</div>
      </div>
      <div class="faculties-about__text"><p>Каждый студент обретает профессию, цель жизни и план ее достижения <br>100% результата. В академии не было ни одного негативного отзыва</p></div>
      <div class="reviews__list grid">
        <!-- <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/I7TrYivTmRs?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz1.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
          <!-- </div> -->

       <!--  <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/kIL3Z6HAUXY?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz2.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
       <!--  </div> -->

       <!--  <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/d-VdB4-Bx0k?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz3.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
       <!--  </div> -->

       <!--  <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/iwuW8chA5qo?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz4.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
       <!--  </div> -->

        <!-- <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/5R_Nnd8hTYs?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz5.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
        <!-- </div> -->

        <!-- <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/BN0OgRFkyd8?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz6.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
        <!-- </div> -->

        <!-- <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/yBxncmfVHp0?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz7.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
         <!--  </div> -->

        <!-- <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/qVYF6aPbRMc?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz8.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
         <!--  </div> -->

        <!-- <div class="col-1-3 col-sm-1-3 col-xs-1-1"> -->
          <a data-fancybox href="https://youtu.be/WnNIRlkjv1o?rel=0">
            <div class="reviews__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/otz9.jpg);">
              <div class="reviews__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></div>
            </div></a>
          <!-- </div> -->
      </div>
      <div class="btn__wrapper btn__wrapper--center">
        <a class="btn btn--full" href="/otzyvy/">Все Отзывы</a>
      </div>
    </div>
</div>
<script>
function videoplay(button) {
    var par = button.parentNode;
    par.innerHTML = '<iframe src="//www.youtube.com/embed/CstpdZZbOfQ?autoplay=1&rel=0" scrolling="no" style="width: 100%; height: 100%;" allow="autoplay"></iframe>';
}
</script>
<div class="container">
  <div class="video1">
    <div class="video1-play" onclick="videoplay(this);"></div>
  </div>
</div>

      
<!-- <div class="study-program">
  <div class="container">
    <div class="mega-title wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
      <div class="title__wrapper">
      <div class="title__main">Программа</div>
      <div class="title__subtext with-border"><span>"СОВЕРШЕННЫЙ ЧЕЛОВЕК"</span></div>
    </div>
    </div>
    <div class="study-program__wrapper wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".4s">
      <div class="study-program__title">4 курса, ПО 12 ТЕМ И 90 ДНЕЙ ПРАКТИКИ  <br>Дипломная ПРАКТИКА И ЗАЩИТА. ВСЕГО 1.5 ГОДА</div>
      <div class="study-program__desc">Каждый курс включает: 108 часов лекций и практик, материалы,
поддержку куратора, доступ к интерактивным вебинарам.</div>
    </div>
    <div class="text-border__wrapper text-border__wrapper--program">
      <div class="text-border__title text-border__title--program"><span>1, 2 КУРСЫ ДАЮТ ОБЩИЕ ЗНАНИЯ <br> 3, 4 ПРОФЕССИОНАЛНЫЕ НАВЫКИ</span></div>
      <div class="text-border__text text-border__text--program">Четыре факультета обеспечивают образование мастеров и наставников: астрологии Джйотиш, классической Йоги, архитектуры Васту и Искусства жизни</div>
    </div>
  </div>
</div> -->


<!-- <div class="how">
    <div class="container">
      <div class="text-border__wrapper text-border__wrapper--title">
        <div class="text-border__title text-border__title--title"><span>как проходит курс</span></div>
        <div class="text-border__text text-border__text--title">три формы обучения</div>
      </div>
      <div class="how__text">
        <p>Видео – Доступен в любое время. Тест после каждой лекции. Чат, Вебинары<br></p>
        <p>Стандарт - Онлайн и Очный. Групповые занятия. Чат, Вебинары. Доступ к Видео<br></p>
       <p> Максимум - Онлайн и Очный (в Индии) и прочее. Проводит Алакх Ниранжан.</p>
          </p>
        <p>* Каждый очный курс в Индии проходит в священных местах</p>
      </div>
      <div class="how__bg">
        <div class="how__wrapper">
          <div class="title__wrapper title__wrapper--how">
            <div class="title__main">Программа</div>
            <div class="title__subtext">каждого курса включает:</div>
          </div>
          <div class="how__twelve">
            <div class="twelve-left">
              <div class="twelve-left__item">Лекций</div>
              <div class="twelve-left__item">Практик</div>
            </div>
            <div class="twelve-center">12</div>
            <div class="twelve-right">
              <div class="twelve-right__item">Медитаций</div>
              <div class="twelve-right__item">Занятий йогой</div>
            </div>
          </div>
          <div class="how__desc">
            <div class="how__budda"></div>
            <ul class="how__list">
              <li class="how__item">Фундаментальные знания нескольких древних традиций</li>
              <li class="how__item">Йога планет — комплекс по гармонизации энергий планет</li>
              <li class="how__item">Экскурсия по древним святым местам (для курса в Индии)</li>
              <li class="how__item">Ягия — ритуал очищения судьбы (для курса в Индии)</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="btn__wrapper btn__wrapper--center-overlapped">
        <a class="btn btn--full" href="/fakultety/">Узнать больше</a>
      </div>
    </div>
</div> -->

<div class="schedule schedule-home" id="schedule">
        <!-- <div class="hidden-data"> -->
          <div class="container">
          <div class="text-border__wrapper text-border__wrapper--title">
        <div class="text-border__title text-border__title--title"><span>Расписание</span></div>
        <div class="text-border__text text-border__text--title">Очных курсов академии</div>
      </div>
            <?php the_content(); ?>
          </div>
        <!-- </div> -->
        <!-- <div class="btn__wrapper btn__wrapper--center">
          <div class="btn btn--full schedule-more">Просмотреть расписание</div>
        </div> -->
</div>

<div class="welcome">
    <div class="container">
      <div class="text-border__wrapper text-border__wrapper--title">
        <div class="text-border__title text-border__title--title"><span>Приветствие</span></div>
        <div class="text-border__text text-border__text--title">духовного наставника академии</div>
      </div>
      <div class="welcome__item">
        <div class="welcome__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/custom/rajendra.png);"></div>
        <div class="welcome__desc">
          <div class="welcome__name">Шри Раджендра Бушан</div>
          <div class="welcome__skills">
            <p>Госвами Махарадж Джйотиш Ачарий. Вайшнав.</p>
            <p>Хранитель Рудра Сампрадайи.</p>
            <p>Потомственный пандит в 21-м поколении</p>
          </div>
          <div class="welcome__icon"><a data-fancybox href="https://youtu.be/vxg_Ex4cJ-Y?rel=0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="512" height="512" viewBox="0 0 512 512"><g></g><path d="M152.443 136.417l207.114 119.573-207.114 119.593z"/></svg></a></div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    (function ($) {
$('.reviews__list').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true,
  responsive: [
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
  ]
});
})(jQuery);
</script>

<?php get_footer(); ?>