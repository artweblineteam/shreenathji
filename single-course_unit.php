<?php get_header(); ?>

<div class="container">
	<section class="unit-template">
		<article>
			<div class="wpcw-custom-unit-wrapper-1">
				<div class="wpcw-custom-unit-wrapper-2">
					<div class="wpcw-custom-unit-nav-wrapper">
						<?php
							if ( function_exists('dynamic_sidebar') )
								dynamic_sidebar('left-sidebar');
						?>
					</div>
					<div class="wpcw-custom-unit-lesson-wrapper-1">
						<div class="wpcw-custom-unit-lesson-wrapper-2">
							<?php if (have_posts()): while (have_posts()): the_post(); ?>
								<h1 class="course-unit-title">
									<?php the_title(); ?>
								</h1>
								<?php the_content(); ?>
							<?php endwhile; endif; ?>
						</div>
					</div>
				</div>
			</div>
		</article>
	</section>
</div>

<?php get_footer("unit"); ?>
<?php get_footer(); ?>