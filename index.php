<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shreenathji
 */

get_header();
?>
<div class="blog__grid index-template">
  <div class="container">
    <?php 
              if ( have_posts() ) :

          if ( is_home() && ! is_front_page() ) :
            ?>
            <header>
              <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
              <div class="title__main with-border"><span><?php single_post_title(); ?></span></div>
              </h2>
            </header>  
            <?php
          endif;
          endif;
    ?>
    <div class="grid grid-top">
        <div class="col-3-4 col-sm-1-1 col-xs-1-1">
      <main id="primary" class="site-main">
      <div class="filter-cat">
        <h2>Выбрать категорию:</h2>
        <div>
        <?php wp_dropdown_categories('show_option_none=Все категории'); ?>
        <script>
          var dropdown = document.getElementById("cat");
          function onCatChange() {
            if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
              location.href = "<?php echo get_option('home');
        ?>/?cat="+dropdown.options[dropdown.selectedIndex].value;
            }
          }
          dropdown.onchange = onCatChange;
        </script>
      </div>
      </div>

        <?php
        if ( have_posts() ) :

          /* Start the Loop */
          while ( have_posts() ) :
            the_post();

            /*
             * Include the Post-Type-specific template for the content.
             * If you want to override this in a child theme, then include a file
             * called content-___.php (where ___ is the Post Type name) and that will be used instead.
             */
            get_template_part( 'template-parts/content', 'archive' );

          endwhile;

          the_posts_navigation();

        else :

          get_template_part( 'template-parts/content', 'none' );

        endif;
        ?>
      </main><!-- #main -->
    </div>
            <div class="col-1-4 col-sm-1-2 col-xs-1-1">
    <?php get_sidebar(); ?>
  </div>
  </div>
  </div>
</div>
<?php

get_footer();
