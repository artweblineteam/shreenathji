<?php
/*
Template Name: price I
*/


 get_header(); ?>


<div class="title-inner">
    <div class="container">
      <h2 class="text-border__wrapper text-border__wrapper--title">
        <div class="text-border__title text-border__title--title"><span>Цены</span></div>
      </h2>
    </div>
</div>
<div class="price-tabs">
    <div class="container">
    	<div class="price-tabs__list">
    		<a href="/">Факультеты</a>
      		<a href="/">Отдельные курсы</a>
    	</div>
    </div>
</div>
<div class="price-features">
	<div class="container">
            <h3 class="title__light">Возможны три формы обучения</h3>
		<div class="price-features__list grid grid-top">
			<div class="col-1-3 col-sm-1-2 col-xs-1-1">
				<div class="price-feature">
					<h5 class="title__light title__light--pink">Пакет "Видео"</h5>
                              <div class="line-divider-short"></div>
					<span>Обучение по видеозаписям<br>&nbsp;</span>
				</div>
			</div>
			<div class="col-1-3 col-sm-1-2 col-xs-1-1">
				<div class="price-feature">
					<h5 class="title__light title__light--pink">Пакет "Стандарт"</h5>
                               <div class="line-divider-short"></div>
					<span>Очное и/или Онлайн обучение, <br>плюс доступ к видео<br>&nbsp;</span>
				</div>
			</div>
			<div class="col-1-3 col-sm-1-2 col-xs-1-1">
				<div class="price-feature">
					<h5 class="title__light title__light--pink">Пакет "Максимум"</h5>
                               <div class="line-divider-short"></div>
					<span>Очное и/или Онлайн обучение, <br>плюс доступ к видео <br>и максимальная поддержка</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="table-inner table__first">
    <div class="container">
      <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__h2 title__pink">Курс I</div>
            <div class="with-border with-border__long"><span>Все Факультеты</span></div>
          </h2>
      <div class="table-price price-list-first">
      	<table>
      		<tr>
      			<th>&nbsp;</th>
      			<th>
      				<span>Пакет «Видео»</span></br>
      				<span class="extra-light-subtitle">(доступ 1 год) </br>Очно IV курс</span>
      			</th>
      			<th colspan="2">
      				<span>Пакет «Стандарт»</span></br>
      				<span class="extra-light-subtitle">(доступ к видеокурсу 2 года)</span>
      			</th>
      			<th colspan="2">
      				<span>Пакет «Максимум»</span></br>
      				<span class="extra-light-subtitle">(доступ к видеокурсу 3 года)</span>
      			</th>
      		</tr>
      		<tr class="table-subtitle">
      			<td width="300px;">
      				<span>I курс</span>
      			</td>
      			<td width="170px;">
      				<span>видео (ч)</span>
      			</td>
      			<td width="170px;">
      				<span>видео (ч)</span>
      			</td>
      			<td width="170px;">
      				<span>Очно/онлайн (ч)</span>
      			</td>
      			<td width="170px;">
      				<span>видео (ч)</span>
      			</td>
      			<td width="170px;">
      				<span>Очно/онлайн (ч)</span>
      			</td>
      		</tr>
      		<tr>
      			<td>Искусство Жизни</td>
      			<td>27</td>
      			<td>27</td>
      			<td>57</td>
      			<td>27</td>
      			<td>57</td>
      		</tr>
      		<tr>
      			<td>Практики Йоги</td>
      			<td>3</td>
      			<td>3</td>
      			<td>36</td>
      			<td>3</td>
      			<td>36</td>
      		</tr>
      		<tr>
      			<td>Качество времени</td>
      			<td>3</td>
      			<td>3</td>
      			<td>15</td>
      			<td>3</td>
      			<td>15</td>
      		</tr>
      		<tr>
      			<td>Вебинары — Искусство Жизни</td>
      			<td>9</td>
      			<td>9</td>
      			<td>-</td>
      			<td>9</td>
      			<td>-</td>
      		</tr>
      		<tr>
      			<td>Распечатка конспекта</td>
      			<td>&nbsp;</td>
      			<td>-</td>
      			<td><i class="fas fa-check"></i></td>
      			<td>-</td>
      			<td><i class="fas fa-check"></i></td>
      		</tr>
      		<tr>
      			<td>Сопровождение куратора</td>
      			<td>12</td>
      			<td>12</td>
      			<td>-</td>
      			<td>6</td>
      			<td>-</td>
      		</tr>
      		<tr>
      			<td>Подготовка карты куратором</td>
      			<td><i class="fas fa-check"></i></td>
      			<td>-</td>
      			<td><i class="fas fa-check"></i></td>
      			<td>-</td>
      			<td>-</td>
      		</tr>
      		<tr>
      			<td>Онлайн-чат для общения</td>
      			<td><i class="fas fa-check"></i></td>
      			<td>-</td>
      			<td><i class="fas fa-check"></i></td>
      			<td>-</td>
      			<td><i class="fas fa-check"></i></td>
      		</tr>
      		<tr>
      			<td>Сопровождение руководителя</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>6</td>
      			<td>-</td>
      		</tr>
      		<tr>
      			<td>Подготовка карты основателем</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td><i class="fas fa-check"></td>
      			<td><i class="fas fa-check"></td>
      		</tr>
      		<tr>
      			<td>Уникальный учебник 1-го курса</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>-</td>
      			<td><i class="fas fa-check"></td>
      		</tr>
      		<tr>
      			<td>Выполнение 11 ягий в Ашраме</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>&nbsp;</td>
      			<td>-</td>
      			<td><i class="fas fa-check"></td>
      		</tr>
      		<tr class="table-subtitle-blue">
      			<td>ИТОГО ЧАСОВ</td>
      			<td>54</td>
      			<td colspan="2">162</td>
      			<td colspan="2">162</td>
      		</tr>
      		<tr>
      			<td>Полная стоимость</td>
      			<td>21 000 <a href="/">купить</a></td>
      			<td colspan="2">45 000 <a href="/">купить</a></td>
      			<td colspan="2">120 000 <a href="/">купить</a></td>
      		</tr>
      		<tr>
      			<td>Стоимость при оплате за 30 дней</td>
      			<td>-</td>
      			<td colspan="2">42 000 <a href="/">купить</a></td>
      			<td colspan="2">110 000 <a href="/">купить</a></td>
      		</tr>
      		<tr>
      			<td>Стоимость со скидкой для пар</td>
      			<td>-</td>
      			<td colspan="2">39 000 <a href="/">купить</a></td>
      			<td colspan="2">100 000 <a href="/">купить</a></td>
      		</tr>
      		<tr>
      			<td>Стоимость курса без практик йоги для онлайн или отказ от очной йоги</td>
      			<td>-</td>
      			<td colspan="2">33 000 <a href="/">купить</a></td>
      			<td colspan="2">100 000 <a href="/">купить</a></td>
      		</tr>
      	</table>
      </div>
    </div>
</div>
<div class="table-inner table__second">
    <div class="container">
      <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__h2 title__pink">Курс II</div>
            <div class="with-border with-border__long"><span>Все Факультеты</span></div>
          </h2>
      <div class="table-price price-list-first">
            <table>
                  <tr>
                        <th>&nbsp;</th>
                        <th>
                              <span>Пакет «Видео»</span></br>
                              <span class="extra-light-subtitle">(доступ 1 год) </br>Очно IV курс</span>
                        </th>
                        <th colspan="2">
                              <span>Пакет «Стандарт»</span></br>
                              <span class="extra-light-subtitle">(доступ к видеокурсу 2 года)</span>
                        </th>
                        <th colspan="2">
                              <span>Пакет «Максимум»</span></br>
                              <span class="extra-light-subtitle">(доступ к видеокурсу 3 года)</span>
                        </th>
                  </tr>
                  <tr class="table-subtitle">
                        <td width="300px;">
                              <span>II курс</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>Очно/онлайн (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>Очно/онлайн (ч)</span>
                        </td>
                  </tr>
                  <tr>
                        <td>Искусство Жизни</td>
                        <td>15</td>
                        <td>15</td>
                        <td>24</td>
                        <td>15</td>
                        <td>24</td>
                  </tr>
                  <tr>
                        <td>Практики Йоги</td>
                        <td>3</td>
                        <td>3</td>
                        <td>18</td>
                        <td>3</td>
                        <td>18</td>
                  </tr>
                  <tr>
                        <td>Вебинары — Искусство Жизни</td>
                        <td>9</td>
                        <td>9</td>
                        <td>-</td>
                        <td>9</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Марафон — Изобилие в благости</td>
                        <td>9</td>
                        <td>9</td>
                        <td>-</td>
                        <td>9</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Распечатка конспекта</td>
                        <td>&nbsp;</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Сопровождение куратора</td>
                        <td>6</td>
                        <td>6</td>
                        <td>-</td>
                        <td>3</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Сопровождение руководителя</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>3</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Уникальный учебник 2-го курса</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Выполнение 11 ягий в Ашраме</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr class="table-subtitle-blue">
                        <td>ИТОГО ЧАСОВ</td>
                        <td>42</td>
                        <td colspan="2">84</td>
                        <td colspan="2">84</td>
                  </tr>
                  <tr>
                        <td>Полная стоимость</td>
                        <td>12 000 <a href="/">купить</a></td>
                        <td colspan="2">24 000 <a href="/">купить</a></td>
                        <td colspan="2">64 000 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость при оплате за 30 дней</td>
                        <td>-</td>
                        <td colspan="2">22 500 <a href="/">купить</a></td>
                        <td colspan="2">59 000 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость со скидкой для пар</td>
                        <td>-</td>
                        <td colspan="2">21 000 <a href="/">купить</a></td>
                        <td colspan="2">54 000 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость курса без практик йоги для онлайн или отказ от очной йоги</td>
                        <td>-</td>
                        <td colspan="2">17 000 <a href="/">купить</a></td>
                        <td colspan="2">50 000 <a href="/">купить</a></td>
                  </tr>
            </table>
      </div>
    </div>
</div>
<div class="table-inner table__third">
    <div class="container">
      <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__h2 title__pink">Курс III</div>
          </h2>
      <div class="price-tabs__list">
                  <a href="/">факультет искусство жизни</a>
                  <a href="/">факультеты йоги, астрологии джйотиш и архитектуры васту</a>
            </div>
      <div class="table-price price-list-first">
            <table>
                  <tr>
                        <th>&nbsp;</th>
                        <th>
                              <span>Пакет «Видео»</span></br>
                              <span class="extra-light-subtitle">(доступ 1 год) </br>Очно III курс</span>
                        </th>
                        <th colspan="2">
                              <span>Пакет «Стандарт»</span></br>
                              <span class="extra-light-subtitle">(доступ к видеокурсу 2 года)</span>
                        </th>
                        <th colspan="2">
                              <span>Пакет «Максимум»</span></br>
                              <span class="extra-light-subtitle">(доступ к видеокурсу 3 года)</span>
                        </th>
                  </tr>
                  <tr class="table-subtitle">
                        <td width="300px;">
                              <span>III курс</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>Очно/онлайн (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>Очно/онлайн (ч)</span>
                        </td>
                  </tr>
                  <tr>
                        <td>Искусство Жизни</td>
                        <td>15</td>
                        <td>15</td>
                        <td>24</td>
                        <td>15</td>
                        <td>24</td>
                  </tr>
                  <tr>
                        <td>Практики Йоги</td>
                        <td>3</td>
                        <td>3</td>
                        <td>18</td>
                        <td>3</td>
                        <td>18</td>
                  </tr>
                  <tr>
                        <td>Распечатка конспекта</td>
                        <td>&nbsp;</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Сопровождение куратора</td>
                        <td>12</td>
                        <td>6</td>
                        <td>-</td>
                        <td>3</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Сопровождение руководителя</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>3</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Уникальный учебник 3-го курса</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Выполнение 11 ягий в Ашраме</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>5 публикаций на ресурсах</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Проведение вебинара по теме</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Дипломная работа</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Практика — общая консультация</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr class="table-subtitle-blue">
                        <td>ИТОГО ЧАСОВ</td>
                        <td>30</td>
                        <td colspan="2">66</td>
                        <td colspan="2">66</td>
                  </tr>
                  <tr>
                        <td>Полная стоимость</td>
                        <td>21 000 <a href="/">купить</a></td>
                        <td colspan="2">24 000 <a href="/">купить</a></td>
                        <td colspan="2">64 000 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость при оплате за 30 дней</td>
                        <td>-</td>
                        <td colspan="2">22 500 <a href="/">купить</a></td>
                        <td colspan="2">59 500 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость со скидкой для пар</td>
                        <td>-</td>
                        <td colspan="2">21 000 <a href="/">купить</a></td>
                        <td colspan="2">54 000 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость курса без практик йоги для онлайн или отказ от очной йоги</td>
                        <td>-</td>
                        <td colspan="2">17 000 <a href="/">купить</a></td>
                        <td colspan="2">50 000 <a href="/">купить</a></td>
                  </tr>
            </table>
      </div>
    </div>
<div class="table-inner table__fourth">
    <div class="container">
      <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__h2 title__pink">Курс IV</div>
          </h2>
      <div class="price-tabs__list">
                  <a href="/">факультет искусство жизни</a>
                  <a href="/">факультеты йоги, астрологии джйотиш и архитектуры васту</a>
            </div>
      <div class="table-price price-list-first">
            <table>
                  <tr>
                        <th>&nbsp;</th>
                        <th>
                              <span>Пакет «Видео»</span></br>
                              <span class="extra-light-subtitle">(доступ 1 год) </br>Очно IV курс</span>
                        </th>
                        <th colspan="2">
                              <span>Пакет «Стандарт»</span></br>
                              <span class="extra-light-subtitle">(доступ к видеокурсу 2 года)</span>
                        </th>
                        <th colspan="2">
                              <span>Пакет «Максимум»</span></br>
                              <span class="extra-light-subtitle">(доступ к видеокурсу 3 года)</span>
                        </th>
                  </tr>
                  <tr class="table-subtitle">
                        <td width="300px;">
                              <span>IV курс</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>Очно/онлайн (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td width="170px;">
                              <span>Очно/онлайн (ч)</span>
                        </td>
                  </tr>
                  <tr>
                        <td>Искусство Жизни</td>
                        <td>24</td>
                        <td>24</td>
                        <td>24</td>
                        <td>&nbsp;</td>
                        <td>24</td>
                  </tr>
                  <tr>
                        <td>Практики Йоги</td>
                        <td>18</td>
                        <td>18</td>
                        <td>18</td>
                        <td>&nbsp;</td>
                        <td>18</td>
                  </tr>
                  <tr>
                        <td>Вебинары — Духовное развитие</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td>9</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Распечатка конспекта материалов</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Сопровождение куратора</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>6</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Духовное посвящение</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Сопровождение руководителя</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>6</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Уникальный учебник 4-го курса</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Выполнение 11 ягий в Ашраме</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Документы для организации филиала</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Диплом подтверждён в Индии</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Трудоустройство дипломников</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Выпускники — услуги на сайте</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr class="table-subtitle-blue">
                        <td>ИТОГО ЧАСОВ</td>
                        <td>42</td>
                        <td colspan="2">63</td>
                        <td colspan="2">63</td>
                  </tr>
                  <tr>
                        <td>Cтоимость</td>
                        <td>17 000 <a href="/">купить</a></td>
                        <td colspan="2">24 000 <a href="/">купить</a></td>
                        <td colspan="2">64 000 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость при оплате за 30 дней</td>
                        <td>15 500 <a href="/">купить</a></td>
                        <td colspan="2">22 500 <a href="/">купить</a></td>
                        <td colspan="2">59 500 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость со скидкой для пар</td>
                        <td>14 000 <a href="/">купить</a></td>
                        <td colspan="2">21 000 <a href="/">купить</a></td>
                        <td colspan="2">54 000 <a href="/">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость курса без практик йоги для онлайн или отказ от очной йоги</td>
                        <td>12 000 <a href="/">купить</a></td>
                        <td colspan="2">17 000 <a href="/">купить</a></td>
                        <td colspan="2">50 000 <a href="/">купить</a></td>
                  </tr>
            </table>
      </div>
    </div>
</div>
<div class="table-inner table__fifth">
    <div class="container">
      <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__h2 title__pink">стоимость </br>при оплате трёх курсов</div>
            <div class="with-border with-border__long"><span>(специалист)</span></div>
          </h2>
      <div class="price-tabs__list">
                  <a href="/">факультет искусство жизни</a>
                  <a href="/">факультеты йоги, астрологии джйотиш и архитектуры васту</a>
            </div>
      <div class="table-price price-list-fifth">
            <table>
                  <tr>
                        <th>&nbsp;</th>
                        <th>
                              <span>Пакет «Видео»</span>
                              
                        </th>
                        <th colspan="2">
                              <span>Пакет «Стандарт»</span>
                        </th>
                        <th colspan="2">
                              <span>Пакет «Максимум»</span>
                        </th>
                  </tr>
                  <tr class="table-subtitle">
                        <td width="300px;">
                              <span>I, II, III курсы</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td colspan="2" width="340px;">
                              <span>Очно, онлайн и видео (ч)</span>
                        </td>
                        <td colspan="2" width="340px;">
                              <span>Очно, онлайн и видео (ч)</span>
                        </td>
                  </tr>
                  <tr class="table-subtitle-blue">
                        <td>ИТОГО ЧАСОВ</td>
                        <td>126</td>
                        <td colspan="2">312</td>
                        <td colspan="2">312</td>
                  </tr>
                  <tr>
                        <td>Cтоимость</td>
                        <td>75 000 </td>
                        <td colspan="2">105 000 </td>
                        <td colspan="2">280 000 </td>
                  </tr>
                  <tr>
                        <td>Стоимость при оплате за 30 дней</td>
                        <td>49 000 </td>
                        <td colspan="2">91 000 </td>
                        <td colspan="2">224 000 </td>
                  </tr>
                  <tr>
                        <td>Стоимость со скидкой для пар</td>
                        <td>45 000 <a href="/">купить</a></td>
                        <td colspan="2">78 000 <a href="/">купить</a></td>
                        <td colspan="2">198 000 <a href="/">купить</a></td>
                  </tr>
            </table>
      </div>
    </div>
<div class="table-inner table__sixth">
    <div class="container">
      <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__h2 title__pink">стоимость </br>при оплате четырех курсов</div>
            <div class="with-border with-border__long"><span>(мастер)</span></div>
          </h2>
      <div class="price-tabs__list">
                  <a href="/">факультет искусство жизни</a>
                  <a href="/">факультеты йоги, астрологии джйотиш и архитектуры васту</a>
            </div>
      <div class="table-price price-list-fifth">
            <table>
                  <tr>
                        <th>&nbsp;</th>
                        <th>
                              <span>Пакет «Видео»</span>
                              
                        </th>
                        <th colspan="2">
                              <span>Пакет «Стандарт»</span>
                        </th>
                        <th colspan="2">
                              <span>Пакет «Максимум»</span>
                        </th>
                  </tr>
                  <tr class="table-subtitle">
                        <td width="300px;">
                              <span>I, II, III, IV курсы</span>
                        </td>
                        <td width="170px;">
                              <span>видео (ч)</span>
                        </td>
                        <td colspan="2" width="340px;">
                              <span>Очно, онлайн и видео (ч)</span>
                        </td>
                        <td colspan="2" width="340px;">
                              <span>Очно, онлайн и видео (ч)</span>
                        </td>
                  </tr>
                  <tr class="table-subtitle-blue">
                        <td>ИТОГО ЧАСОВ</td>
                        <td>168</td>
                        <td colspan="2">375</td>
                        <td colspan="2">375</td>
                  </tr>
                  <tr>
                        <td>Cтоимость</td>
                        <td>69 000 </td>
                        <td colspan="2">135 000 </td>
                        <td colspan="2">360 000 </td>
                  </tr>
                  <tr>
                        <td>Стоимость при оплате за 30 дней</td>
                        <td>66 000 </td>
                        <td colspan="2">115 000 </td>
                        <td colspan="2">288 000 </td>
                  </tr>
                  <tr>
                        <td>Стоимость со скидкой для пар</td>
                        <td>59 000 <a href="/">купить</a></td>
                        <td colspan="2">102 000 <a href="/">купить</a></td>
                        <td colspan="2">252 000 <a href="/">купить</a></td>
                  </tr>
            </table>
      </div>
      <div class="economy">
            <div class="economy__list"><div class="title__light title__light--pink">Экономия 40%</div></div>
            <div class="economy__list"><div class="title__light">-51 000</div></div>
            <div class="economy__list"><div class="title__light">-24 000</div></div>
            <div class="economy__list"><div class="title__light title__light--pink">20%</div></div>
            <div class="economy__list"><div class="title__light">-36 000</div></div>
            <div class="economy__list"><div class="title__light title__light--pink">33,3%</div></div>
            <div class="economy__list"><div class="title__light">-160 000</div></div>
      </div>
    </div>
</div>


  
<?php get_footer(); ?>