<?php
/*
Template Name: fakulty-vastu
*/


 get_header(); ?>

      
      <div class="faculty-about" id="faculty-veda-about">
        <div class="container">
          <h1 class="faculty-about__title wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="title__h2">факультет Ведическая архитектура И ДИЗАЙН</div>
            <div class="title__main with-border"><span>васту</span></div>
            <div class="title__h2">профессиональное образование</div>
          </h1>
          <div class="faculty-about__text">
            <p>Факультет проводит фундаментальную подготовку сознания студентов Академии Шринатджи</p>
            <p>Программа изучает природу и законы взаимодействия энергии в пространстве,<br> роль и влияние дома, участка, пространства на способности и судьбу человека<br>Дает Знания и Навыки Искусства жизни, управления сознанием и коуч навыки</p>
            <p>Выпускники способны подбирать участки земли и дома, давать рекомендации, <br>проектировать и создавать дизайн помещений, с учетом психики человека.</p>
            <p>Программа длится 1.5 года и включает 4 курса. <br>Стоимость от 81 000 до 400 000 рублей</p>
          </div>
          <div class="grid qualification2">
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">ВАСТУ — ДИЗАЙНЕР <br>ИНТЕРЬЕРОВ, КОНСУЛЬТАНТ
                <div class="line-divider"></div>
                <span>С нулевого уровня</span>
              </div>
              
            </div>
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">ВАСТУ — ЛАНДШАФТНЫЙ <br>ДИЗАЙНЕР, наставник
                <div class="line-divider"></div>
                <span>С нулевого уровня </span>
              </div>
    
            </div>
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">ВАСТУ – АРХИТЕКТОР, <br>НАСТАВНИК
                <div class="line-divider"></div>
                <span>Для сертифицированных кадровиков</span>
              </div>

            </div>
          </div>
          <div class="btn__wrapper btn__wrapper--center faculty__btn-wrapper1 with-border1 with-border1__long">
            <a class="btn btn--full" href="/specialty/#vastu">Специальности</a>
          </div>
          <div class="faculty-about__text">
            <p>Программа факультета профессионального образования <br>ВЕДИЧЕСКАЯ АРХИТЕКТУРА И ДИЗАЙН ВАСТУ <br>позволяет каждому студенту решить такие задачи:</p>
          </div>
          <div class="grid">
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">1</div>
                  <div class="faculty__task-desc">Найти своё предназначение, высокую цель и энергию для её достижения</div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">2</div>
                  <div class="faculty__task-desc">Получить знание архитектуры Васту от представителя самой уважаемой школы</div>
                </div>
              </div>
            </div>
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">3</div>
                  <div class="faculty__task-desc">Обрести высокооплачиваемую профессию архитектора и дизайнера Васту</div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">4</div>
                  <div class="faculty__task-desc">Обрести Знание о мироздании и социуме. Научится строить высокие отношения</div>
                </div>
              </div>
            </div>
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">5</div>
                  <div class="faculty__task-desc">Научится управлять сознанием и событиями. Обрести удачу, целеустремленность</div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">6</div>
                  <div class="faculty__task-desc">Научится планировать. Получить управленческие и наставнические навыки</div>
                </div>
              </div>
            </div>
          </div>
          <div class="faculty__margin text__c">
            <!-- Для полной реализации недостаточно приобрести профессию. Необходимо обладать<br>широким мировоззрением и владеть знанием психологии личности и социума.<br>
            В достаточной мере управлять своим сознанием и видеть Мироздание во всей полноте<br>
            Обладать организационными, управленческими и наставническими навыкамм. -->
          </div>
        </div>
      </div>
      <div class="faculty-learning" id="faculty-veda-learning">
        <div class="container">
          <div class="title__wrapper wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="title__h2">факультет Ведическая архитектура И ДИЗАЙН</div>
            <div class="title__h2 with-border with-border__long">васту</div>
            <div class="title__h2">И ДИЗАЙНУ ВАСТУ В АКАДЕМИИ</div>
          </div>
          <h3 class="title__light title__light--pink">Учебный процесс</h3>
          <div class="text__c">
            <p>Обучение предусматривает четыре курса по 108 часов каждый и рассчитано на 1,5 года.  <br>Первые два курса являются базовыми — общая программа для всех направлений.  <br>Третий и четвертый курсы дают специальность и общие фундаментальные знания.</p>
            <p>Программа обучения дает фундаментальные знания о мироздании, <br>человеке, отношениях, концентрации и основах йоги, <br> без которых восприятие любой ведической науки невозможно.</p>
          </div>
          <div class="grid grid-top">
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-i-general/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;I</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Предназначение энергия отношения<br>«Сознание Брахмы»</p>
                    <p class="text__gray">Творец. Курс из трех частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Передает фундаментальные Ведические Знания (57 ч):</h5>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">О Мироздании, Человеке, Карме (Судьбе), Дхарме, Долге, Духовном предназначении;</li>
                    <li class="list-small-dots__item">О Высоких гармоничных отношениях, Энергообмене и Карма-йоге;</li>
                    <li class="list-small-dots__item">Наделяет осознанием Высокой Цели, Призвания, Энергией и Удачей.</li>
                  </ul>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Введение в астрологию Джйотиш, Панчанга, Мухурта, I ступень (15 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Базовые практики Хатха-йоги, очищения, здоровья спины, суставов (36 ч).</h5>
                  <p class="text__12light text__black">
                    Каждый участник I-го курса получает расшифровку натальной карты в области предназначения, призвания и долга. Каждый выпускник получает свидетельство о прохождении курса, которое является допуском ко II-му курсу.
                  </p>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2245"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-ii-jizn/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;II</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Джйотиш, концентрация, успех,<br>«Сознание Шивы»</p>
                    <p class="text__gray">Организатор. Курс из трёх частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Фундаментальные Знания психики человека и Законов успеха (36 ч).</h5>
                  <p class="text__12light text__black">
                    Принципы концентрации и управления собственной психикой. Позволяет усилить характер и способности, что приводит к успеху.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Практики Кундалини-йоги, Пранаямы, Пратьяхары и Концентрации (36 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Базовые Знания Джйотиш, II ступень (36 ч).</h5>
                  <p class="text__12light text__black">Включает подробное изучение всех инструментов Ведической астрологии.</p>
                  <p class="text__12light text__black">
                    Каждый участник по окончании II-го курса получает свидетельство о прохождении курса, которое является допуском к III-му курсу.  </p>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="3527"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-iii-vastu/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;III</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Управление сознанием и действительностью<br>«Сознание Вишну»</p>
                    <p class="text__gray">Лидер. Курс из трёх частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Фундаментальные знания социальной психологии, законов общества (36 ч). </h5>
                  <p class="text__12light text__black">
                    Учит способности обретать и умножать счастье благодаря работе с обществом. Несет Знание и навык осознанности, глубокое восприятие общества и Вселенной.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Передает Знания Раджа-йоги и практик Дхараны, свидетельствования (36 ч). </h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Фундаментальный курс Ведической архитектуры Васту.</h5>
                  <p class="text__12light text__black">
                    Понятия. 12 законов Васту. Методы коррекции. Васту и все сферы жизни: здоровье, деньги, путешествия, хозяин дома, супружеские отношения. Программы Васту. Практика.
                    Все сферы жизни: здоровье, деньги, отношения, путешествия. Практика. Более 100 примеров.</p>
                  <p class="text__12light text__black">
                    Каждый студент по окончании III-го курса, сдачи экзамена и прохождения практики получает сертификат с указанием квалификации и профессии:</p>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">Oрганизатор и лидер;</li>
                    <li class="list-small-dots__item">Васту дизайнер интерьеров.</li>
                  </ul>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2143"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-iv-vastu/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;IV</div>
                    <div class="course__option text__pink16light">ОЧНО</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Познание истины<br>«Сознание Абсолюта»</p>
                    <p class="text__gray">Наставник. Курс из трёх частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Дает фундаментальные знания о Всевышнем и Истории Знания (36 ч).  </h5>
                  <p class="text__12light text__black">
                    Несет любовь, способность восприятия Высших сил и счастье. Студенты получают уникальную возможность обрести посвящение непосредственно из первых рук – хранителей древнейших сампрадай (традиций) на Земле.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Знания и практики Раджа-, Гьяна- и Бхакти-йоги (36 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Васту проектирование на участке. Васту ландшафтный дизайн. Васту для бизнеса и предприятий (36 ч).</h5>
                  <p class="text__12light text__black">
                    Студенты по окончании IV-го курса, сдачи экзамена, защиты Диплома и прохождения практики получают Диплом выпускника Академии с квалификацией и профессией:</p>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">Наставник;</li>
                    <li class="list-small-dots__item">Васту ландшафтный дизайнер;</li>
                    <li class="list-small-dots__item">Васту архитектор (только для сертифицированных специалистов)</li>
                  </ul>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2197"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
          </div>
          <div class="course__statement text__c">
            <p>
              Обучение проходит на четырех курсах и длится 2-3 года (включая практику). <br>Первые два курса одинаковы для каждого факультета – они несут базовые, <br>Фундаментальные Знания о Мироздании, человеке, отношениях, концентрации, основах <br>Йоги и Джйотиш, без которых восприятие любой Ведической науки невозможно.
            </p>
          </div>
        </div>
        <div class="course__compare-package text__c"><a class="btn btn--full btn--longer" href="/fakultety/#programm-coast">Сравнение пакетов</a></div>
        <div class="container">
          <div class="course__general wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="with-border with-border__long title__pink">О&nbsp;васту</div>
          </div>
          <div class="course__general--info">
            <div class="grid grid-top">
              <div class="col-1-2 col-sm-1-1">
                <p>
                  Васту — наука Ведической архитектуры, предназначенная для построения счастливого общества и гармонизации
                  человеческих отношений в нем. Васту-шастра на санскрите обозначает: Васту — место, т.е. выделенное пространство,
                  а шастра — наука или трактат. Васту — одно из 64 древних индийских видов искусства — существует уже много
                  тысяч лет. </p>
                  <p>В древних манускриптах, написанных более 5000 лет назад — «Махабхарате» и «Рамаяне» — описываются древние города с многоэтажными домами и бассейнами. </p>
                  <p>В 1922 году был раскопан город Мохенджо-Даро, выстроенный в соответствии со всеми принципами Васту. Город вызывает удивление своей планировкой, системами канализации, сетью проездов и неслыханными удобствами, даже
                  по современным меркам. Радиоуглеродный анализ позволяет отнести его к 2000 гг. до нашей эры. Интересно также,
                  что с Васту был знаком и знаменитый древнеримский архитектор Ветрувий. Он слово в слово повторяет один из разделов Манасары — классической книги Васту.</ul>
              </div>
              <div class="col-1-2 col-sm-1-1">
                <p>В основе Васту лежит знание Законов взаимодействия тонких энергий Вселенной. Каждый предмет на тонком уровне связан со всеми другими предметами. В ведические времена к дому относились как к вместилищу души, и она
                в благодарность за любовь и заботу, охраняла жизнь
                и покой своих хозяев. Васту-шастра основана на гармоничном влиянии Лунных и Солнечных воздействий на сознание человека во времени. По этой причине Васту тесным образом связана с Ведической астрологией Джйотиш и наукой
                о жизни Аюрведой. </p>
                <p>Каждый студент факультета Ведической архитектуры и дизайна Васту получит знания о том, как сделать участок, дом, офис гармоничным, здоровым, уютным и красивым. Выпускники факультета смогут помогать людям, делая их жизнь полнее и счастливее, нести в мир здоровье, уверенность, оптимизм и благосостояние. Это не просто профессия, которая даст материальное благополучие, это уверенность и ощущение целителя человеческих душ.</p>
              </div>
            </div>
          </div>
          <div class="line-divider margin-bottom90"></div>
        </div>
      </div>
    
<script type="text/javascript">
  var boxes = document.getElementsByClassName('course__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>

<?php get_footer(); ?>
