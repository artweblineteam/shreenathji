<?php
/**
 * shreenathji functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package shreenathji
 */

if ( ! function_exists( 'shreenathji_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function shreenathji_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on shreenathji, use a find and replace
		 * to change 'shreenathji' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'shreenathji', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_theme_support('menus');
		
		/* Woocommerce */
		add_theme_support( 'woocommerce' );
		define('WOOCOMMERCE_USE_CSS', false);

/*      Marked for deletion
 * 
 * 		function woo_style() {
			wp_register_style( 'mihalica-woocommerce', get_template_directory_uri() . '/woocommerce.css', null, 1.0, 'screen' );
			wp_enqueue_style( 'mihalica-woocommerce' ); 
		} 
		add_action( 'wp_enqueue_scripts', 'woo_style' );
		/*** ФИНАЛ перенос woocommerce.css в файл шаблона ***/

		/*** перенос woocommerce-layout.css в файл шаблона ***/
/*		function woo2_style() {
			wp_register_style( 'mihalica-woocommerce-layout', get_template_directory_uri() . '/woocommerce-layout.css', null, 1.0, 'screen' );
			wp_enqueue_style( 'mihalica-woocommerce-layout' ); 
		} 
		add_action( 'wp_enqueue_scripts', 'woo2_style' );
		/*** ФИНАЛ - перенос woocommerce-layout.css в файл шаблона ***/

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu' => esc_html__( 'Primary', 'shreenathji' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'shreenathji_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'shreenathji_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function shreenathji_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'shreenathji_content_width', 640 );
}
add_action( 'after_setup_theme', 'shreenathji_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function shreenathji_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'shreenathji' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'shreenathji' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'shreenathji_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function shreenathji_scripts() {
	if (!is_page(31) && !is_home() && !is_front_page()) {
		wp_enqueue_style( 'shreenathji-style', get_stylesheet_uri() );
	}
	wp_enqueue_script( 'shreenathji-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'shreenathji-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	
	// Delete after migration to css/main.css
	// if (!is_page(31) && !is_page(336) && !is_page(313) && !is_home() && !is_front_page()) {
	// 	wp_enqueue_style( 'main-style', get_template_directory_uri() . '/main.css' );
	// }
	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }
}
add_action( 'wp_enqueue_scripts', 'shreenathji_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//remove_filter('the_content', 'wpautop');

/* Marked for deletion
 * 
add_action( 'wp_footer', 'back_to_top' );
function back_to_top() {
echo '<img id="totop" src="http://www.shreenathji.top/wp-content/themes/shreenathji/img/vverh.png" />';
}
add_action( 'wp_head', 'back_to_top_style' );
function back_to_top_style() {
echo '<style type="text/css">
#totop {
cursor:pointer;
position: fixed;
right: 30px;
bottom: 30px;
display: none;
outline: none;
}
</style>';
}
add_action( 'wp_footer', 'back_to_top_script' );
function back_to_top_script() {
echo '<script type="text/javascript">
jQuery(document).ready(function($){
$(window).scroll(function () {
if ( $(this).scrollTop() > 400 )
$("#totop").fadeIn();
else
$("#totop").fadeOut();
});
 
$("#totop").click(function () {
$("body,html").animate({ scrollTop: 0 }, 800 );
return false;
});
});
</script>';
}*/

// function save_profile_fields( $user_id ) {
// 	update_usermeta( $user_id, 'city', $_POST['city'] );
// 	update_usermeta( $user_id, 'gender', $_POST['gender'] );
// }
 
// add_action( 'personal_options_update', 'save_profile_fields' );
// add_action( 'edit_user_profile_update', 'save_profile_fields' );



## Отключает Гутенберг (новый редактор блоков в WordPress).
## ver: 1.2
if( 'disable_gutenberg' ){
	remove_theme_support( 'core-block-patterns' ); // WP 5.5

	add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );

	// отключим подключение базовых css стилей для блоков
	// ВАЖНО! когда выйдут виджеты на блоках или что-то еще, эту строку нужно будет комментировать
	remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );

	// Move the Privacy Policy help notice back under the title field.
	add_action( 'admin_init', function(){
		remove_action( 'admin_notices', [ 'WP_Privacy_Policy_Content', 'notice' ] );
		add_action( 'edit_form_after_title', [ 'WP_Privacy_Policy_Content', 'notice' ] );
	} );
}
/*Длина описания поста*/
add_filter( 'excerpt_length', function(){
	return 85;
} );
 
function change_excerpt( $more ) {
     //return '...';
     return '&hellip; <a href="' . get_the_permalink() . '" class="read-more-link">Читать далее»</a>';
}
add_filter('excerpt_more', 'change_excerpt'); //Wordpress 2.8+

add_filter( 'woocommerce_login_redirect', 'shreenathji_login_redirect', 25, 2 );
 
function shreenathji_login_redirect( $redirect, $user ) {
 
	$redirect = site_url('/my-account/courses/');
	return $redirect;
}
add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){

  wp_redirect( home_url() );
  exit();

}


function register_my_widgets(){
	register_sidebar( array(
		'name' => "Сайдбар курсов",
		'id' => 'left-sidebar',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	) );
}
add_action( 'widgets_init', 'register_my_widgets' );


function price_shortcode_callback( $atts ) {
    $atts = shortcode_atts( array(
        'id' => null,
    ), $atts, 'bartag' );

    $html = '';

    if( intval( $atts['id'] ) > 0 && function_exists( 'wc_get_product' ) ){
         $_product = wc_get_product( $atts['id'] );
         $html = number_format($_product->get_price(),  0, ',', ' ');
    }
    return $html;
}
add_shortcode( 'shree_price', 'price_shortcode_callback' );



function shree_add_to_cart( $atts) {
    $atts = shortcode_atts(
        array('id'  => get_the_ID() ),
        $atts, 'stock_status'
    );
    $product = wc_get_product( $atts['id'] );
    $stock_status = $product->get_stock_status();
    $product_price = $product->get_price_html();
    $product_price = '';
    if( intval( $atts['id'] ) > 0 && function_exists( 'wc_get_product' ) ){
         $_product = wc_get_product( $atts['id'] );
         $product_price = number_format($_product->get_price(),  0, ',', ' ');
    }

    $_product = $product->add_to_cart_url();
    $id = $product->get_id();

    if ( 'instock' == $stock_status) {
        return '<p class="stock in-stock">'. '₽' . '&nbsp;' . $product_price  .'<a href="/cart/?add-to-cart='.$id.'" data-quantity="1" title="Купить"><i></i></a></p>';
    } else {
        return '<p class="stock out-of-stock" title="Временно недоступен">'. '₽' . '&nbsp;' .$product_price.'<i></i></p>';
    }
}
add_shortcode( 'woocommerce_price', 'shree_add_to_cart' ); 


add_filter( 'woocommerce_checkout_fields' , 'custom_remove_woo_checkout_fields' );
 
function custom_remove_woo_checkout_fields( $fields ) {
    // remove billing fields
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    // remove shipping fields  
    unset($fields['shipping']['shipping_company']);
    unset($fields['shipping']['shipping_address_1']);
    unset($fields['shipping']['shipping_address_2']);
    unset($fields['shipping']['shipping_city']);
    unset($fields['shipping']['shipping_postcode']);
    unset($fields['shipping']['shipping_country']);
    unset($fields['shipping']['shipping_state']);
    
    // remove order comment fields;
    return $fields;
}

// // Добавление чекбокса
// add_action( 'woocommerce_review_order_before_submit', 'truemisha_privacy_checkbox', 25 );
 
// function truemisha_privacy_checkbox() {
 
// 	woocommerce_form_field( 'privacy_policy_checkbox', array(
// 		'type'          => 'checkbox',
// 		'class'         => array( 'form-row' ),
// 		'label_class'   => array( 'woocommerce-form__label-for-checkbox' ),
// 		'input_class'   => array( 'woocommerce-form__input-checkbox' ),
// 		'required'      => true,
// 		'label'         => 'Согласен с <a href="/wp-content/uploads/2020/10/Oferta-Vedicheskoi-akademii-Shrinatdzhi.pdf" target="_blank">Договором-офертой</a>',
// 	));
 
// }
 
// // Валидация
// add_action( 'woocommerce_checkout_process', 'truemisha_privacy_checkbox_error', 25 );
 
// function truemisha_privacy_checkbox_error() {
 
// 	if ( empty( $_POST[ 'privacy_policy_checkbox' ] ) ) {
// 		wc_add_notice( 'Ваш нужно дать согласие с договором-офертой.', 'error' );
// 	}
 
// }

// // Добавление чекбокса
// add_action( 'woocommerce_register_form', 'truemisha_registration_privacy_checkbox', 25 );
 
// function truemisha_registration_privacy_checkbox() {
 
// 	woocommerce_form_field(
// 		'privacy_policy_reg',
// 		array(
// 			'type'          => 'checkbox',
// 			'class'         => array( 'form-row' ),
// 			'label_class'   => array( 'woocommerce-form__label-for-checkbox' ),
// 			'input_class'   => array( 'woocommerce-form__input-checkbox' ),
// 			'required'      => true,
// 			'label'         => 'Принимаю <a href="/wp-content/uploads/2020/10/Politika-konfidencialnosti.pdf" target="_blank">Политику конфиденциальности</a>',
// 		)
// 	);
 
// }
 
// // Валидация
// add_filter( 'woocommerce_registration_errors', 'truemisha_registration_privacy_checkbox_error', 25 );
 
// function truemisha_registration_privacy_checkbox_error( $errors ) {
 
// 	if( is_checkout() ) {
// 		return $errors;
// 	}
 
// 	if ( empty( $_POST[ 'privacy_policy_reg' ] ) ) {
// 		$errors->add( 'privacy_policy_reg_error', 'Вам нужно согласиться с передачей и обработкой персональных данных.' );
// 	}
 
// 	return $errors;
 
// }
