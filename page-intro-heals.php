<?php
/*
Template Name: page-34 (intro-heals)
*/


 get_header(); ?>

<div class="banner banner-inner page-34">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-logotype wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">Здоровье</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">гормональной системы мужчины и женщины</h2>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s" style="color: #00c0f3">Видео курс</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>Знаете ли вы, как формировать и управлять своими гормонами? <br>От состояния гормональной системы зависит, практически, всё в нашей жизни: <br>работа иммунной и нервной систем, наша способность продолжать род, <br> наша внешность, эмоциональное состояние, успешность и реализованность</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>ВЕДУЩИЙ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro">
    <div class="container">
      <div class="teacher-intro">
          <div class="teacher-name">
            <h2>
              <div class="title__subtext">ЮЛИЯ РОЗЕНСОН</div>
            </h2>
            <span>Психолог, Акушер</span>
          </div>
          
          <span class="teacher-descr">Специализация Семейная, Перинатальная <br>и Нейропсихология. Опыт работы 25 лет. <br>Руководитель проекта родительской культуры «ДАР». <br>Автор проекта «Секреты гормонального здоровья»</span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <p>По большому счету, вся наша жизнь — это игра гормонов.</p>
              <p>По ряду причин, у подавляющего большинства современных мужчин и женщин наблюдаются те или иные отклонения в работе гормональной системы. </p>
              <p>Современная эндокринология предлагает нам только методы гормонозаместительной терапии. </p>
              <p>Зачастую это приводит ещё к большим проблемам со здоровьем и уменьшает продолжительность жизни.</p>
              <span class="course-info__user course-info__user-into">Для каждого человека <br>очень важно знать:</span>
              <div class="divider"></div>
              <ul>
                <li>Виды гормонов и устройство гормональной системы;</li>
                <li>Симптомы и причины гормональных нарушений;</li>
                <li>Влияние гормональной системы на эмоции и настроение.</li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">На курсе, посвященном <br>вашему гормональному балансу, <br>мы разберем следующие темы:</span>
              <div class="divider"></div>
              <ul>
                <li>Что такое гормоны и гормональная система?</li>
                <li>Роль половых гормонов, гормонов щитовидной железы, гормонов надпочечников, гормонов поджелудочной железы, гормонов головного мозга в нашем здоровье;</li>
                <li>Симптомы нарушения гормонального баланса;</li>
                <li>Причины нарушения гормонального баланса;</li>
                <li>Немедикаментозные  методы коррекции гормональных нарушений.</li>
              </ul>  
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full" href="/cart/?add-to-cart=2289">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img22.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img23.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img24.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Онлайн <br>Видео</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">3 темы — 1 видео <br>Всего 2 академ. часа</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Контрольный тест <br>после каждой темы</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Дополнительный <br>курс</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <!-- <table>
                  <tr class="table-header">
                        <td colspan="4">&nbsp;</td>
                  </tr>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Здоровье Гормональной системы мужчины и женщины</span></br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span></br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 3 часа, 11 тем</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Cтоимость</td>
                        <td>&nbsp;</td>
                        <td>900 <a href="/">Купить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </table> -->
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Здоровье Гормональной системы мужчины и женщины</span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 2 часа, 3 темы</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2289"]');?> <a href="/cart/?add-to-cart=2289">купить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>

            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>
    </div>
  </div>
</div>


<div class="course-result-end course-result-end_intro">
  <div class="container">
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><a class="btn btn--full" href="/cart/?add-to-cart=2289">Добро пожаловать</a></div>
  </div>
</div>


<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

var boxes = document.getElementsByClassName('course-info__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>

<?php get_footer(); ?>
