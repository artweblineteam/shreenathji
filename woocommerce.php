<?php get_header(); ?>
<div class="product-template">
	<div class="container">
			<section>
				<article>
				 	<?php woocommerce_content(); ?>
				</article>
			</section>
	</div>
</div>

<?php get_footer(); ?>
