<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$allowed_html = array(
	'a' => array(
		'href' => array(),
	),
);
?>

<h2 class="title__wrapper title-24">
            <div class="title__subtext">Добро пожаловать,<br></div>
             <div class="title__subtext title__light--pink">
             	<?php printf(esc_html( $current_user->display_name ));?>
             </div>
     </h2>

<div class="with-border with-border__long woo-logout">
	<span><?php
	/* translators: 1: user display name 2: logout url */
	printf(
		wp_kses(__( 'Вы не %1$s? <a href="%2$s">Выйти</a>', 'woocommerce' ), $allowed_html ),
		esc_html( $current_user->display_name ),
		esc_url( wc_logout_url() )
	);
?></span>
</div>


<div class="account-additional-info"><p>
	<?php
	/* translators: 1: Orders URL 2: Address URL 3: Account URL. */
	$dashboard_desc = __( 'Из главной страницы аккаунта вы можете посмотреть ваши <a href="%1$s">недавние заказы</a>, или <a href="%3$s">изменить пароль и основную информацию</a>.', 'woocommerce' );
	if ( wc_shipping_enabled() ) {
		/* translators: 1: Orders URL 2: Addresses URL 3: Account URL. */
		$dashboard_desc = __( 'Из главной страницы аккаунта вы можете посмотреть ваши <a href="%1$s">недавние заказы</a>, или <a href="%3$s">изменить пароль и основную информацию</a>.', 'woocommerce' );
	}
	printf(
		wp_kses( $dashboard_desc, $allowed_html ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'edit-account' ) )
	);
	?>
</p>
<p>Если у вас возникли вопросы по работе личного кабинета, пишите на <a href="mailto:info.shreenathjiacademy@gmail.com">info.shreenathjiacademy@gmail.com</a></p>
</div>


<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
