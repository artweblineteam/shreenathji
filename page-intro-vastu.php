<?php
/*
Template Name: page-39 (intro-vastu)
*/


 get_header(); ?>

<div class="banner banner-inner page-24 page-38">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-logotype wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">ЧТО ТАКОЕ ВЕДИЧЕСКАЯ АРХИТЕКТУРА ВАСТУ?</h1>
      <!-- <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">МАЛО РОДИТЬСЯ ЖЕНЩИНОЙ, ВАЖНО СТАТЬ ЕЮ!</h2> -->
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s" style="color: #00c0f3">Видео курс</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>
          Васту — это великая наука священной архитектуры, предназначенная для <br>построения счастливого общества и гармонизации человеческих отношении. <br>
В основе Васту лежит знание и использование тонких законов Вселенной.
        </span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>ВЕДУЩИЙ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro">
    <div class="container">
      <div class="teacher-intro">
          <div class="teacher-name">
            <h2>
              <div class="title__subtext">Ади Шакти</div>
            </h2>
            <span>(Анастасия Королюк)</span>
          </div>
          
          <span class="teacher-descr">Дипломированный наставник Васту. <br>Духовный последователь Джуно Агхары</span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              
                <span class="course-info__user">ВАСТУ ДАЁТ ВОЗМОЖНОСТЬ:</span>
                <div class="divider"></div>
                <ul>
                  <li>Узнать сильные и слабые стороны жилья и получить методы гармонизации</li>
                  <li>Выбрать правильную планировку помещения и расстановку мебели</li>
                  <li>Подобрать цветовое оформление для каждой комнаты в доме</li>
                  <li>Узнать свое особое место в доме, где наиболее полезно и комфортно находиться;</li>
                  <li>Усилить и гармонизировать определенные сферы жизни </li>
                  <li>Увидеть позитивные и негативные стороны своей судьбы и определить путь своего развития</li>
                </ul> 
              
            </div>
          </div>
        </div>
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc">
              <span class="course-info__user">НА КУРСЕ ВАСТУ ВЫ УЗНАЕТЕ:</span>
              <div class="divider"></div>
              <ul>
                <li>Основные понятия науки Васту, её цели и пути практического применения.</li>
                <li>Каким образом работать с энергиями и как очищать дом от негативных энергетических потоков?</li>
                <li>Как сделать жильё уютным, гармоничным, чтобы оно восстанавливало силы человека?</li>
                <li>Как исследовать пространство и определять энергетический центр помещения?</li>
                <li>Научитесь составлять Васту карты и пользоваться активатором энергий планет – янтрами?</li>
                <li>Как и с помощью чего определять индивидуальное место силы в доме?</li>
                <li>Как правильно зонировать, оформлять комнаты, расставлять мебель и располагать зеркала?</li>
              </ul>  
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full " href="/pricelist-single-courses/#love-yourself">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img1.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img2.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img3.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Видео<br>&nbsp;</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">1 тема — 1 видео <br>Всего: 20 минут</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Нет<br>&nbsp;</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Особенность курса — <br>только суть</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">ЧТО ТАКОЕ ВЕДИЧЕСКАЯ АРХИТЕКТУРА ВАСТУ?</span><br>
                              <span class="extra-light-subtitle">Видео</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Видео, 20 минут, 1 тема</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>Бесплатно <p><a href="/cart/?add-to-cart=3833">Изучить</a></p></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody>
          </table>
    </div>
  </div>
</div>


<div class="course-result-end course-result-end_intro">
  <div class="container">
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><a class="btn btn--full" href="/cart/?add-to-cart=3833">Добро пожаловать</a></div>
  </div>
</div>


<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

var boxes = document.getElementsByClassName('course-info__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>

<?php get_footer(); ?>
