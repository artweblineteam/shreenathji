<?php
/*
Template Name: page-29 (intro)
*/


 get_header(); ?>

<div class="banner banner-inner page-29">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-logotype wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">Вводный</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">видео курс Ведической Академии <br>Shreenathji</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>Мироздание и человек   •   Путь к здоровью и счастью   •   Уникальные Знания и Практики</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>ВЕДУЩИЙ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro">
    <div class="container">
      <div class="teacher-intro">
          <div class="teacher-name">
            <h2>
              <div class="title__subtext">Алакх Ниранжан</div>
            </h2>
            <span>Астролог Джйотиш</span>
          </div>
          
          <span class="teacher-descr">Основатель и наставник Академии. <br>В прошлом физик, организатор <br>крупного бизнеса, мастер единоборств</span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <span class="course-info__user">Фундаментальные Знания</span>
              <div class="divider"></div>
              <ul>  
                <li>О Ведах;</li>
                <li>О сознании человека, смысле жизни и путях развития;</li>
                <li>Об устройстве Мироздания, Времени, Пространства;</li>
                <li>Об энергосистеме человека;</li>
                <li>О Судьбе и Карме.</li>
              </ul>  
              <div class="divider"></div>
              <span class="title__light--pink">Базовые практики</span>
              <ul>
                <li>Концентрации и успеха;</li>
                <li>Построения гармоничных отношений;</li>
                <li>Погружение в Ведическую астрологию.</li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">На Курсе вы получите <br>важные жизненные ориентиры</span>
              <div class="divider"></div>
              <ul>
                <li>Веды - сверхзнание;</li>
                <li>Сознание и путь развития человека;</li>
                <li>Причины несчастья;</li>
                <li>Практика концентрации и успеха;</li>
                <li>Формула счастья, 4 шага к реализации и счастью;</li>
                <li>Об устройстве Мироздания, Времени, Пространства;</li>
                <li>Энергосистема человека. Устройство психики и тела;</li>
                <li>Введение в астрологию;</li>
                <li>Время и реализация судьбы, карма;</li>
                <li>Построение натальной карты человека;</li>
                <li>Влияние Вед на лучшие умы человечества;</li>
                <li>Особенности сверхзнания.</li>
              </ul>  
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full" href="/cart/?add-to-cart=2284">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img7.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img8.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img9.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Онлайн <br>Видео</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">8 тем — 8 видео <br>Всего 3 часа</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Нет<br>&nbsp;</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Особенность курса — <br>плотность Знания</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Вводный курс</span><br>
                              <span class="extra-light-subtitle">Онлайн</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 3 часа, 8 тем</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>Бесплатно <p><a href="/cart/?add-to-cart=2284">Изучить</a></p></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>
    </div>
  </div>
</div>


<div class="course-result-end course-result-end_intro">
  <div class="container">
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><a class="btn btn--full" href="/cart/?add-to-cart=2284">Добро пожаловать</a></div>
  </div>
</div>


<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);
</script>

<?php get_footer(); ?>
