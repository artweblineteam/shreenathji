<?php
/*
Template Name: price II
*/


 get_header(); ?>


<div class="title-inner">
    <div class="container">
      <h2 class="text-border__wrapper text-border__wrapper--title">
        <div class="text-border__title text-border__title--title"><span>Цены</span></div>
      </h2>
    </div>
</div>
<div class="price-tabs">
    <div class="container">
    	<div class="price-tabs__list">
    		<a href="/">Факультеты</a>
      		<a href="/">Отдельные курсы</a>
    	</div>
    </div>
</div>

<div class="table-inner table__first">
    <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__h2 title__pink">ОБУЧЕНИЕ НА ОТДЕЛЬНЫХ <br>КУРСАХ АКАДЕМИИ</div>
          </div>
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Вводный курс</span><br>
                              <span class="extra-light-subtitle">Онлайн</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 3 часа, 8 тем</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>Бесплатно <a href="/">Изучить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
      </div>
      <!-- -->
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Что такое Ведическая астрология Джйотиш?</span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 30 минут</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>Бесплатно <a href="/">Изучить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
      </div>
      <!-- -->
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Мировоззрение и гармония сознания</span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 1 час, 6 тем</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>2 000 <a href="/">купить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
      </div>
      <!-- -->
      <!-- -->
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Дхарма, Предназначение, Цели и Путь</span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 3 часа, 11 тем</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>6 000 <a href="/">купить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
      </div>
      <!-- -->
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Биохакинг (целебное питание)</span><br>
                              <span class="extra-light-subtitle">Онлайн марафон</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                                    <tr>
                        <td>8-дневная авторская методика улучшения метаболизма (е-версия)</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Доступ к закрытой группе в fb (можно получить ответы на все вопросы)</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Доступ в чат WhatsApp (для каждой группы)</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                   <tr>
                        <td>Индивидуальная Skype консультация мастера Екатерины Щербаковой</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Ежедневный физический комплекс активации метаболизма Дмитрия Голюнова</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Индивидуальное сопровождение, работа с дневником питания, личная поддержка</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Методичка марафона с актуальными сезонными рецептами и программой</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Рекомендации по питанию после завершения марафона (памятка)</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Предварительные индивидуальные консультации, Е. Щербакова и Д. Голюнов</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Ежедневные обучающие видео. Непосредственное общение с ведущим</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Расшифровка лабораторных анализов. Письменные рекомендации по питанию</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Стоимость</td>
                        <td>5 000 <a href="/cart/?add-to-cart=2213">Купить</a></td>
                        <td>6 000 <a href="/cart/?add-to-cart=2214">Купить</a></td>
                        <td>20 000 <a href="/cart/?add-to-cart=2215">Купить</a></td>
                  </tr>
            </tbody></table>
      </div>
      <!-- -->
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Здоровье Гормональной системы мужчины и женщины</span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 2 часа, 3 темы</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>900 <a href="/">купить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
      </div>
      <!-- -->
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Тренинг для женщин  «Полюби себя»</span><br>
                              <span class="extra-light-subtitle">Онлайн марафон</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн-чат в WhatsApp</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Сопровождение помощника ведущей</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Личное сопровождение Татьяны Качина</td>
                        <td>-</td>
                        <td>&nbsp;</td>
                        <td><i class="fas fa-check"></i></td>
                  </tr>
                  <tr>
                        <td>Стоимость</td>
                        <td>&nbsp;</td>
                        <td>3 900 <a href="/">купить</a></td>
                        <td>5 900 <a href="/">купить</a></td>
                  </tr>
            </tbody></table>
      </div> 
      <!-- -->
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Подготовка, беременность и рождение ребёнка</span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 4,5 часа, 6 тем</td>
                        <td>-</td>
                        <td><i class="fas fa-check"></i></td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>8 000 <a href="/">купить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
      </div>
    </div>
</div>




  
<?php get_footer(); ?>