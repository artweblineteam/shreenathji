<?php
/**
 * Student Account - View Courses.
 *
 * This template can be overridden by copying it to yourtheme/wp-courseware/account/account-view-courses.php.
 *
 * @package WPCW
 * @subpackage Templates\Account
 * @version 4.3.0
 *
 * Variables available in this template:
 * ---------------------------------------------------
 * @var array $courses The array of student courses.
 * @var int $current_page The current page of the student courses.
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

$courses_columns = apply_filters( 'wpcw_student_account_courses_columns', array(
	'course-title'    => esc_html__( 'Course', 'wp-courseware' ),
	'course-progress' => esc_html__( 'Progress', 'wp-courseware' ),
) );

if ( $courses ) : ?>
	<?php echo do_shortcode( '[wpcw_course_progress user_progress="true" user_grade="false"]' ); ?>
<?php else : ?>
	<?php wpcw_print_notice( sprintf( __( 'У вас еще нет ни одного курса. <a href="/fakultety/">Выбрать курсы</a>', 'wp-courseware' ), wpcw_get_page_permalink( 'courses' ) ), 'info' ); ?>
<?php endif; ?>


<script type="text/javascript">
	$(".wpcw_fe_course_progress_row").click(function(e) {
  		e.preventDefault();
  		$("wpcw_fe_course_progress_row").removeClass('active');
  	$(this).toggleClass('active');
})
</script>