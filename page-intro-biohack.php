<?php
/*
Template Name: page-33 (intro-bio)
*/


 get_header(); ?>

<div class="banner banner-inner page-32">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-trip wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">BIOHACKING UPGRADE</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">Как в 45 лет чувствовать себя на 30 и <br>продлить жизнь, прилагая минимум усилий?</h2>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s" style="color: #00c0f3">ОБУЧАЮЩИЙ ОНЛАЙН КУРС</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>Комплексный 8-дневный Обучающий Курс, направленный на оптимизацию  <br>человеческого организма. Каждый Курс Biohacking Upgrade уникален  <br>и посвящен определенному аспекту поддержания здоровья</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн марафон<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>ВЕДУЩИЙ ЭКСПЕРТ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro biohack">
    <div class="container">
      <div class="teacher-intro">
          <div class="teacher-name">
            <h2 class="title__subtext">
              Екатерина Щербакова
            </h2>
          </div>
          
          <span class="teacher-descr">Биохакер, диетолог, ученый, к.н.</span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1 block_height">
          <div class="course-info__item">
            <div class="course-info__desc">
              <span class="course-info__user">В теории и на практике <br>раскрываем темы</span>
              <div class="divider"></div>
              <ul>
                <li>Белки, жиры, углеводы – ценность, источники. Метаболизм;</li>
                <li>Пища: витамины, минералы, важные микронутриенты;</li>
                <li>Основные дефициты в питании и как их контролировать?</li>
                <li>Как оптимизировать свой рацион по БЖУ?</li>
                <li>Питание и циркадные ритмы;</li>
                <li>Индивидуальные схемы питания и голодания;</li>
                <li>Микробиота кишечника. Как поддерживать и почему важно? </li>
                <li>Основные способы контроля метаболизма (снижение веса);</li>
                <li>Что такое detox? Как поддерживать детоксикацию организма?</li>
                <li>Физическая нагрузка для здоровья и улучшения метаболизма.</li>
              </ul>
              <span class="course-info__user margin-top">Польза за 8 дней марафона</span>
              <div class="divider"></div>
              <ul>
                <li>Удаление клеточного “мусора”; </li>
                <li>Активизация Аnti-age клеточных сигнальных путей;</li>
                <li>Нормализация жирового и углеводного обмена;</li>
                <li>Выведение лишней воды;</li>
                <li>Детоксикация;</li>
                <li>Нормализация работы кишечника;</li>
                <li>Восстановление микробиоты кишечника;</li>
                <li>Восстановление электролитного баланса;</li>
                <li>Обогащение организма ценными микронутриентами;</li>
                <li>Восстановление циркадных ритмов и нормализация сна.</li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1 block_height">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">Этот марафон для вас, если:</span>
              <div class="divider"></div>
              <ul>
                <li>По утрам тяжело просыпаться, нет энергии вставать с кровати;</li>
                <li>Понимаете, что надо следить за своим здоровьем, но запутались в питании;</li>
                <li>Постоянно находитесь в стрессе, легко выходите из себя;</li>
                <li>Сложно концентрироваться, стала часто подводить память;</li>
                <li>У родителей были серьезные болезни; вы боитесь, что они могут появиться у вас;</li>
                <li>В попытке похудеть перепробовали много диет, но ничего не работает;</li>
                <li>Хотите больше времени проводить с семьей, но на это не хватает сил;</li>
                <li>Нужны практические навыки, как держать организм в тонусе каждый день;</li>
                <li>Хотите улучшить самочувствие и добиваться успеха в бизнесе.</li>
              </ul>
              <span class="course-info__user margin-top">Мы ждем тех, кто хочет научиться управлять своей жизнью и получать </span>
                
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full" href="/pricelist-single-courses/#biohack">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img19.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img20.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img21.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Онлайн <br>Марафон</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Всего 8 дней <br>&nbsp;</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Диетолог, helth-coach,  <br>администратор, эксперт</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Курс является проектом <br>Клуба GoodTrip</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Биохакинг </span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«TAKE AWAY»</td>
                        <td>«STANDARD LIGHT»</td>
                        <td>«STANDARD»</td>
                  </tr>
                                    <tr>
                        <td>8-дневная авторская методика улучшения метаболизма (е-версия)</td>
                        <td>да</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                  </tr>
                  <tr>
                        <td>Доступ к закрытой группе в fb (можно получить ответы на все вопросы)</td>
                        <td>да</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                  </tr>
                  <!-- <tr>
                        <td>Доступ в чат WhatsApp (для каждой группы)</td>
                        <td>да</td>
                        <td>да</td>
                        <td>да</td>
                  </tr> -->
                   <tr>
                        <td>Индивидуальная Skype консультация эксперта Екатерины Щербаковой</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Ежедневный физический комплекс активации метаболизма</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Индивидуальное сопровождение, работа с дневником питания, личная поддержка</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Методичка марафона с актуальными сезонными рецептами и программой</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <!-- <tr>
                        <td>Рекомендации по питанию после завершения марафона (памятка)</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                        <td>да</td>
                  </tr> -->
                  <tr>
                        <td>Предварительные индивидуальные консультации экспертов Курса</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Непосредственное общение с ведущим</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Расшифровка лабораторных анализов. Письменные рекомендации по питанию</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Стоимость</td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2213"]');?></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2214"]');?></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2215"]');?></td>
                  </tr>
                  
            </tbody></table>
            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>
    </div>
  </div>
</div>


<div class="course-result-end course-result-end_intro">
  <div class="container">
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><a class="btn btn--full" href="/pricelist-single-courses/#biohack">Добро пожаловать</a></div>
  </div>
</div>


<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

var boxes = document.getElementsByClassName('course-info__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>

<?php get_footer(); ?>
