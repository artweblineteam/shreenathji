<?php
/*
Template Name: page-30 (intro-juotisha)
*/


 get_header(); ?>

<div class="banner banner-inner page-30">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-logotype wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">Что такое Ведическая астрология</h2>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">Джйотиш?</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s" style="color: #00c0f3">Видео курс</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>Джйотиш — «Божественный свет» — древняя, авторитетная духовная наука. <br>Звездная астрология уже тысячи лет формирует предсказания с точностью более 80% <br>и демонстрирует на практике, как работает современная «Теория полей»</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>ВЕДУЩИЙ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro">
    <div class="container">
      <div class="teacher-intro">
          <div class="teacher-name">
            <h2>
              <div class="title__subtext">Алакх Ниранжан</div>
            </h2>
            <span>Астролог Джйотиш</span>
          </div>
          
          <span class="teacher-descr">Основатель и наставник Академии. <br>В прошлом физик, организатор <br>крупного бизнеса, мастер единоборств</span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <span class="course-info__user">Джйотиш даёт возможность</span>
              <div class="divider"></div>
              <ul>  
                <li>Раскрыть суть Дхармы - предназначение, призвание, долг;</li>
                <li>Показать способы изменения Кармы и судьбы (Упаи);</li>
                <li>Определить лучшее время (Мухурту) для начала  действия;</li>
                <li>Определить последствия того или иного действия;</li>
                <li>Ответить на любой вопрос.</li>
              </ul>  
              <span class="course-info__user course-info__user-into">Базовые практики</span>
              <div class="divider"></div>
              <p>Для мастера Джйотиш жизнь — часть единого бытия, это позволяет не только увидеть будущее и прошлое, но и указать, как влиять на них.</p>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">На Курсе о Джйотиш вы узнаете:</span>
              <div class="divider"></div>
              <ul>
                <li>Почему человек рождается в данной семье, в определенном месте и моменте времени?</li>
                <li> Как связано Мироздание, Карма и Судьба человека?</li>
                <li>Что способна показать астрология?</li>
                <li>Какие существуют методы влияния на судьбу?</li>
                <li>Каков смысл жизни человека?</li>
                <li>Какие способы достижения человеком счастья?</li>
                <li>Кто такой Ведический астролог?</li>
              </ul>  
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full" href="/cart/?add-to-cart=2287">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img10.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img11.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img12.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Онлайн <br>Видео</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">1 тема — 1 видео <br>Всего 30 мин</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Нет<br>&nbsp;</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Курс является частью <br>I ступени Академии</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <!-- <table>
                  <tr class="table-header">
                        <td colspan="4">&nbsp;</td>
                  </tr>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Что такое Ведическая астрология Джйотиш?</span></br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span></br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 3 часа, 8 тем</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>Бесплатно <a href="/">Изучить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </table> -->
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Что такое Ведическая астрология Джйотиш?</span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 30 минут</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>Бесплатно <p><a href="/cart/?add-to-cart=2287">Изучить</a></p></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>
    </div>
  </div>
</div>


<div class="course-result-end course-result-end_intro">
  <div class="container">
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><a class="btn btn--full" href="/cart/?add-to-cart=2287">Добро пожаловать</a></div>
  </div>
</div>


<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);
</script>

<?php get_footer(); ?>
