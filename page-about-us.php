<?php
/*
Template Name: About Us
*/


 get_header(); ?>


<div class="title-inner about-us">
    <div class="container">
      <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Академия ШРИНАТДЖИ</span></div>
            <div class="title__subtext title-24">ВЕДа, АСТРОЛОГИЯ ДЖЙОТИШ,  <br>КЛАССИЧЕСКАЯ ЙОГА, ВАСТУ И АЮРВЕДА</div>
     </h2>
    </div>
</div>

<div class="o-nas">
      <div class="container">
            <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
                  <div class="title__main with-border with-border__long title__light--pink title-24"><span>О нас</span></div>
            </div>
            <div class="o-nas-list grid grid-top">
                  <div class="o-nas-list-left col-1-2 col-sm-1-1">
                        <p>Вас приветствует команда Академии Ведической Астрологии, Йоги, Васту и Джйотиш «Шринатджи».</p>
                        <p>Все наставники являются проводниками исконных школ древнего Ведического знания. Каждый преподаватель получил посвящение и Знание в своей Парампаре — цепи ученической преемственности.</p>
                        <p>Вы можете найти сотни образовательных курсов, школ, университетов. Большинство из них приносит несомненную пользу. Каждый курс или программа хороши в своей области, но они описывают лишь часть жизни человека. </p>
                        <p>В этом мире существует совершенное всеохватывающее знание — это Веды. Это не религия и не эзотерика, а точная наука. На санскрите Веды означает Знание. За последние 5000 лет Ведические Знания были рассеяны на множество философий, школ, течений и наук – от Логики, Материализма, Дао до Буддизма и Веданты.</p>
                  </div>
                  <div class="o-nas-list-right col-1-2 col-sm-1-1">
                        <p>Наши наставники объединили эти частицы знания и создали фундаментальную четырехуровневую программу, которая объединила многие школы: </p>
                        <ul>
                              <li> Веданты
                              <li> Йоги</li>
                              <li> Дао</li>
                              <li> Буддизма</li>
                              <li> Ведической астрологии Джйотиш</li>
                              <li> Аюрведы</li>
                              <li> Васту</li>
                              <li> Нейрофизиологии</li>
                              <li> Психологии </li>
                              <li> Антропологии и других наук</li>
                        </ul>
                  </div>
            </div>
      </div>
</div>

<div class="academy-mission">
      <div class="container">
            <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
                  <div class="title__main with-border with-border__long title__light--pink title-24"><span>Миссия академии</span></div>
            </div>
            <span>Передать человечеству объединенное Знание древнего Ведического и постведического наследия. <br>Предоставить каждому студенту нашей школы возможность обрести полное Мировоззрение <br>и получить достойную высоковостребованную профессию.
            </span>
      </div>
</div>

<div class="vedu-name">
      <div class="container">
            <h3 class="title__wrapper title__light-no-bottom-margin">Ведическая Академия <br>получила имя и благословение наших учителей:</h3>
            <span class="vedu-name-color-subtitle">Shree Nath Ji (Шринаджи), что означает «Великий владыка, дающий благо» </span>
            <span class="vedu-name-subtitle">(см. Википедиа – Vedic Academy «Shreenathji»)</span>
      </div>
</div>
  
<div class="our-teachers">
      <div class="container">
            <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
                  <div class="title__main with-border with-border__long title-24"><span>Наши НАСТАВНИКИ</span></div>
            </div>
            <div class="our-teachers__list">
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_an.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Алакх Ниранжан</span>
                              <div class="teachers__skills">Ведическая астрология (Джйотиш) <br>Психология. Фундаментальные знания</div>
                              <div class="teachers__info">
                                    <p>Философ, Физик, Психолог, Антрополог, Вайшнав. Астролог Джйотиш (оказал помощь тысячам искателей). Ученик Джйотиш Ачария Шри Раджендра Бушан Госвами Махарадж. В прошлом мастер боевых искусств, судья, член сборной страны (каратэ, кунг-фу). Инженер и лектор, организатор успешных крупных бизнеспроектов. Основатель Ведической школы «Jyoti Peeth» и Ведической академии Астрологии, Йоги, Васту и Аюрведы «Шринатджи». Автор книги «Наследие Ведической культуры».</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_va.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Вина Ачария</span>
                              <div class="teachers__skills">Классическая Йога <br>Мантра-видья, медитация</div>
                              <div class="teachers__info">
                                    <p>Родилась в семье браминов древнейшего в Индии храма Кришны в Гималаях. Изучала Санскрит. Окончила Бихарскую Школу Йоги. Окончила Джайн Вишва Бхарати Институт. Степень мастера Science of Living (Наука жизни). Провела сотни семинаров по всему миру. Вдохновленная примером своего Гуру Свами Ниранджананда Сарасвати основала Йога ашрам Yoga Dham Ashram (yogadham.in) в Наггаре, Гималаи, Индия.</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_sh.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Шиям Самвел</span>
                              <div class="teachers__skills">Гьяна-йога. Раджа-йога. Теология. <br>Энергосистема человека. </div>
                              <div class="teachers__info">
                                    <p>Монах, Отрешенный (Садху). Мастер йоги Знания и Концентрации.</p> 
                                    <p>Более 15 лет живёт в святых местах Индии (Бриджабаси). Изучает йогу любви к Всевышнему (Раджа-йогу), восточную философию, психологию. Доступно поможет осознать суть духовного пути и место Всевышнего в жизни каждого.</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_ap.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Алак Пури Махарадж Садху Баба</span>
                              <div class="teachers__skills">Раджа-Йога. </div>
                              <div class="teachers__info">
                                    <p>20 лет практикует йогу и медитацию. В 50 отрёкся от мира. Обрёл статус Махарадж. Имеет научное образование и степень по философии. Живёт в Индии, в Гималаях. Принял посвящение в садху (монахи) священного ордена йогинов Juna Akhara. Использует средства йоги для помощи в самореализации и для духовного роста. До посвящения работал на руководящих должностях в крупных компаниях в Москве. Создатель философии «Гуд Трип». Основатель Good trip club. Десятки учеников.</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_ud.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Уджвала Ниламани</span>
                              <div class="teachers__skills">Аюрведа. Васту. Теология. <br>Фундаментальные знания. </div>
                              <div class="teachers__info">
                                    <p>Философ, Теолог, Доктор Аюрведы (более 20 лет стажа). <br>Ученик Баннаньджи Говинда пандит Ачарйа и Ваидинатх Шастри. <br>Известный художник.</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_bb.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Бикаш Баба</span>
                              <div class="teachers__skills">Аштанга-Йога, Тайчи, <br>Цигун, Даосские практики </div>
                              <div class="teachers__info">
                                    <p>Йогин. Йога-терапевт. Шивананда йога, Тибетская йога Туммо. Мастер карате, Тайчи и Цигун. Опыт преподавания более 15 лет. Закончил ГДОИФК П.А. Лесгафта, специализация — «Адаптивная физическая культура для лиц среднего и старшего возраста». Обучался в Институте йоги и санскрита в г. Вриндаван, Индия. Духовный наставник Берендрананда Свами, ученик Шивананды. Использует опыт Шри Кришнамачарья, Б.К.С.Айенгар, Патабхи Джойс и др. Создатель Арт-йоги, соединившей элементы Даосских практик и Йоги.</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_ak.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Анна Казарян</span>
                              <div class="teachers__skills">Йога, массаж, <br>дыхательные практики</div>
                              <div class="teachers__info">
                                    <p>Ведический консультант. Психология кризисов. <br>Специалист «Ошо ребалансинг». <br>Мастер массажа Кобидо. Выпускница академии Шринатджи.</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_as.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Ади Шакти</span>
                              <div class="teachers__skills">Васту архитектура и дизайн, <br>васту-консультант. Бизнес-тренер</div>
                              <div class="teachers__info">
                                    <p>Высшее экономическое образование. <br>Закончила Лондонскую Академию Васту. Ученица Ася-Леана Мигдаль. Последовательница Махайог Пайлота Бабы.</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_es.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Екатерина Щербакова</span>
                              <div class="teachers__skills">Биохакинг <br>Законы питания. Живая диета</div>
                              <div class="teachers__info">
                                    <p>Биохимик, специалист по питанию. Писатель. Потомственный биолог. Закончила Биологический факультет МГУ им. Ломоносова. Научный сотрудник НИИ Канцерогенеза Онкологического Научного Центра РАМН. Более 15 лет исследует биохимию питания, физиологию, средства оптимизации работы человеческого организма и продление жизни. Автор многих работ и программ по научному подходу к питанию.</p>
                              </div>
                        </div>
                  </div>
                  <div class="teachers__item">
                        <div class="teachers__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_yr.png);"></div>

                        <div class="teachers__desc">
                              <span class="teachers__user_title" href="/">Юлия Розенсон</span>
                              <div class="teachers__skills">Психология, подготовка к беременности <br>и рождению детей</div>
                              <div class="teachers__info">
                                    <p>Психолог, Акушер. Закончила УГУ и МиКПП Специализация: Семейная, Перинатальная и Нейропсихология. Организатор и руководитель проекта родительской культуры «ДАР» в г. Екатеринбург. Автор проекта «Секреты гормонального здоровья». Опыт работы 25 лет.</p>
                              </div>
                        </div>
                  </div>
            </div>
      </div>
</div>

<div class="supervisors supervisors-about-us">
    <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
                  <div class="title__main with-border with-border__long title-24"><span>Руководители И НАСТАВНИКИ</span></div>
            </div>
      <div class="supervisors__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <div class="supervisors__item">
            <div class="supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_tk.png);">
      </div>

            <div class="supervisors__desc"><a class="supervisors__user" href="#null">ТАТЬЯНА КАЧИНА</a>
              <div class="supervisors__info">Коуч, Женский тренер. Выпускник академии «Shreenathji». Училась у Садхгуру, Кехо, Диспенза, Тальписа, Зелинского, Роббинса, Правдиной. Очные тренинги в России, Испании, Франции, Италии</div>
            </div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <div class="supervisors__item">
            <div class="supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_mn.png);">
      </div>

            <div class="supervisors__desc"><a class="supervisors__user" href="#null">МАРИНА НИКИТИНА</a>
              <div class="supervisors__info">Лектор и наставник йоги. Ведический Астролог. Выпускник Ведической академии «Shreenathji». Мама троих детей. Организатор Клуба ЗОЖ в Атырау, Казахстан. Основатель компании «Хорошее настроение»</div>
            </div>
          </div>
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <div class="supervisors__item">
            <div class="supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_kk.png);">
      </div>

            <div class="supervisors__desc"><a class="supervisors__user" href="#null">КРИСТИНА КЛИМЕНЧУК</a>
              <div class="supervisors__info">Трансперсональный ведический психолог, гипнолог, энерготерапевт, <br>коуч, гештальт-терапевт, <br>выпускник академии «Shreenathji», мастер Кундалини йоги</div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="soul-supervisors">
      <div class="container">
            <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
                  <div class="title__main with-border with-border__long title-24"><span>Духовные наставники академии</span></div>
            </div>
            <div class="soul-supervisors__list grid grid-top">
               <div class="col-1-2 col-sm-1-1">
                        <div class="soul-supervisors__item">
                          <div class="soul-supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_soul4.png);"></div>
                          <div class="soul-supervisors__desc">
                            <div class="soul-supervisors__name">Шри Раджендра Бушан Госвами Махарадж</div>
                          </div>
                          
                        </div>
                        <div class="soul-supervisors__item__skills">
                              <p>Джйотиш Ачарий. Вайшнав. Хранитель Рудра Сампрадайи, в которую входят Вишнусвами, Валлабха Ачарий и многие другие. Потомственный пандит в 21-м поколении.</p>
                        </div>
                </div>
                <div class="col-1-2 col-sm-1-1">
                        <div class="soul-supervisors__item">
                          <div class="soul-supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_soul1.png);"></div>
                          <div class="soul-supervisors__desc">
                            <div class="soul-supervisors__name">Гуру Баннаньджи Говинда Пандит Ачарйа</div>
                          </div>
                        </div>
                        <div class="soul-supervisors__item__skills">
                              <p>Величайший знаток Санскрита современности. Хранитель Хамса (Брахма) Сампрадайи, в которую входили Чатурмукха Брахмā, Дурваса, Гаруд̣а-вāхана, МадхвАчАрйа и другие великие личности.</p>
                        </div>
                  </div>
                  <div class="col-1-2 col-sm-1-1">
                        <div class="soul-supervisors__item">
                          <div class="soul-supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_soul5.png);"></div>
                          <div class="soul-supervisors__desc">
                            <div class="soul-supervisors__name">СВАМИ НИРАНДЖАНАНАНДА САРАСВАТИ</div>
                          </div>
                          
                        </div>
                        <div class="soul-supervisors__item__skills">
                              <p>Принял парамахамса саньясу от Гуру Свами Сатьянанда. Руководитель Бихарской школы йоги, Шивананда-матха, Фонда Исследований Йоги. Основал Йога-центры в Европе, Австралии, Северной и Южной Америке. Основал Джайн Вишва Бхарати Институт. Автор более двадцати книг по йоге, тантре и упанишадам.</p>
                        </div>
                  </div>
                  <div class="col-1-2 col-sm-1-1">
                        <div class="soul-supervisors__item">
                          <div class="soul-supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_soul6.png);"></div>
                          <div class="soul-supervisors__desc">
                            <div class="soul-supervisors__name">МАХАРАДЖ БХАРАТ ПУРИ САДХУ БАБА</div>
                          </div>
                          
                        </div>
                        <div class="soul-supervisors__item__skills">
                              <p>Монах древнего ордена Juna Akhara более 50 лет. Благодаря практики Раджа-Йоги достиг просветления. Настоятель храма Господа Шивы в Олд Дели.</p>
                        </div>
                  </div>
                  <div class="col-1-2 col-sm-1-1">
                        <div class="soul-supervisors__item">
                          <div class="soul-supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_soul7.png);"></div>
                          <div class="soul-supervisors__desc">
                            <div class="soul-supervisors__name">СОМНАТХ ГИРИДЖИ  (ПАЙЛОТ БАБА)</div>
                          </div>
                          
                        </div>
                        <div class="soul-supervisors__item__skills">
                              <p>Представитель ордена Juna Akhara. Бывший военный летчик. Последователь Крия – Йоги. Под руководством Ученик Хари Бабы и Аватара Бабы. Постиг науку Самадхи.</p>
                        </div>
                  </div>
                  <div class="col-1-2 col-sm-1-1">
                        <div class="soul-supervisors__item">
                          <div class="soul-supervisors__img" style="background-image: url(<?php bloginfo("template_directory"); ?>/assets/img/about_soul3.png);"></div>
                          <div class="soul-supervisors__desc">
                            <div class="soul-supervisors__name">Ваидинатх Шастри</div>
                          </div>
                          
                        </div>
                        <div class="soul-supervisors__item__skills">
                              <p>Потомственный лекарь царского семейства в Варанаси. Представитель парампары, берущей начало от Брахми Праджтиапа, Ащвини, Дханвантари из Кащи и других прославленных лекарей.</p>
                        </div>
                  </div>   
            </div>
      </div>
</div>
<?php get_footer(); ?>