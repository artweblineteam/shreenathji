<?php
/*
Template Name: Course
*/


 get_header(); ?>

      
      <div class="course-banner">
        <div class="course-first-back"></div>
        <div class="container">
          <div class="banner__logo">
            <div class="course-first-logo wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
            <h1 class="course-banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">Курс I</h1>
            <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">Предназначение, энергия, отношения<Br>три смысла жизни «Сознание Брахмы»</h2>
          </div>
          <div class="course-banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
            <div class="text__blue text__romul text__40 margin-bottom20">Творец</div>
            <div class="text__16 margin-bottom20"><span class="span-margin10">Веды</span> &#8226; <span class="span-margin10">Астрология Джйотиш</span> &#8226; <span class="span-margin10">Йога</span> &#8226; <span class="span-margin10">Призвание</span> &#8226; <span class="span-margin10">Отношения</span> &#8226; <span class="span-margin10">Здоровье</span></div>
          </div>
          <div class="text__blue text__c">Единый фундаментальный курс Ведического образования</div>
        </div>
      </div>
      <div class="course__info background-gray">
        <div class="container">
          <div class="grid grid-top grid-nomargin-top padding-top100">
            <div class="col-1-2 col-sm-1-1 margin-bottom20">
              <div class="course__info--item">
                <div class="text__18 text__romul text__pink">Знания Веды, Джйотиш и Йоги</div>
                <div class="line-divider"></div>
                <ul class="list-small-dots">
                  <li>Всё это древнейшие науки, которым более 5000 лет;</li>
                  <li>Из них вышли все религии и философии: Дао, Дзен, Каббала и пр.;</li>
                  <li>Все коучи мира черпают знания в какой-то из них;</li>
                </ul>
                <p>Но в академии вы получите их все вместе!</p>
                <div class="line-divider"></div>
                <div class="text__pink text__18">Если вы:</div>
                <ul class="course-objective-list list-small-dots">
                  <li>Хотите уверенности в своём завтра;</li>
                  <li>Хотите обрести смысл жизни;</li>
                  <li>Хотите преуспеть в бизнесе;</li>
                  <li>Хотите создать лучшие для себя отношения;</li>
                  <li>Хотите растить счастливых и успешных детей;</li>
                  <li>Хотите подружиться со своим телом;</li>
                  <li>Хотите творить свою жизнь в легкости и радости;</li>
                </ul>
                <div class="text__pink text__18">Этот курс для вас!</div>
              </div>
            </div>
            <div class="col-1-2 col-sm-1-1">
              <div class="course__info--item">
                <div class="text__18 text__romul text__pink">Вы обретёте</div>
                <div class="line-divider"></div>
                <ul class="list-small-dots">
                  <li>Смысл жизни предназначение и цель;</li>
                  <li>Преодолеете одиночество и лень;</li>
                  <li>Успех, энергию, гармоничные отношения;</li>
                  <li>Знание законов времени и судьбы;</li>
                  <li>Фундамент профессионального Ведического обучения;</li>
                </ul>
                <div class="line-divider"></div>
                <div class="text__pink text__18 margin-bottom10">Курс передает знания трех факультетов:</div>
                <div class="text__pink text__16">«Искусство жизни, Веды, практическая психология»:</div>
                <ul class="list-small-dots">
                  <li>Мироздание, энергосистема человека, предназначение и цели;</li>
                  <li>Судьба, теория успеха, психология отношений и синергия;</li>
                </ul>
                <div class="text__pink text__16">Факультет «Классическая йога и йогатерапия»:</div>
                <ul class="list-small-dots">
                  <li>Основы классической йоги и практики очищения;</li>
                  <li>Утренний комплекс. Навыки здорового образа жизни;</li>
                </ul>
                <div class="text__pink text__16">Факультет «Ведическая астрология Джйотиш»:</div>
                <ul class="list-small-dots">
                  <li>Введение в астрологию. Базовые знания Джйотиш (I ступень;</li>
                  <li>Панчанга, мухурта (Выбор благоприятного времени).;</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="btn__wrapper btn__wrapper--center with-border1 with-border1__long"><a class="btn btn--full min-width245">Записаться на курс</a></div>
        </div>
      </div>
    
   

<?php get_footer(); ?>
