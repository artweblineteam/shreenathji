<?php
/*
Template Name: page-41 (intro-happy-rel)
*/


 get_header(); ?>

<div class="banner banner-inner page-35">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-logotype wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">Счастливые отношения</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">МАЛО РОДИТЬСЯ ЖЕНЩИНОЙ, ВАЖНО СТАТЬ ЕЮ!</h2>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s" style="color: #00c0f3">Онлайн-тренинг для женщин</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>    Вы когда-нибудь задумывались о том, что такое женское предназначение <br>и в чем женские приоритеты отличаются от мужских?</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>ВЕДУЩИЙ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro">
    <div class="container">
      <div class="teacher-intro">
          <div class="teacher-name">
            <h2>
              <div class="title__subtext">ТАТЬЯНА КАЧИНА</div>
            </h2>
            <span>Коуч, мастер предназначения, психолог</span>
          </div>
          
          <span class="teacher-descr">Выпускник и наставник академии «Shreenathji». <br>Училась у Садхгуру, Кехо, Диспенза, Тальписа, Зелинского, <br>Роббинса, Правдиной. Очные тренинги в России, Испании, <br>Франции, Италии. Более 1500 студентов по всему миру</span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <div class="course-info__desc"><span class="course-info__user">Пришло время осознать::</span>
              <div class="divider"></div>
              <ul>
                <li>Что означает на практике «принять себя»;</li>
                <li>Как совмещать социальную реализацию с отношениями и семьей?</li>
                <li>Как перестать уставать и начать жить счастливо в ресурсном женском состоянии?</li>
              </ul> 
              <span class="course-info__user course-info__user-into">Мы предлагаем УДОБНЫЙ <br>формат тренинга в WhatsApp:</span>
              <div class="divider"></div>
              <ul>
                <li>Задания приходят утром, вы делаете их в удобное время;</li>
                <li>Задания проверяет личный куратор либо ведущая тренинга (зависит от уровня сложности);</li>
                <li>Каждое задание прорабатывается индивидуально с каждым участником;</li>
                <li>Специальная Группа в WhatsApp c постоянной активностью на протяжении всего марафона;</li>
                <li>Сопровождение индивидуальное — онлайн;</li>
                <li>Индивидуальная консультация после тренинга.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">Марафон раскрывает <br>следующие темы:</span>
              <div class="divider"></div>
              <ul>
                <li>Учимся осознавать пройденный путь;</li>
                <li>Открываем сердце;</li>
                <li>Распознаем природу наших желаний;</li>
                <li>Поймем отличие женского типа мышления от мужского;</li>
                <li>Узнаем об энергетических центрах человека — чакрах;</li>
                <li>Узнаем, как накапливать энергию и быть по-настоящему счастливой и любимой;</li>
                <li>Прокачаем свой сексуальный центр;</li>
                <li>Узнаем древние техники исцеления матки;</li>
                <li>Узнаем секреты гаремных техник;</li>
                <li>Обретем рабочий инструмент исполнения желаний.</li>
              </ul>  
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full " href="/pricelist-single-courses/#love-yourself">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img25.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img26.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img27.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Онлайн <br>Марафон</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Всего 21 день<br>&nbsp;</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Проработка заданий с ведущим/помощником</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Группа в Whats App<br>&nbsp;</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Тренинг для женщин  «Полюби себя»</span><br>
                              <span class="extra-light-subtitle">Онлайн марафон</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн-чат в WhatsApp</td>
                        <td>-</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Сопровождение помощника ведущей</td>
                        <td>-</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Личное сопровождение Татьяны Качина</td>
                        <td>-</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Стоимость</td>
                        <td>&nbsp;</td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2302"]');?> <a href="/cart/?add-to-cart=2302">купить</a></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2301"]');?> <a href="/cart/?add-to-cart=2301">купить</a></td>
                  </tr>
            </tbody></table>
            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>
    </div>
  </div>
</div>


<div class="course-result-end course-result-end_intro">
  <div class="container">
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><a class="btn btn--full" href="/pricelist-single-courses/#love-yourself">Добро пожаловать</a></div>
  </div>
</div>


<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

var boxes = document.getElementsByClassName('course-info__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>

<?php get_footer(); ?>
