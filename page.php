<?php get_header(); ?>

<section>
	<article>
		<div class="container">
			<?php if (have_posts()): while (have_posts()): the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</div>
	</article>
</section>

<?php get_footer(); ?>
