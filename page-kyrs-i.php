<?php
/*
Template Name: page-18 (kyrs I)
*/


 get_header(); ?>

<div class="banner banner-inner page-25">
  <div class="container">
    <div class="banner__logo">
      <div class="logo_one wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">курс I</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">ПРЕДНАЗНАЧЕНИЕ, ЭНЕРГИЯ, ОТНОШЕНИЯ <br>ТРИ СМЫСЛА ЖИЗНИ «СОЗНАНИЕ БРАХМЫ»</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <h2 class="title__wrapper title__wrapper--how" >
            <div class="title__main">творец</div>
          </h2>
      <div class="banner__text-subtitle">
        <span>Веды   •   Астрология Джйотиш   •   Йога   •   Призвание   •   Отношения   •   Здоровье</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Единый фундаментальный курс Ведического образования</a>
    </div>
  </div>
</div>

<div class="mentors">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main">Опытные</div>
            <div class="title__main with-border"><span>Наставники</span></div>
            <div class="title__subtext">ИЗ РАЗНЫХ ДРЕВНЕЙШИХ ВЕДИЧЕСКИХ ШКОЛ <br> ПОДЕЛЯТСЯ С ВАМИ ЗНАНИЯМИ</div>
     </div>
  <div class="mentors-city-list grid grid-top">
    <div class="col-1-5 col-sm-1-2 col-xs-1-1">
      <div class="mentors-list">
      <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/small_lotous.png">
       <div class="title__subtext title__light--pink">Онлайн</div>
       <ul>
         <li>Кристина Дхарма</li>
         <li>Марина Никитина</li>
         <li>Вина Ачария</li>
         <li>Алакх Ниранжан</li>
       </ul>
    </div>
    </div>
    <div class="col-1-5 col-sm-1-2 col-xs-1-1">
      <div class="mentors-list">
      <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/small_lotous.png">
       <div class="title__subtext title__light--pink">Индия</div>
       <ul>
         <li>Самвел Саакян</li>
         <li>Вина Ачария</li>
         <li>Алакх Ниранжан</li>
       </ul>
     </div>
    </div>
    <div class="col-1-5 col-sm-1-2 col-xs-1-1">
      <div class="mentors-list">
      <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/small_lotous.png">
       <div class="title__subtext title__light--pink">Россия</div>
       <ul>
         <li class="title__light--pink">Москва</li>
         <li>Татьяна Качина</li>
         <li>Бикаш Баба</li>
       </ul>
     </div>
    </div>
    <div class="col-1-5 col-sm-1-2 col-xs-1-1">
      <div class="mentors-list">
      <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/small_lotous.png">
       <div class="title__subtext title__light--pink">Казахстан</div>
       <ul>
         <li>Марина Никитина</li>
         <li>Бикаш Баба</li>
       </ul>
     </div>
    </div>
    <div class="col-1-5 col-sm-1-2 col-xs-1-1">
      <div class="mentors-list">
      <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/small_lotous.png">
       <div class="title__subtext title__light--pink">Россия</div>
       <ul>
         <li class="title__light--pink">Санкт-Петербург</li>
         <li>Кристина</li>
         <li>Бикаш Баба</li>
       </ul>
     </div>
    </div>
  </div>
  <script type="text/javascript">
    (function ($) {
$('.mentors-city-list').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dots: true,
});
})(jQuery);
</script>
      <div class="mentor-feature">
         <h3 class="title__light title__light--pink">Онлайн 28 дней</h3>
         <h3 class="title__light title__light--pink">Очно 14 день</h3>
       </div>

  </div>
</div>


<div class="course-info">
    <div class="container">
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <span class="course-info__user">Знания Веды, Джйотиш и Йоги</span>
              <div class="divider"></div>
              <ul>
                <li>Все это древнейшие науки, которым более 5000 лет;</li>
                <li>Их них вышли все религии и философии: <br>Дао, Дзен, Каббала и пр.;</li>
                <li>Все коучи мира черпают Знание в какой-то из них;</li>
                <li>Но в академии вы получите их все вместе!</li>
              </ul>  
              <div class="divider"></div>
              <span class="title__light--pink">Если вы:</span>
              <ul>
                <li>Хотите уверенности в своём завтра;</li>
                <li>Хотите обрести смысл жизни;</li>
                <li>Хотите достигать своих целей;</li>
                <li>Хотите преуспеть в бизнесе;</li>
                <li>Хотите создать лучшие для себя отношения;</li>
                <li>Хотите растить счастливых и успешных детей;</li>
                <li>Хотите подружиться со своим телом;</li>
                <li>Хотите творить свою жизнь в легкости и радости</li>
              </ul>
              <span class="title__light--pink">Этот курс для вас!</span>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">Вы обретёте</span>
              <div class="divider"></div>
              <ul>
                <li>Смысл жизни, предназначение и цель;</li>
                <li>Преодолеете одиночество и лень;</li>
                <li>Успех, энергию, гармоничные отношения;</li>
                <li>Знание законов времени и судьбы;</li>
                <li>Фундамент профессионального Ведического обучения.</li>
              </ul>
              <div class="divider"></div>
              <span class="title__light--pink">Курс передает знания трех факультетов:</span><br>
              <span class="title__light--pink"><small>«Искусство жизни, Веды, Практическая Психология»:</small></span>
              <ul> 
                <li>Мироздание, Энергосистема человека. Предназначение и Цели;</li>
                <li>Судьба, теория Успеха, Психология отношений и Синергия.</li>
              </ul>
              <span class="title__light--pink"><small>Факультет «Классическая Йога и Йога-терапия»:</small></span>
              <ul> 
                <li>Основы Классической Йоги и практики очищения;</li>
                <li>Утренний комплекс, навыки здорового образа жизни.</li>
              </ul>
              <span class="title__light--pink"><small>Факультет «Ведическая астрология Джйотиш»:</small></span>
              <ul> 
                <li>Введение в астрологию. Базовые знания Джйотиш (I ступень);</li>
                <li>Панчанга, Мухурта (Выбор благоприятного времени).</li>
              </ul>
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full" href="/pricelist/#kyrs-i">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img31.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img32.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img33.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main">Программа</div>
            <div class="title__main with-border"><span>I курса</span></div>
            <div class="title__subtext">Ведической академии Шринатджи</div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">Базовые знания <br>веды</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Мироздание, Мировоззрение</td><td class="hours">6ч</td></tr>
                    <tr><td>Энергосистема, Прана</td><td class="hours">8ч</td></tr>  
                    <tr><td>Йога, Санатана, Садхана</td><td class="hours">6ч</td></tr>
                    <tr><td>Дхарма, Призвание</td><td class="hours">10ч</td></tr>
                    <tr><td>Законы и формир. судьбы</td><td class="hours">10ч</td></tr>
                    <tr><td>Отношения, синергия</td><td class="hours">5ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">45 часов</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">&nbsp;<br>Джйотиш</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Физика в Джйотиш</td><td class="hours">1ч</td></tr>
                    <tr><td>Планеты и знаки</td><td class="hours">4ч</td></tr>  
                    <tr><td>Качество времени</td><td class="hours">2ч</td></tr>
                    <tr><td>Панчанга, Вара</td><td class="hours">3ч</td></tr>
                    <tr><td>Накшатры, Йоги</td><td class="hours">2ч</td></tr>
                    <tr><td>Титхи, Караны</td><td class="hours">3ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">15 часов</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">&nbsp;<br>Основы йоги</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Муктасана. Сурия Намаскар</td><td class="hours">6ч</td></tr>
                    <tr><td>Позвоночник, кишечник, глаза</td><td class="hours">6ч</td></tr>  
                    <tr><td>Шакти бандха. Энергоблоки</td><td class="hours">6ч</td></tr>
                    <tr><td>Утренний комплекс практик</td><td class="hours">6ч</td></tr>
                    <tr><td>Асаны, медитация</td><td class="hours">6ч</td></tr>
                    <tr><td>Пратьяхары (Нидра, Ом)</td><td class="hours">6ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">36 часов</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">Практики <br>(сатсанги)</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Очищение носоглотки</td><td class="hours">1ч</td></tr>
                    <tr><td>Массаж органов</td><td class="hours">1ч</td></tr>  
                    <tr><td>Очищение тела</td><td class="hours">3ч</td></tr>
                    <tr><td>Питание Йога</td><td class="hours">3ч</td></tr>
                    <tr><td>Постановка целей</td><td class="hours">2ч</td></tr>
                    <tr><td>Построение отношений</td><td class="hours">2ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">12 часов</span>
              </div>
            </div>    

     </div>
     <span class="programm-course-iii-subtitle">Всего: 108 часа</span>
     <div class="day-schedule">
       <div class="title__wrapper">
          <div class="title__pink title__h2">Распорядок дня</div>
          <div class="with-border with-border__long"><span>I курса</span></div>
        </div>
        <table>
          <tr class="first-line"><td class="schedule-hours">5:00 — 5:30</td><td>Подъем, прохладное омовение, прием воды с лимоном (0.6-1л) и трифалы</td></tr>
          <tr><td class="schedule-hours">5:30 — 6:20</td><td>Практики созерцания — медитации в храме, в классе, в зале</td></tr>
          <tr><td class="schedule-hours">6:20 — 7:00</td><td>Йога пробуждения: очистительные практики, спец. комплекс</td></tr>
          <tr><td class="schedule-hours">7:00 — 8:00</td><td>Завтрак</td></tr>
          <tr><td class="schedule-hours">8:00 — 13:00</td><td>Основная лекция по программе</td></tr>
          <tr><td class="schedule-hours">13:00 — 18:30</td><td>Обед и отдых</td></tr>
          <tr><td class="schedule-hours">18:30 — 20:00</td><td>Йога</td></tr>
          <tr class="last-line"><td class="schedule-hours">21:00</td><td>Приглашение ко сну</td></tr>
        </table>
     </div>
     <div class="button__list grid grid-top">
      <div class="col-1-2 col-sm-1-1"><div class="btn btn--full programm-more">ПРОГРАММА ПОДРОБНЕЕ</div></div>
      <div class="col-1-2 col-sm-1-1"><div class="btn btn--full schedule-more">РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ</div></div>
       
     </div>
    </div>
</div>

<div class="course-iii-descr">
  <div class="container">
    <div class="course-iii-descr-text">
      <p>В Академии студенты обретают не только востребованные профессии, но и уникальные навыки развития сознания, а также полное Мировоззрение</p>
    </div>  
      <div class="line-divider-short"></div>
      <div class="course-iii-descr-text">
      <p>Для результативного обучения до начала курса необходимо ознакомиться с рекомендованной литературой, аудио- и видео-записями в соответствии с вашим курсом</p>
    </div>
  </div>
</div>
<div class="btn__wrapper btn__wrapper--center-overlapped"><a href="/library/" class="btn btn--full">В библиотеку</a></div>


<div class="course-price">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="title__main with-border"><span>Стоимость</span></div>
        <div class="title__subtext">обучения и практик</div>
      </div>
      <div class="table-price price-list-first">
        <table>
          <tr>
            <th>
              <span>I курс</br>&nbsp;</span>
            </th>
            <th>
              <span>Самопознание</span></br>
              <span class="extra-light-subtitle">Видео</span>
            </th>
            <th>
              <span>Мастер </span></br>
              <span class="extra-light-subtitle">Онлайн / очно</span>
            </th>
            <th>
              <span>Пакет «Максимум»</span></br>
              <span class="extra-light-subtitle">Онлайн / очно</span>
            </th>
          </tr>
          <tr class="table-subtitle">
                        <td>
                              <span>Программа</span>
                        </td>
                        <td>
                              <span>Видео уроки, час.</span>
                        </td>
                        <td >
                              <span>Видео уроки + практики, час.</span>
                        </td>
                        <td>
                              <span>Видео уроки + практики, час.</span>
                        </td>
                  </tr>
                  <tr>
                        <td>Искусство жизни</td>
                        <td>33</td>
                        <td>57</td>
                        <td>57</td>
                  </tr>
                  <tr>
                        <td>Практики Йоги</td>
                        <td>3</td>
                        <td>36</td>
                        <td>36</td>
                  </tr>
                  <tr>
                        <td>Качество времени</td>
                        <td>6</td>
                        <td>15</td>
                        <td>15</td>
                  </tr>
                  <tr>
                        <td>Вебинары — Искусство Жизни</td>
                        <td>9</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Сопровождение куратора</td>
                        <td>да</td>
                        <td>-</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Сопровождение наставника</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Сопровождение основателя</td>
                        <td>-</td>
                        <td>-</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Онлайн-чат для студентов</td>
                        <td>-</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Подготовка карты наставником</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Подготовка карты основателем</td>
                        <td>-</td>
                        <td>-</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Уникальный учебник I курса PDF</td>
                        <td>-</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Выполнение 11 ягий в Ашраме</td>
                        <td>-</td>
                        <td>-</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Сертификат об окончании I курса</td>
                        <td>-</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr class="table-subtitle-blue">
                        <td>ИТОГО ЧАСОВ</td>
                        <td>54</td>
                        <td>162</td>
                        <td>162</td>
                  </tr>
                <tr>
                  <td>Полная стоимость</td>
                  <td><?php echo do_shortcode('[woocommerce_price id="2245"]') ?><a href="/cart/?add-to-cart=2245">купить</a></td>
                  <td><?php echo do_shortcode('[woocommerce_price id="2246"]') ?><a href="/cart/?add-to-cart=2246">купить</a></td>
                  <td><?php echo do_shortcode('[woocommerce_price id="2250"]') ?><a href="/cart/?add-to-cart=2250">купить</a></td>
                </tr>
                <tr>
                  <td>Стоимость при оплате за 30 дней</td>
                  <td>-</td>
                  <td><?php echo do_shortcode('[woocommerce_price id="2247"]') ?><a href="/cart/?add-to-cart=2247">купить</a></td>
                  <td><?php echo do_shortcode('[woocommerce_price id="2251"]') ?><a href="/cart/?add-to-cart=2251">купить</a></td>
                </tr>
                <tr>
                  <td>Стоимость со скидкой для пар (с чел.) <br>*При оплате за двоих с одного счета.</td>
                  <td>-</td>
                  <td><?php echo do_shortcode('[woocommerce_price id="2248"]') ?><a href="/cart/?add-to-cart=2248">купить</a></td>
                  <td><?php echo do_shortcode('[woocommerce_price id="2252"]') ?><a href="/cart/?add-to-cart=2252">купить</a></td>
                </tr>
                <tr>
                  <td>Стоимость по акции</td>
                  <td>-</td>
                  <td><?php echo do_shortcode('[woocommerce_price id="2249"]') ?><a href="/cart/?add-to-cart=2249">купить</a></td>
                  <td>-</td>
                </tr>
            </table>
            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>
    </div>
  </div>
</div>

<div class="course-conditions">
  <div class="container">
    <!-- <div class="course-conditions-more"> -->
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
      <div class="title__main with-border"><span>Условия</span></div>
      <div class="title__subtext">учебы, проживания и отдыха</div>
      <div class="title__subtext title__light--pink">Для очных студентов</div>
    </div>
    <h3 class="title__wrapper title__light-no-bottom-margin">Расписание ближайших курсов смотрите на странице</h3>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="/fakultety/#schedule" class="btn btn--full ">Факультеты</a>
    </div>
    <div class="course-conditions-place">
      <h3 class="title__wrapper title__light-no-bottom-margin">В России, Казахстане курс может проходить в выездном «стандарт», городском «эконом» и «городском» (без йоги) вариантах.Расписание и время занятий в городском формате согласуется с группой. В Индии курс проходит в вариантах «Стандарт» и «Продвинутый»</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Программа I курса в Индии проходит <br>на побережье Индийского океана или в Гималаях</h3>
    </div>

    <div class="course-conditions-infos grid grid-top">
      <div class="col-1-2 col-sm-1-1">
        <div class="course-conditions-info">
          <span class="title__light--pink">Наггар – древняя столица Индии в Гималаях</span>
          <div class="divider"></div>
          <p>Курс проходит в ашраме Храма Кришны. Старейший духовный центр мира (более 5000 лет). Уникальная природа. Геотермальные источники. Ежедневные Арати и Киртаны. Вход в Шамбалу. А также усадьба мистика и художника Н. Рериха. <br><br>Прибыть в аэропорт Нью Дели до 12:00 за день да начала. Самолёт из а/п Нью Дели должен отправляться после 15:00 на следующий день после окончания ретрита.</p>
       </div>
      </div>
      <div class="col-1-2 col-sm-1-1">
        <div class="course-conditions-info">
          <span class="title__light--pink">Гокарна – деревня браминов, <br>священнослужителей</span>
          <div class="divider"></div>
          <p>Здесь находится озеро Коти Тиртха, откуда явился бог Шива. Здесь расположен один из 12 Атмалингамов Шивы и его Храм. Здесь получают благословение на создание семей и детей. Здесь Храм Ганеши, бога мудрости и благих начинаний. На взморье Храм Рамы, источник у храма исцеляет больных. Живописные пляжи с белым песком, пальмами и соснами. <br><br>Курс проходит в живописных резортах на берегу океана Namaste Sanjeevini, Digambara, Papa Jolly’s Eco-Resort (Goa) Прибыть в а/п Даболим до 9:00, а отправляться после 18:00  </p>
        </div>
      </div>
    </div>

    <div class="course-conditions-image-slider grid grid-top">
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider11.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider12.png">
      </div> 
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider10.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider3.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider2.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider1.png">
      </div>
    </div>
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Программа предусматривает два выходных <br>с возможностью организации экскурсий</h3>
   <!--  </div> -->
    <!-- <div class="btn__wrapper btn__wrapper--center">
      <div class="btn btn--full conditions-more">УCЛОВИЯ РАЗМЕЩЕНИЯ ОЧНЫХ СТУДЕНТОВ</div>
    </div> -->
    
  </div>
</div>

<div class="coast-place">
  <div class="container">
        <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Стоимость</span></div>
            <div class="title__subtext">размещения</div>
            <div class="title__subtext title__light--pink">оплата на месте</div>
     </div>
     <div class="coast-place-features grid grid-top">
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon1.png">
           <span class="title__light--pink">2-местное размещение</span>
           <small>15 дней, 14 ночей</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td width="40%" ><span>Эконом</span></td>
                <td width="60%" style="text-align: right;"><span>$140 ($10 руб./чел.)</span></td>
              </tr>
              <tr>
                <td width="40%"><span>Стандарт</span></td>
                <td width="60%" style="text-align: right;"><span>$280 ($20/чел.)</span></td>
              </tr>
              <tr>
                <td width="40%"><span>Эксклюзив</span></td>
                <td width="60%" style="text-align: right;"><span >$560 ($40/чел.)</span></td>
              </tr>
            </table>
           </div>
           </div>
         </div>
         <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon2.png">
           <span class="title__light--pink">Завтрак, обед</span>
           <small>Йогическое, здоровое питание</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td width="40%"><span>Стандарт</span></td>
                <td width="60%" style="text-align: right;"><span>$140 ($20/чел.)</span></td>
              </tr>
              <tr>
                <td width="40%"><span>Эксклюзив</span></td>
                <td width="60%" style="text-align: right;"><span >$210 ($15/чел.)</span></td>
              </tr>
              <tr>
                <td width="40%" >&nbsp;</td>
                <td width="60%" style="text-align: right;">&nbsp;</td>
              </tr>
            </table>
           </div>
           </div>
         </div>
         <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon3.png">
           <span class="title__light--pink">Отдых</span>
           <small>Экскурсии, сопровождение</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td width="100%" colspan="2" style="text-align: center;"><span>$50/чел.</span></td>
              </tr>
              <tr>
                <td width="40%">&nbsp;</td>
                <td width="60%" style="text-align: right;">&nbsp;</td>
              </tr>
              <tr>
                <td width="40%" >&nbsp;</td>
                <td width="60%" style="text-align: right;">&nbsp;</td>
              </tr>
            </table>
           </div>
           </div>
         </div>
       </div>
      <div class="coast-place-features__list">
        <ul>
          <li>* Размещение приезжих участников вне центра — невозможно.</li>
          <li>* Студенты с годовой и более визой могут жить за пределами центра. Доп. платеж за услуги $50.</li>
          <li>* Возможно размещение членов семьи без участия в программе (условия проживания те же).</li>
          <li>* При существенном изменении курса доллара стоимость в рублях может измениться.</li>
        </ul>
      </div>
     </div>
  </div>
</div>

<div class="trip-coast">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main"><span>Стоимость</span></div>
            <div class="title__subtext">путешествия</div>
     </div>
     <div class="trip-coast-features grid grid-top">
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon1.png">
           <div class="divider"></div>
           <p>Перелет в Даболим или Нью-Дели — ориентировочная стоимость</p>
           <p class="blue-text">$500 (400) /чел.</p>
         </div>
       </div>
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon2.png">
           <div class="divider"></div>
           <p>Трансфер от аэропорта и обратно — ориентировочная стоимость</p>
           <p class="blue-text">$60/чел.</p>
         </div>
       </div>
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt="" src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon3.png">
           <div class="divider"></div>
           <p>Электронная виза — <span class="blue-text">$100/чел.</span></p>
           <p>Виза в посольстве — <span class="blue-text">$60/чел.</span></p>
         </div>
       </div>
     </div>
     <small>Полная стоимость путешествия и обучения — от $1200</small>
  </div>
</div>
<div class="btn__wrapper btn__wrapper--center-overlapped"><a href="/pricelist/#kyrs-i" class="btn btn--full">оплатить КУРС</a></div>

<div class="course-sert">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="title__subtext">По окончании курса</div>
            <div class="title__main with-border"><span>вы получите</span></div>
     </div>
     <div class="sertificates grid grid-top">
       <div class="col-1-2 col-sm-1-1">
         <div class="sertificate natal">
        <h3 class="title__light title__light--pink">Натальную карту</h3>
        <span>C расшифровкой предназначения, <br>благоприятных видах деятельности с рекомендациями, <br>в соответствии с жизненными периодами</span>
     </div>
       </div>
       <div class="col-1-2 col-sm-1-1">
         <div class="sertificate sert_one">
        <h3 class="title__light title__light--pink">Сертификат</h3>
        <span>Сертификат о прохождении курса<br>&nbsp;<br>&nbsp;</span>
     </div>
       </div>
     </div>
  </div>
</div>

<div class="course-result-title">
  <div class="container">
        <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Результаты</span></div>
            <div class="title__subtext">I курса</div>
     </div>
    <h3 class="title__wrapper title__light-no-bottom-margin">Студенты академии получают на курсе не только уникальные <br>и востребованные профессии, но и развиваются в материальном <br>и духовном плане, получают полное Мировоззрение</h3>
  </div>
</div>

<div class="course-result">
  <div class="container">
    <div class="you-can">
      <div class="you-can-text"> 
        <ul>
          <li>Получите свою натальную карту с расшифровкой;</li>
          <li>Узнаете свое предназначение и путь к его достижению;</li>
          <li>Научитесь определять благоприятное время для деятельности;</li>
          <li>Научитесь понимать природу поступков людей;</li>
          <li>Сможете гармонично выстраивать личные и деловые отношения;</li>
          <li>Вы получите знания о сохранении молодости и стройности тела;</li>
          <li>Вы забудете о страхах, эмоциональном выгорании, одиночестве;</li>
          <li>Научитесь убирать сомнения, преодолевать лень;</li>
          <li>Ваше тело очистится, омолодится, вы отдохнёте;</li>
          <li>Постигнете Базовый уровень Джйотиш «Качество и выбор времени».</li>
        </ul>
      </div>
    </div>
  </div>
</div>  

<div class="course-result-end">
  <div class="container">
    <div class="yoga-consult">
      <h3 class="title__wrapper title__light title__light--pink btn__wrapper--center-overlapped">Вы сможете:</h3>
      <ul>
        <li>Сделать осознанный выбор Мировоззрения;</li>
        <li>Обрести Свободу от невежества и преодолеть страхи (Ана-майи);</li>
        <li>Получить Знание законов Мироздания, Судьбы и Природы человека;</li>
        <li>Обрести Знание теории успеха и девяти энергий победы;</li>
        <li>Получить инструменты роста энергии, свободных средств;</li>
        <li>Обрести Знание законов отношений, Энергообмена и Синергии;</li>
        <li>Осознать Смысл жизни, Предназначение, Цели, Пути (Дхарму);</li>
        <li>Принять Знание Йоги (очищение тела, оздоровление спины);</li>
        <li>Получить Знание астрологии для ежедневного применения.</li>
      </ul>
    </div>
    <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink title-divider">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
    <div class="divider"></div>
    <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    <div class="btn__wrapper btn__wrapper--center"><a href="/pricelist/#kyrs-i" class="btn btn--full">СТОИМОСТЬ КУРСА</a></div>
  </div>
</div>
<style type="text/css">
  #modal-layer-big {
    display: none;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, .4);
    text-align: center;
    z-index: 999;
}

#modal-layer-big::before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
}

#modal-big {
    display: inline-block;
    min-width: 500px;
    padding: 50px;
    background: #fff;
    border-radius: 5px;
    box-shadow: 0 0 3px 3px rgba(0, 0, 0, .4);
}
#modal-big .title__wrapper{
  margin-bottom: 25px;
}
#modal-big .title__wrapper-link{
  font-size: 20px;
  font-weight: 400;
}
</style>

<!-- <div id="modal-layer-big">
  <div id="modal-big">
    <div class="title__wrapper">
      <div class="title__h2 title__pink">Новогодняя</div>
      <div class="title__pink with-border"><span>Акция</span></div>
    </div>
    <h3 class="title__wrapper title__light-no-bottom-margin">Профессиональное образование<br>
    I курс (ОНЛАЙН)</h3>
    <h3 class="title__wrapper title__wrapper-link title__light-no-bottom-margin"><a href="/cart/?add-to-cart=2249">за 35 000 рублей</a></h3>
    <h3 class="title__wrapper title__light-no-bottom-margin">до 15 января 2021 года</h3>

  </div>
</div> -->

<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

var boxes = document.getElementsByClassName('course-info__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}

$('.programm-more').click(function(){
    $('.programm-hidden-data').slideToggle(300, function(){
      if ($(this).is(':hidden')) {
        $('.programm-more').html('ПРОГРАММА ПОДРОБНЕЕ');
      } else {
        $('.programm-more').html('Скрыть ПРОГРАММУ');
      }             
    });
    return false;
});

$('.schedule-more').click(function(){
    $('.day-schedule').slideToggle(300, function(){
      if ($(this).is(':hidden')) {
        $('.schedule-more').html('РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ');
      } else {
        $('.schedule-more').html('Скрыть РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ');
      }             
    });
    return false;
});

$('.conditions-more').click(function(){
    $('.course-conditions-more').slideToggle(300, function(){
      if ($(this).is(':hidden')) {
        $('.conditions-more').html('УCЛОВИЯ РАЗМЕЩЕНИЯ ОЧНЫХ СТУДЕНТОВ');
      } else {
        $('.conditions-more').html('Скрыть УCЛОВИЯ РАЗМЕЩЕНИЯ ОЧНЫХ СТУДЕНТОВ');
      }             
    });
    return false;
});

// function showModalBig() {
//     $('#modal-layer-big').fadeIn('fast').delay(5000).fadeOut('slow', function() {
//         setTimeout(showModalBig, 5000);
//     });
// }
// setTimeout(showModalBig, 5000);


</script>

<?php get_footer(); ?>
