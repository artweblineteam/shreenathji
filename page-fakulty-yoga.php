<?php
/*
Template Name: fakulty-yoga
*/


 get_header(); ?>

      
      <div class="faculty-about" id="faculty-veda-about">
        <div class="container">
          <h1 class="faculty-about__title wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="title__h2">факультет Классическая</div>
            <div class="title__main with-border"><span>йога И ЙОГА Терапия</span></div>
            <div class="title__h2">профессиональное образование</div>
          </h1>
          <div class="faculty-about__text">
            <p>Факультет проводит фундаментальную подготовку сознания студентов Академии Шринатджи</p>
      <p>Программа изучает природу тела и сознания, качества и законы энергообмена, <br>а также законы Судьбы и природу Мироздания, с точки зрения Веды.</p>
      <p>Дает Знания и Навыки Искусства жизни, оздоровления и управления сознанием и телом.<br>
          Выпускники способны проводить занятия Йоги любой сложности, а также оказывать физическую, <br>энергетическую и психологическую помощь как Йога-терапевт.</p>
      <p>Программа длится 1,5 и включает 4 курса.<br>Стоимость от 81 000 до 400 000 рублей.</p>
          </div>
          <div class="grid qualification2">
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">Консультант Йоги
                <div class="line-divider"></div>
                <span>С нулевого уровня<br> &nbsp;</span>
              </div>
              
            </div>
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">НАСТАВНИК ЙОГИ
                <div class="line-divider"></div>
                <span>С нулевого уровня <br> &nbsp;</span>
              </div>
    
            </div>
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">НАСТАВНИК ЙОГА-ТЕРАПЕВТ
                <div class="line-divider"></div>
                <span>Для сертифицированных <br>специалистов реабилитации</span>
              </div>

            </div>
          </div>
          <div class="btn__wrapper btn__wrapper--center faculty__btn-wrapper1 with-border1 with-border1__long">
            <a class="btn btn--full" href="specialty/#yoga">Специальности</a>
          </div>
          
          <div class="two-blocks grid">
            <div class="col-1-4 col-sm-1-1"></div>
            <div class="col-1-2 col-sm-1-1">
              <div class="two-blocks__with-border">
                <h3 class="two-blocks__with-border--title">Дополнительные навыки:</h3>
                <ul>
                  <li>применение Джйотиш в Йоге</li>
                  <li>определение Призвания </li>
                  <li>Управление Судьбой</li>
                  <li>заработок от Р 150 тыс. / мес.</li>
                </ul>
              </div>
            </div>
            <div class="col-1-4 col-sm-1-1"></div>
          </div>

          <div class="faculty-about__text">
            <p>
              Программа факультета профессионального образования <br>
КЛАССИЧЕСКАЯ ЙОГА И ЙОГА ТЕРАПИЯ<br>
позволяет каждому студенту решить такие задачи:
</p>
          </div>
          <div class="grid">
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">1</div>
                  <div class="faculty__task-desc">Найти своё предназначение, высокую цель и энергию для её достижения</div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">2</div>
                  <div class="faculty__task-desc">Получить практики йоги и посвящение из рук представителя древней парампары</div>
                </div>
              </div>
            </div>
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">3</div>
                  <div class="faculty__task-desc">Обрести высокооплачиваемую профессию наставника йоги и йога-терапевта</div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">4</div>
                  <div class="faculty__task-desc">Обрести Знание о мироздании и социуме. Научится строить высокие отношения</div>
                </div>
              </div>
            </div>
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">5</div>
                  <div class="faculty__task-desc">Научится управлять сознанием и событиями. Обрести удачу, целеустремленность</div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">6</div>
                  <div class="faculty__task-desc">Научится планировать. Получить управленческие и наставнические навыки</div>
                </div>
              </div>
            </div>
          </div>
          <div class="faculty__margin text__c">
            <!-- Для полной реализации недостаточно приобрести профессию. Необходимо обладать<br>широким мировоззрением и владеть знанием психологии личности и социума.<br>
            В достаточной мере управлять своим сознанием и видеть Мироздание во всей полноте<br>
            Обладать организационными, управленческими и наставническими навыкамм. -->
          </div>
        </div>
      </div>
      <div class="faculty-learning" id="faculty-veda-learning">
        <div class="container">
          <div class="title__wrapper wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="title__h2">факультет Классическая</div>
            <div class="title__h2 with-border with-border__long"><span>йога И ЙОГА Терапия</span></div>
            <div class="title__h2">профессиональное образование</div>
          </div>
          <h3 class="title__light title__light--pink">Учебный процесс</h3>
          <div class="text__c">
            <p>
              
            </p>
            <p>Обучение предусматривает четыре курса по 108 часов каждый и рассчитано на 1,5 года.  <br>Первые два курса являются базовыми — общая программа для всех направлений.  <br> Третий и четвертый курсы дают специальность и общие фундаментальные знания.</p>
          <p>Программа обучения дает фундаментальные знания о мироздании, <br>человеке, отношениях, концентрации и основах йоги,  <br>без которых восприятие любой ведической науки невозможно.
</p>
          </div>
          <div class="grid grid-top">
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-i-general/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;I</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Предназначение энергия отношения<br>«Сознание Брахмы»</p>
                    <p class="text__gray">Творец. Курс из трех частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Передает фундаментальные Ведические Знания (57ч):</h5>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">O мироздании, человеке, карме (судьбе), дхарме, долге, духовном предназначении;</li>
                    <li class="list-small-dots__item">О высоких гармоничных отношениях, энергообмене и карма-йоге;</li>
                    <li class="list-small-dots__item">Наделяет сознанием высокой цели, призвания, энергией и удачей.</li>
                  </ul>
                  <h5 class="course__part-title text__12light text__12light--pink">Введение в астрологию Джйотиш, Панчанга, Мухурта, I ступень (15 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Базовые практики Хатха-йоги, очищения, здоровья спины, суставов (36 ч).</h5>
                  <p class="text__12light text__black">
                    Каждый участник I-го курса получает расшифровку натальной карты в области предназначения, призвания и долга. Каждый выпускник получает свидетельство о прохождении курса, которое является допуском ко II-му курсу.
                  </p>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2245"]');?> РУБ.</div>
                  </div>
                 <!--  <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-ii-jizn/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;II</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Джйотиш, концентрация, успех,<br>«Сознание Шивы»</p>
                    <p class="text__gray">Организатор. Курс из двух частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Фундаментальные знания психики человека и законов успеха(36ч):</h5>
                  <p class="text__12light text__black">
                    Принципы концентрации и управления собственной психикой. Позволяет усилить характер и способности, что приводит к успеху.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Практика кундалини-йоги, пранаямы, пратьяхары и концентрации (18ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Базовые Знания Джйотиш, II-я ступень (36 ч).</h5>
                  <p class="text__12light text__black">Включает подробное изучение всех инструментов ведической астрологии.</p>
                  <p class="text__12light text__black">
                    Каждый участник по окончании II-го курса получает свидетельство о прохождении курса, которыйявляется допуском ко III-му курсу.</p>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="3517"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-iii-yoga/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;III</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Управление сознанием и действительностью<br>«Сознание Вишну»</p>
                    <p class="text__gray">Лидер. Курс из двух частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Фундаментальные знания социальной психологии, законов общества (36ч):</h5>
                  <p class="text__12light text__black">
                    Учит способности обретать и умножать счастье благодаря работе с обществом. Несет знание и навык осознанности,глубокое восприятие общества и вселенной.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Передает знания раджа-йоги и практик дхараны, свидетельствования (18ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Психология йоги. Анатомия тела. </h5>
                  <p class="text__12light text__black">
                    Нети, Кунджал. Гьяна, Кундалини, Нидра. Виньяса йога. Мудры, Джапа Перевернутые, Балансирующие асаны, Падмасана. Пранаяма, Пратьяхара, Дхарана, Ом, Тратака, Мауна.</p>
                  <p class="text__12light text__black">
                    Каждый студент по окончании III-го курса, сдачи экзамена и прохождения практикиполучает сертификат с указанием квалификации и профессии:</p>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">Oрганизатор и лидер;</li>
                    <li class="list-small-dots__item">Консультант-астролог.</li>
                  </ul>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2123"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-iv-yoga/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;IV</div>
                    <div class="course__option text__pink16light">ОЧНО</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Познание истины<br>«Сознание Абсолюта»</p>
                    <p class="text__gray">Наставник. Курс из трёх частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Дает фундаментальные знания о Всевышнем и Истории Знания (36 ч). </h5>
                  <p class="text__12light text__black">
                    Несет любовь, способность восприятия Высших сил и счастье. Студенты получают уникальную возможность обрести посвящение непосредственно из первых рук – хранителей древнейших сампрадай (традиций) на Земле.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Знания и практики Раджа-, Гьяна- и Бхакти-йоги (36 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Крийя, Бхагти, Антаранга, Садхана, Свара йога.</h5>
                  <p class="text__12light text__black">
                    Методика проведения занятий по йоге. Этика учителя. Распространенные заболевания и методы йога-терапии. Продвинутые асаны. Пранаяма с задержкой. Йога Нидра – уровни 5, 6. Чидакаш Дхарана. Медитации.</p>
                  <p class="text__12light text__black">
                    Студенты по окончании IV-го курса, сдачи экзамена, защиты Диплома и прохождения практики получают Диплом выпускника Академии с квалификацией и профессией:</p>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">Наставник;</li>
                    <li class="list-small-dots__item">Преподаватель йоги;</li>
                    <li class="list-small-dots__item">Йога-терапевт (только для дипломированных специалистов).</li>
                  </ul>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2167"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
          </div>
          <div class="course__statement text__c">
            <p>
              Обучение проходит на 4 курсах и длится до 2 лет.<br>Курс несет фундаментальные знания о мироздании, человеке,<br>
              отношениях, концентрации и основах йоги,<br>
              без которых восприятие любой ведической науки невозможно.
            </p>
          </div>
        </div>
        <div class="course__compare-package text__c"><a class="btn btn--full btn--longer" href="/fakultety/#programm-coast">Сравнение пакетов</a></div>
        <div class="container">
          <div class="course__general wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="with-border with-border__long title__pink">О&nbsp;Йоге</div>
          </div>
          <div class="course__general--info">
            <div class="grid grid-top">
              <div class="col-1-2 col-sm-1-1">
                <p>
                  «Йога» — означает единство. Инструмент, связующий в единое целое душу, сознание, разум, ум, чувства, энергетику и тело человека.</p>
                <p>Это ценнейшее наследие древности, способное сделать каждого человека счастливым. Это насущная необходимость сегодняшнего дня и культуры будущего. В современном мире Классическую Йогу разделили на десятки различных видов, что противоречит самому названию этой науки.</p>
              </div>
              <div class="col-1-2 col-sm-1-1">
                <p>В Академии мы стремимся показать Йогу как единую целостную науку, которая очень плотно связана с другими науками о человеке: Ведической медициной — Аюрведа — и астрологией Джйотиш.</p>
                <p>Йога воздействует на все аспекты человека: физический, ментальный, эмоциональный, психологический и духовный. Йога означает баланс и гармонию тела, ума и эмоций. Йога – это наука о счастливой жизни!</p>
              </div>
            </div>
          </div>
          <div class="line-divider margin-bottom90"></div>
        </div>
      </div>
<script type="text/javascript">
  var boxes = document.getElementsByClassName('course__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script> 

<?php get_footer(); ?>
