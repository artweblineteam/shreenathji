<?php
/*
Template Name: fakulty-juotisha
*/


 get_header(); ?>

      
      <div class="faculty-about" id="faculty-veda-about">
        <div class="container">
          <h1 class="faculty-about__title wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="title__h2"><span>факультет Ведическая астрология</span></div>
            <div class="title__main with-border">джйотиш</div>
            <div class="title__h2"><span>профессиональное образование</span></div>
          </h1>
          <div class="faculty-about__text">
            <p>Факультет проводит фундаментальную подготовку сознания студентов Академии Шринатджи</p>
            <p>Программа изучает природу Мироздания и сознания, законы их взаимодействия, <br>роль и влияние планет на способности и судьбу человека, с точки зрения Веды<br> Дает Знания и Навыки Искусства жизни, управления сознанием и коуч навыки.</p>
            <p>Выпускники способны оказывать комплексную помощь окружающим. <br>Программа длится 1.5 года и включает 4 курса. <br>Стоимость от 81 000 до 400 000 рублей</p>
          </div>
          <div class="grid qualification2">
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">КОНСУЛЬТАНТ-<br>АСТРОЛОГ
                <div class="line-divider"></div>
                <span>С нулевого уровня</span>
              </div>
              
            </div>
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">НАСТАВНИК <br>АСТРО-ПСИХОЛОГ
                <div class="line-divider"></div>
                <span>С нулевого уровня </span>
              </div>
    
            </div>
            <div class="qualification2__item qualification-text">
              <div class="qualification-text__valign">НАСТАВНИК АСТРОЛОГ- <br>КОНСУЛЬТАНТ HR-АГЕНСТВА
                <div class="line-divider"></div>
                <span>Для сертифицированных кадровиков</span>
              </div>

            </div>
          </div>
          <div class="btn__wrapper btn__wrapper--center faculty__btn-wrapper1 with-border1 with-border1__long">
            <a class="btn btn--full" href="/specialty/#juotisha">Специальности</a>
          </div>
          <div class="two-blocks grid">
      <div class="col-1-4 col-sm-1-1"></div>
      <div class="col-1-2 col-sm-1-1">
        <div class="two-blocks__with-border">
          <h3 class="two-blocks__with-border--title">Дополнительные навыки:</h3>
            <ul>
              <li>применение в консультации Джйотиш практик Йоги, основ Васту и Аюрведы</li>
              <li>практики работы с сознанием </li>
              <li> заработок от Р 200 тыс. / мес.</li>
            </ul>
        </div>
      </div>
      <div class="col-1-4 col-sm-1-1"></div>
    </div>
          <div class="faculty-about__text">
            <p>Программа факультета профессионального образования <br>ВЕДИЧЕСКАЯ АСТРОЛОГИЯ ДЖЙОТИШ <br>позволяет каждому студенту решить такие задачи:</p>
          </div>
          <div class="grid">
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">1</div>
                  <div class="faculty__task-desc">Найти своё предназначение, высокую цель и энергию для её достижения </div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">2</div>
                  <div class="faculty__task-desc">Получить знание Джйотиш и посвящение в одной из древнейшей парампаре</div>
                </div>
              </div>
            </div>
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">3</div>
                  <div class="faculty__task-desc">Обрести высокооплачиваемую профессию мастера и наставника Джйотиш</div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">4</div>
                  <div class="faculty__task-desc">Обрести Знание о мироздании и социуме. Научится строить высокие отношения</div>
                </div>
              </div>
            </div>
            <div class="col-1-3 col-xs-1-1">
              <div class="faculty__task-item">
                <div class="faculty__task-item--top">
                  <div class="faculty__task-number">5</div>
                  <div class="faculty__task-desc">Научится управлять сознанием и событиями. Обрести удачу, целеустремленность</div>
                </div>
                <div class="faculty__task-item--bottom">
                  <div class="faculty__task-number">6</div>
                  <div class="faculty__task-desc">Научится планировать. Получить управленческие и наставнические навыки</div>
                </div>
              </div>
            </div>
          </div>
          <div class="faculty__margin text__c">
            <!-- Для полной реализации недостаточно приобрести профессию. Необходимо обладать<br>широким мировоззрением и владеть знанием психологии личности и социума.<br>
            В достаточной мере управлять своим сознанием и видеть Мироздание во всей полноте<br>
            Обладать организационными, управленческими и наставническими навыкамм. -->
          </div>
        </div>
      </div>
      <div class="faculty-learning" id="faculty-veda-learning">
        <div class="container">
          <div class="title__wrapper wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="title__h2">факультет Ведическая астрология</div>
            <div class="title__h2 with-border with-border__long">Джйотиш</div>
            <div class="title__h2">профессиональное образование</div>
          </div>
          <h3 class="title__light title__light--pink">Учебный процесс</h3>
          <div class="text__c">
            <p>
              Обучение на факультете предусматривает четыре курса, по 108 часов каждый. <br>Первые два являются базовыми – их программа повторяется на всех факультетах. <br>Третий и четвертый дают как общие, так и специальные Знания.
            </p>
          </div>
          <div class="grid grid-top">
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-i-general/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;I</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Предназначение энергия отношения<br>«Сознание Брахмы»</p>
                    <p class="text__gray">Творец. Курс из трех частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Передает фундаментальные Ведические Знания (57 ч):</h5>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">О Мироздании, Человеке, Карме (Судьбе), Дхарме, Долге, Духовном предназначении;</li>
                    <li class="list-small-dots__item">О Высоких гармоничных отношениях, Энергообмене и Карма-йоге;</li>
                    <li class="list-small-dots__item">Наделяет осознанием Высокой Цели, Призвания, Энергией и Удачей.</li>
                  </ul>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Введение в астрологию Джйотиш, Панчанга, Мухурта, I ступень (15 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Базовые практики Хатха-йоги, очищения, здоровья спины, суставов (36 ч).</h5>
                  <p class="text__12light text__black">
                    Каждый участник I-го курса получает расшифровку натальной карты в области предназначения, призвания и долга. Каждый выпускник получает свидетельство о прохождении курса, которое является допуском ко II-му курсу.

                  </p>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2245"]');?> РУБ.</div>
                  </div>
                 <!--  <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-ii-juotisha/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;II</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Джйотиш, концентрация, успех,<br>«Сознание Шивы»</p>
                    <p class="text__gray">Организатор. Курс из трёх частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Фундаментальные Знания Психики человека и Законов успеха (36 ч). </h5>
                  <p class="text__12light text__black">
                    Принципы концентрации и управления собственной психикой. Позволяет усилить характер и способности, что приводит к успеху.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Практики Кундалини-йоги,  Пранаямы, Пратьяхары и Концентрации (36 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Базовые Знания Джйотиш, II-я ступень (36 ч).</h5>
                  <p class="text__12light text__black">Включает подробное изучение всех инструментов ведической астрологии.</p>
                  <p class="text__12light text__black">
                    Каждый участник по окончании II-го курса получает свидетельство о прохождении курса, которыйявляется допуском ко III-му курсу.</p>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2266"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-iii-jyotisha/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;III</div>
                    <div class="course__option text__pink16light">ОЧНО / ОНЛАЙН</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Управление сознанием и действительностью<br>«Сознание Вишну»</p>
                    <p class="text__gray">Лидер. Курс из трёх частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Фундаментальные знания социальной психологии, законов общества (36 ч).</h5>
                  <p class="text__12light text__black">
                    Учит способности обретать и умножать счастье благодаря работе с обществом. Несет Знание и навык осознанности, глубокое восприятие общества и Вселенной.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Передает Знания Раджа-йоги и практик Дхараны, свидетельствования (36 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Применение инструментов Джйотиш (III ст.). Все Лагны и дробные карты, Палмистри, Нумерологии (36 ч).</h5>
                  <p class="text__12light text__black">
                    Все сферы жизни: здоровье, деньги, отношения, путешествия. Практика. Более 100 примеров.</p>
                  <p class="text__12light text__black">
                    Каждый студент по окончании III-го курса, сдачи экзамена и прохождения практики получает сертификат с указанием квалификации и профессии:</p>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">Oрганизатор и лидер;</li>
                    <li class="list-small-dots__item">Консультант-астролог.</li>
                  </ul>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2133"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
            <div class="col-1-2 col-sm-1-1"><a data-fancybox data-src="#hidden-content" class="course__item gray-border" href="/kyrs-iv-juotisha/">
                <h4>
                  <div class="course__title">
                    <div class="course__number title__h2 text__black">Курс&nbsp;IV</div>
                    <div class="course__option text__pink16light">ОЧНО</div>
                  </div>
                  <div class="course__subtitle text__16light text__black">
                    <p>
                      Познание истины<br>«Сознание Абсолюта»</p>
                    <p class="text__gray">Наставник. Курс из трёх частей</p>
                  </div>
                </h4>
                <div class="line-divider"></div>
                <div class="course__content">
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 1. Дает фундаментальные знания о Всевышнем и Истории Знания (36 ч).  </h5>
                  <p class="text__12light text__black">
                    Несет любовь, способность восприятия Высших сил и счастье. Студенты получают уникальную возможность обрести посвящение непосредственно из первых рук – хранителей древнейших сампрадай (традиций) на Земле.</p>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 2. Знания и практики Раджа-, Гьяна- и Бхакти-йоги (36 ч).</h5>
                  <h5 class="course__part-title text__12light text__12light--pink">Часть 3. Искусство Джйотиш (IV-я ступень), ректификация, предсказания и пр. Более 30 примеров (36 ч).</h5>
                  <p class="text__12light text__black">
                    Студенты по окончании IV-го курса, сдачи экзамена, защиты Диплома и прохождения практики получают Диплом выпускника Академии с квалификацией и профессией:</p>
                  <ul class="list-small-dots text__12light text__black">
                    <li class="list-small-dots__item">Наставник;</li>
                    <li class="list-small-dots__item">Астро-психолог;</li>
                    <li class="list-small-dots__item">Астролог-консультант HR агентств (только для дипломированных специалистов).</li>
                  </ul>
                </div>
                <div class="line-divider"></div>
                <div class="course__buy">
                  <div class="course__buy--group">
                    <div class="course__buy--type text__16light text__16light--romul text__gray">От</div>
                    <div class="course__buy--price text__24romul text__24romul--pink"><?php echo do_shortcode('[shree_price id="2184"]');?> РУБ.</div>
                  </div>
                  <!-- <div class="course__buy--more text__24 text__24--pink">ПОДРОБНЕЕ О КУРСЕ</div> -->
                </div></a></div>
          </div>
          <div class="course__statement text__c">
            <p>
              Обучение проходит на 4 курсах и длится до 2 лет. <br>Курс несет Фундаментальные Знания о Мироздании, человеке, <br>отношениях, концентрации и основах Йоги, <br>без которых восприятие любой Ведической науки невозможно.
            </p>
          </div>
        </div>
        <div class="course__compare-package text__c"><a class="btn btn--full btn--longer" href="/fakultety/#programm-coast">Сравнение пакетов</a></div>
        <div class="container">
          <div class="course__general wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
            <div class="with-border with-border__long title__pink">О&nbsp;джйотиш</div>
          </div>
          <div class="course__general--info">
            <div class="grid grid-top">
              <div class="col-1-2 col-sm-1-1">
                <p>
                  Джйотиш — с санскрита «Божественный свет»,
                  «Свет Знания». По сути, это возможность видеть прошлое
                  и будущее каждого человека или процесса, это истинное зрение образованного человека. </p>
                  <p>Ведическая астрология — наука, которая:</p>
                  <ul>
                  <li>Позволяет выбрать самый благоприятный
                  сценарий жизни;</li>
                  <li>Открывает человеку его природу, предназначение,
                  призвание, долг и цель жизни, позволяет изменить
                  черты характера, которые непосредственно влияют
                  на судьбу;</ul>
              </div>
              <div class="col-1-2 col-sm-1-1">
                <p>В Академии мы стремимся показать Йогу как единую целостную науку, которая очень плотно связана с другими науками о человеке: Ведической медициной — Аюрведа — и астрологией Джйотиш.</p>
                <p>Йога воздействует на все аспекты человека: физический, ментальный, эмоциональный, психологический и духовный. Йога означает баланс и гармонию тела, ума и эмоций. Йога – это наука о счастливой жизни!</p>
              </div>
            </div>
          </div>
          <div class="line-divider margin-bottom90"></div>
        </div>
      </div>
    
 <script type="text/javascript">
  var boxes = document.getElementsByClassName('course__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>  

<?php get_footer(); ?>
