<?php
/*
Template Name: page-38 (intro-yoga)
*/


 get_header(); ?>

<div class="banner banner-inner page-22 page-38">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-logotype wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s"></h2>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">КАК РАБОТАЕТ  КЛАССИЧЕСКАЯ ЙОГА?</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s" style="color: #00c0f3">Видео курс</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>Йога – это больше, чем здоровье, это больше чем красивое сильное гибкое тело. <br>
Йога – это древнейшее учение, которое даёт знание о том, как обрести гармонию <br>внутри себя и с окружающим миром. <br>
Йога – это связь, соединение с тем, что откроет каждый для себя внутри и вовне, <br>встав и непрерывно идя по великому пути йоги. <br>
Welcome to yoga life style</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro" style="padding-bottom: 200px;">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>УЧИТЕЛЬ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro">
    <div class="container">
      <div class="teacher-intro" style="margin: -312px auto 0;">
          <div class="teacher-name">
            <h2>
              <div class="title__subtext">Вина Ачарья</div>
            </h2>
            <span>Основатель школы йоги Yoga Dham Ashram в Наггаре <br>
Степень Мастера Science of Living (Науки жизни) <br>
Джайн Вишва Бхарати Института в Ладун <br>
</span>
          </div>
          
          <span class="teacher-descr">Потомственная носительница ведической культуры и знаний из семьи действующих браминов (высшая каста в Индии), истории рода которого более 2000 лет. Преемница и гуру парампары  Свами Ниранджананда Сарасвати – Свами Сатьянанда Сарасвати – Свами Шивананда Сарасвати - …, способная и готовая передать древнейшие сакральные знания своим ученикам.   </span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <div class="course-info__desc"><span class="course-info__user">ВИНА АЧАРЬЯ О ЙОГЕ:</span>
              <div class="divider"></div>
              <ul>
                <li>«Йога» - означает единство. Это самое ценное наследие прошлого. </li>
                <li>Это насущная необходимость сегодняшнего дня будущего.</li>
                <li>Йога воздействует на все аспекты человека: физический, 
                ментальный, эмоциональный, психологический и духовный. </li>
                <li>Йога означает баланс и гармонию тела, ума и эмоций. </li>
                <li>Йога – это наука о счастливой жизни!»</li>
              </ul> 
              <span class="course-info__user course-info__user-into">ЙОГА ДАЁТ ВОЗМОЖНОСТЬ</span>
              <div class="divider"></div>
              <ul>
                <li>Увидеть по-новому себя самого. </li>
                <li>Получить доступ к знаниям, скрытым внутри вас. </li>
                <li>Развить физическое, эмоциональное и умственное тело. </li>
                <li>Быть осознанными каждое мгновение вашей жизни. </li>
                <li>Наполняться энергией. </li>
                <li>Ощутить полноту и ценность жизни. </li>
                <li>Обрести гармонию и научиться чувствовать себя счастливым в любых обстоятельствах. </li>
                <li>Понять смысл вашей жизни и ваше предназначение. </li>

              </ul>
            </div>
          </div>
        </div>
      </div>
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc">
              <span class="course-info__user">ДАННЫЙ КУРС ВКЛЮЧАЕТ:</span>
              <div class="divider"></div>
              <ul>
                <li>Лекции</li>
                <li>Практические занятия</li>
              </ul>
              <span class="course-info__user course-info__user-into">ТЕМАТИКА КУРСА:</span>
              <div class="divider"></div>
              <ul>
                <li>Что такое йога.</li>
                <li>Цель йоги. </li>
                <li>Парампара в йоге. </li>
                <li>Ступени и виды йоги. </li>
                <li>Карма. Закон причины и следствия.</li>
                <li>Что такое энергия и как ей управлять.</li>
                <li>Подготовка к пранаямам.</li>
                <li>Как справляться со стрессом и физической болью. </li>
                <li>Базовые асаны отстройка.</li>
                <li>Простые виньясы </li>
              </ul>  
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full " href="/pricelist-single-courses/#love-yourself">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img1.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img2.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img3.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Видео<br>&nbsp;</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">9 тем — 9 видео<br>&nbsp;</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Нет<br>&nbsp;</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">—<br>&nbsp;</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">КАК РАБОТАЕТ КЛАССИЧЕСКАЯ ЙОГА?</span><br>
                              <span class="extra-light-subtitle">Видео</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Видео, 1.5 часа, 9 видео</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>Бесплатно <p><a href="/cart/?add-to-cart=3831">Изучить</a></p></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
    </div>
  </div>
</div>


<div class="intro-course-result-end">
  <div class="container">
    
  </div>
</div>

<div class="course-result-end course-result-end_intro">
  <div class="container">
    <p style="text-align: center;">
      Особенностью данного курса является его универсальность. <br><br>
      И если вы раньше считали, что йога – не для вас, потому что у вас не хватает гибкости, то с нами вы можете смело начать заниматься. <br>
      Вы поймёте, что йогой может заниматься абсолютно каждый человек, даже если у него есть физические ограничения. Ведь главное в йоге – это не асаны. Пройдя этот курс вы поймёте, как далеко выходит йога за пределы коврика. Вина Ачарья даст вам не только уникальные знания, но и поможет вам понять, как вы можете применять полученные знания в вашей повседневной жизни. Кому-то йога поможет решить какие-то внутренние проблемы, избежать депрессивных и негативных состояний. Кому-то решить семейные проблемы и наладить взаимоотношения с близкими. Кому-то выстроить успешно карьеру. Кому-то найти свой духовный путь вне зависимости от вашего вероисповедания. А кто-то с удивлением обнаружит, что с течением времени его физическое тело изменилось так, что он стал способен делать такие физические упражнения, которые казались ему не доступны ранее. <br><br>
      Йога теперь принадлежит всем и доступна для каждого!<br><br>
    </p>
    <!-- <div class="divider"></div> -->
    <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink title-divider">КУРС БУДЕТ ИНТЕРЕСЕН КАК НОВИЧКАМ, ТАК И ОПЫТНЫМ ПРАКТИКАМ, <br>ЖЕЛАЮЩИМ ПОЧУВСТВОВАТЬ ТОНКОСТИ ЙСТИННОЙ ЙОГИ, <br>ПЕРЕДАВАЕМОЙ ПО ПАРАМПАРЕ В ТРАДИЦИЯХ БИХАРСКОЙ ШКОЛЫ. </h3>
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><a class="btn btn--full" href="/cart/?add-to-cart=3831">Добро пожаловать</a></div>
  </div>
</div>



<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

var boxes = document.getElementsByClassName('course-info__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>

<?php get_footer(); ?>
