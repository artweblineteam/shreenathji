<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shreenathji
 */

get_header();
?>
<div class="blog__grid archive-template">

	<div class="container">

		<div class="grid grid-top">
				<div class="col-3-4 col-sm-1-1 col-xs-1-1">
						<div id="primary" class="content-area archive-posts">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				 
				<?php
				the_archive_title( '<h1 class="page-title"><div class="title__main"><span>', '</span></div></h2>' );

				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'archive' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
				</div>
				<div class="col-1-4 col-sm-1-2 col-xs-1-1">
		<?php get_sidebar(); ?>
	</div>
		</div>
	</div>
</div>	


<?php
get_footer();
