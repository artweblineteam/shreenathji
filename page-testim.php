<?php
/*
Template Name: page-15 (testimonials)
*/


 get_header(); ?>
<div class="title-inner contacts">
    <div class="container">
      <h2 class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Отзывы</span></div>
            <div class="title__subtext title-24">Студентов академии</div>
     </h2>
    </div>
</div>

<div class="testim-video">
	<div class="container">
		<iframe width="1150" height="650" src="https://www.youtube.com/embed/Xv8W43q4is4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>
</div>

<div class="testim-grid-block">
	<div class="container">
		<?php echo do_shortcode('[testimonial_view id="2"]'); ?>
	</div>
</div>

<div class="testim-slider-block">
	<div class="container">
		<?php echo do_shortcode('[testimonial_view id="1"]'); ?>
	</div>
</div>

<script type="text/javascript">   
</script>

<?php get_footer(); ?>