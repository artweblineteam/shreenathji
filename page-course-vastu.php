<?php
/*
Template Name: page-24 (kyrs III)
*/


 get_header(); ?>

<div class="banner banner-inner page-24">
  <div class="container">
    <div class="banner__logo">
      <div class="logo_wings wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">курс III</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">ПОТОК, ЦИВИЛИЗАЦИЯ<br>
ПУТЬ К СЧАСТЬЮ  «СОЗНАНИЕ ВИШНУ»</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <h2 class="title__wrapper title__wrapper--how" >
            <div class="title__main">Лидер</div>
          </h2>
      <div class="banner__text-subtitle">
        <span>Психология общества   •   Общество   •   Осознанность   •   Успех лидера   •   Практики Йоги
<br>Васту Дизайнер</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Профессиональный курс Васту дизайна<br>
Курс доступен после II ступени<br></a>
    </div>
  </div>
</div>

<div class="mentors">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main">Опытные</div>
            <div class="title__main with-border"><span>Наставники</span></div>
            <div class="title__subtext">ИЗ РАЗНЫХ ДРЕВНЕЙШИХ ВЕДИЧЕСКИХ ШКОЛ <br> ПОДЕЛЯТСЯ С ВАМИ ЗНАНИЯМИ</div>
     </div>

     <div class="mentors-list">
      <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/small_lotous.png">
       <div class="title__subtext title__light--pink">Индия</div>
       <ul>
         <li>Алакх Ниранжан</li>
         <li>Самвел Саакян</li>
         <li>Марина Никитина</li>
         <li>Вина Ачария</li>
         <li>Ади Шакти</li>
       </ul>
       <div class="mentor-feature">
         <h3 class="title__light title__light--pink">Онлайн 21 день</h3>
         <h3 class="title__light title__light--pink">Очно 14 день</h3>
       </div>
     </div>

  </div>
</div>


<div class="course-info">
    <div class="container">
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <span class="course-info__user">В основе курса</span>
              <div class="divider"></div>
              <ul>
                <li>Знание психологии общества (Веды);</li>
                <li>Знание законов и навыки успеха Лидера (Веды);</li>
                <li>Практики Йоги (Классическая и Раджа Йога).</li>
              </ul>  
              <div class="divider"></div>
              <span class="title__light--pink">Вы обретете:</span>
              <ul>
                <li>Преодоление зависимости от успеха (Гьяна-майа);</li>
                <li>Внутреннюю свободу и непривязанность к целям;</li>
                <li>Осознанное участие в управлении судьбой материального мира;</li>
                <li> Благородную, востребованную, уникальную профессию;</li>
                <li> Возможность путешествовать по всему миру;</li>
                <li> Путь к безусловному счастью (Кама);</li>
                <li> Сознание Лидера (Кшатрий)</li>
                <li> Профессию Васту дизайнера.</li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">Курс передает <br>фундаментальные знания</span>
              <div class="divider"></div>
              <span class="title__light--pink">Искусство жизни:</span>
              <ul>
                <li>Знание природы психологии общества;</li>
                <li>Законы развития общества и достижения успеха в обществе (Кама).</li>
              </ul>  
              <span class="title__light--pink">Классическая Йога и Йога-терапия:</span>
              <ul> 
                <li>Фундаментальный курс Ведической архитектуры Васту;</li>
                <li>12 законов Васту;</li>
                <li>Методы коррекции негативных влияний;</li>
                <li>Васту во всех сферах жизни;</li>
                <li>Здоровье, деньги, путешествия, отношения и пр.;</li>
                <li>Лучшие программы Васту;</li>
                <li>Свой проект под руководством мастера.</li>
              </ul>
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a href="/pricelist/#faculty-iii-vastu" class="btn btn--full">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img1.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img2.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img3.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main">Программа</div>
            <div class="title__main with-border"><span>III курса</span></div>
            <div class="title__subtext">Факультет ведической арихитектуры <br>Васту</div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">Базовые <br>знания</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    tr><td>Психология общества</td><td class="hours">5ч</td></tr>
                    <tr><td>Цивилизация </td><td class="hours">2ч</td></tr>  
                    <tr><td>Палмистри, Нумерология</td><td class="hours">2ч</td></tr>
                    <tr><td>Осознанность  </td><td class="hours">5ч</td></tr>
                    <tr><td>Сознание Свидетеля  </td><td class="hours">5ч</td></tr>
                    <tr><td>Искусство управления</td><td class="hours">5ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">24 часа</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">&nbsp;<br>Специализация</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Все понятия  Васту</td><td class="hours">4ч</td></tr>
                    <tr><td>12 законов Васту</td><td class="hours">12ч</td></tr>  
                    <tr><td>Методы коррекции. Васту</td><td class="hours">8ч</td></tr>
                    <tr><td>Здоровье, брак и Васту</td><td class="hours">5ч</td></tr>
                    <tr><td>Деньги, успех и Васту</td><td class="hours">5ч</td></tr>
                    <tr><td>Программы Васту </td><td class="hours">2ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">36 часов</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">&nbsp;<br>Основы йоги</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Асаны среднего уровня</td><td class="hours">6ч</td></tr>
                    <tr><td>Нети, Кунджал и Шанкха</td><td class="hours">6ч</td></tr>  
                    <tr><td>Пранаяма, Мудры, Бандха</td><td class="hours">6ч</td></tr>
                    <tr><td>Пратьяхара (Нидра, Мауна)</td><td class="hours">6ч</td></tr>
                    <tr><td>Хридьякаш Дхарана</td><td class="hours">6ч</td></tr>
                    <tr><td>Виньяса йога, Джапа </td><td class="hours">6ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">36 часов</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">Практики <br>(сатсанги)</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Практики осознанности</td><td class="hours">4ч</td></tr>
                    <tr><td>Искусство управления</td><td class="hours">2ч</td></tr>  
                    <tr><td>Джйотиш</td><td class="hours">6ч</td></tr>
                    <tr><td>Вебинары</td><td class="hours">18ч</td></tr>
                    <tr><td>&nbsp;</td><td class="hours">&nbsp;</td></tr>
                    <tr><td>&nbsp;</td><td class="hours">&nbsp;</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">30 часов</span>
              </div>
            </div>    

     </div>
     <span class="programm-course-iii-subtitle">Всего: 126 часа</span>
     <div class="day-schedule">
       <div class="title__wrapper">
          <div class="title__pink title__h2">Распорядок дня</div>
          <div class="with-border with-border__long"><span>III курса</span></div>
        </div>
        <table>
          <tr class="first-line"><td class="schedule-hours">5:00 — 5:30</td><td>Подъем, прохладное омовение, прием воды с лимоном (0.6-1л) и трифалы</td></tr>
          <tr><td class="schedule-hours">5:30 — 6:20</td><td>Практики созерцания — медитации в храме, в классе, в зале</td></tr>
          <tr><td class="schedule-hours">6:20 — 7:00</td><td>Йога пробуждения: очистительные практики, спец. комплекс</td></tr>
          <tr><td class="schedule-hours">7:00 — 8:00</td><td>Завтрак</td></tr>
          <tr><td class="schedule-hours">8:00 — 13:00</td><td>Основная лекция по программе</td></tr>
          <tr><td class="schedule-hours">13:00 — 18:30</td><td>Обед и отдых</td></tr>
          <tr><td class="schedule-hours">18:30 — 20:00</td><td>Йога</td></tr>
          <tr class="last-line"><td class="schedule-hours">21:00</td><td>Приглашение ко сну</td></tr>
        </table>
     </div>
     <div class="button__list grid grid-top">
      <div class="col-1-2 col-sm-1-1"><div class="btn btn--full programm-more">ПРОГРАММА ПОДРОБНЕЕ</div></div>
      <div class="col-1-2 col-sm-1-1"><div class="btn btn--full schedule-more">РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ</div></div>
       
     </div>
    </div>
</div>

<div class="course-iii-descr">
  <div class="container">
    <div class="course-iii-descr-text">
      <p>В Академии студенты обретают не только востребованные профессии, но и уникальные навыки развития сознания, а также полное Мировоззрение</p>
    </div>  
      <div class="line-divider-short"></div>
      <div class="course-iii-descr-text">
      <p>Для результативного обучения до начала курса необходимо ознакомиться с рекомендованной литературой, аудио- и видео-записями в соответствии с вашим курсом</p>
    </div>
  </div>
</div>
<div class="btn__wrapper btn__wrapper--center-overlapped"><a href="/library/" class="btn btn--full">В библиотеку</a></div>


<div class="course-price">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Стоимость</span></div>
            <div class="title__subtext">обучения и практик</div>
     </div>
     <div class="table-inner table__third">
        <div class="table-price price-list-first">
            <table>
                  <tbody>
<tr>
                        <th>
                              <span>&nbsp;<br>III курс</span>
                        </th>
                        <th>
                          <span>«Онлайн»</span></br>
                          <span class="extra-light-subtitle">Онлай/видео, 1 год</span>
                        </th>
                        <th>
                          <span>«Очно» </span></br>
                          <span class="extra-light-subtitle">Очно/видео, 1 год</span>
                        </th>
                        <th>
                          <span>«Эксклюзив»</span></br>
                          <span class="extra-light-subtitle">Онлай/очно/видео, 3 года</span>
                        </th>
                  </tr>
                  <tr class="table-subtitle">
                        <td>
                              <span>Программа</span>
                        </td>
                        <td>
                             <span>Видео уроки + практики,<br> час.</span>
                        </td>
                        <td >
                              <span>Видео уроки + практики,<br> час.</span>
                        </td>
                        <td>
                              <span>Видео уроки + практики,<br> час.</span>
                        </td>
                  </tr>
                  <tr>
                      <td>Видео записи основ курса</td>
                      <td>54</td>
                      <td>54</td>
                      <td>54</td>
                  </tr>
                  <tr>
                        <td>Искусство Жизни</td>
                        <td>24</td>
                        <td>24</td>
                        <td>24</td>
                  </tr>
                  <tr>
                        <td>Практики йоги</td>
                        <td>36</td>
                        <td>36</td>
                        <td>36</td>
                  </tr>
                  <tr>
                        <td>Специализация</td>
                        <td>48</td>
                        <td>48</td>
                        <td>48</td>
                  </tr>
                  <tr>
                        <td>Вебинары - специализация</td>
                        <td>да</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Сопровождение куратора</td>
                        <td>да</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Сопровождение наставником</td>
                        <td>да</td>
                        <td>да</td>
                        <td>нет</td>
                  </tr>
                  <tr>
                        <td>Сопровождение основателем </td>
                        <td>нет</td>
                        <td>нет</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Уникальный учебник III курса PDF</td>
                        <td>нет</td>
                        <td>нет</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Участие в консультации мастера</td>
                        <td>да</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Курсовая работа, специальность</td>
                        <td>да</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Сертификат об окончании курса</td>
                        <td>да</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr class="table-subtitle-blue">
                              <td>ИТОГО ЧАСОВ</td>
                              <td>162 <br><span class="extra-light-subtitle">(108 онлайн и 54 в записи)</span> </td>
                              <td>162 <br><span class="extra-light-subtitle">(108 очно и 54 в записи)</span></td>
                              <td>162 <br><span class="extra-light-subtitle">(108 очно и 54 в записи)</span></td>
                  </tr>
                  <tr>
                        <td>Полная стоимость</td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2140"]') ?> <a href="/cart/?add-to-cart=2140">купить</a></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2141"]') ?> <a href="/cart/?add-to-cart=2141">купить</a></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2145"]') ?> <a href="/cart/?add-to-cart=2145">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость со скидкой для пар (с чел.) <br>*При оплате за двоих с одного счета.</td>
                        <td><?php echo do_shortcode('[woocommerce_price id="3978"]') ?> <a href="/cart/?add-to-cart=3978">купить</a></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2143"]') ?> <a href="/cart/?add-to-cart=2143">купить</a></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2147"]') ?> <a href="/cart/?add-to-cart=2147">купить</a></td>
                  </tr>
            </tbody></table>
            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>
      </div>
      <?php //the_content();?>
    </div>
  </div>
</div>

<div class="course-conditions">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">учебы, проживания и отдыха</div>
            <div class="title__subtext title__light--pink">Для очных студентов</div>
     </div>
     <h3 class="title__wrapper title__light-no-bottom-margin">Расписание ближайших курсов смотрите на странице</h3>
    <div class="btn__wrapper btn__wrapper--center">
        <a href="/fakultety/#schedule" class="btn btn--full ">Факультеты</a>
    </div>
    <div class="course-conditions-place">
      <h3 class="title__wrapper title__light-no-bottom-margin">В Индии курс проходит в вариантах «Стандарт» и «Элит»</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Программа III курса в Индии проходит в Наггаре, Гималаи</h3>
    </div>

    <div class="course-conditions-info">
      <span class="title__light--pink">Наггар – древняя столица Индии в Гималаях</span>
      <div class="divider"></div>
          <p>Курс проходит в ашраме Храма Кришны.
Старейший духовный центр мира (более 5000 лет). 
Уникальная природа. Геотермальные источники. 
Ежедневные Арати и Киртаны. Вход в Шамбалу. 
А также усадьба мистика и художника Н. Рериха.
<br><br>Прибыть в аэропорт Нью Дели до 12:00 за день да начала.
Самолёт из а/п Нью Дели должен отправляться после 15:00 
на следующий день после окончания ретрита.</p>
    </div>

    <div class="course-conditions-image-slider grid grid-top">
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
       <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider1.png">
     </div>
     <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider2.png">
       </div> 
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider3.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider3.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider2.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider1.png">
      </div>
      
    </div>
    <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Программа предусматривает два выходных
<br>с возможностью организации экскурсий</h3>
  </div>
</div>
</div>

<div class="coast-place">
  <div class="container">
        <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Стоимость</span></div>
            <div class="title__subtext">размещения</div>
            <div class="title__subtext title__light--pink">оплата на месте</div>
     </div>
     <div class="coast-place-features grid grid-top">
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon1.png">
           <span class="title__light--pink">2-местное размещение</span>
           <small>15 дней, 14 ночей</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td width="40%" ><span>Эконом</span></td>
                <td width="60%" style="text-align: right;"><span>$140 ($10 руб./чел.)</span></td>
              </tr>
              <tr>
                <td width="40%"><span>Стандарт</span></td>
                <td width="60%" style="text-align: right;"><span>$280 ($20/чел.)</span></td>
              </tr>
              <tr>
                <td width="40%"><span>Эксклюзив</span></td>
                <td width="60%" style="text-align: right;"><span >$560 ($40/чел.)</span></td>
              </tr>
            </table>
           </div>
           </div>
         </div>
         <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon2.png">
           <span class="title__light--pink">Завтрак, обед</span>
           <small>Йогическое, здоровое питание</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td width="40%" ><span>Стандарт</span></td>
                <td width="60%" style="text-align: right;"><span>$140 ($10 сут./чел.)</span></td>
              </tr>
              <tr>
                <td width="40%"><span>Эксклюзив</span></td>
                <td width="60%" style="text-align: right;"><span>$210 ($15 сут./чел.)</span></td>
              </tr>
              <tr>
                <td width="40%"><span>&nbsp;</span></td>
                <td width="60%" style="text-align: right;"><span >&nbsp;</span></td>
              </tr>
            </table>
           </div>
           </div>
         </div>
         <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon3.png">
           <span class="title__light--pink">Отдых</span>
           <small>Экскурсии, сопровождение</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td style="text-align: center;"><span>$50/чел.</span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table>
           </div>
           </div>
         </div>
       </div>
      <div class="coast-place-features__list">
        <ul>
          <li>* Возможно размещение членов семьи без участия в программе (условия проживания те же)</li>
          <li>* Размещение приезжих участников вне центра – невозможно.</li>
          <li>* Студенты с годовой и более визой могут жить за пределами центра. Доп. платеж за услуги $50.</li>
          <li>* При существенном изменении курса доллара стоимость в рублях может измениться.</li>
        </ul>
      </div>
     </div>
  </div>
</div>

<div class="trip-coast">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main"><span>Стоимость</span></div>
            <div class="title__subtext">путешествия</div>
     </div>
     <div class="trip-coast-features grid grid-top">
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon1.png">
           <div class="divider"></div>
           <p>Перелет из Москвы в Нью Дели — ориентировочная стоимость</p>
           <p class="blue-text">$400 /чел.</p>
         </div>
       </div>
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon2.png">
           <div class="divider"></div>
           <p>Трансфер из аэропорта и обратно — ориентировочная стоимость</p>
           <p class="blue-text">$60/чел.</p>
         </div>
       </div>
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon3.png">
           <div class="divider"></div>
           <p>Электронная виза — <span class="blue-text">$100/чел.</span></p>
           <p>Виза в посольстве — <span class="blue-text">$60/чел.</span></p>
         </div>
       </div>
     </div>
     <small>* Полная стоимость путешествия и обучения — от $1200</small>
  </div>
</div>
<div class="btn__wrapper btn__wrapper--center-overlapped"><a href="/pricelist/#faculty-iii-vastu" class="btn btn--full">оплатить КУРС</a></div>

<div class="course-sert">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="title__subtext">По окончании курса</div>
            <div class="title__main with-border"><span>вы получите</span></div>
     </div>
     <div class="sertificate sert_three">
        <h3 class="title__light title__light--pink">Сертификат</h3>
        <span>Сертификат о прохождении курса</span>
     </div>
  </div>
</div>

<div class="course-result-title">
  <div class="container">
        <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Результаты</span></div>
            <div class="title__subtext">III курса</div>
     </div>
    <h3 class="title__wrapper title__light-no-bottom-margin">Студенты академии получают на курсе <br>не только уникальную квалификацию, но и развиваются <br>в материальном и духовном плане</h3>
  </div>
</div>

<div class="course-result">
  <div class="container">
    <div class="you-can">
      <div class="you-can-text"> 
        <h3 class="title__light-no-bottom-margin title__light--pink">Вы сможете:</h3>
        <ul>
          <li>Получить навык мышления Лидера (Кшатрий);</li>
          <li>Преодолеть зависимость от успеха (Гьяна-майа);</li>
          <li>Достичь внутренней свободы и непривязанности к целям;</li>
          <li>Изучить Законы развития и деградации общества;</li>
          <li>Принять осознанное участие в управлении судьбой материального мира;</li>
          <li>Узнать путь достижения успеха в обществе;</li>
          <li>Обрести инструменты достижения счастья (Кама).</li>
        </ul>
      </div>
    </div>
  </div>
</div>  

<div class="course-result-end">
  <div class="container">
    <div class="yoga-consult">
      <h3 class="title__wrapper title__light title__light--pink btn__wrapper--center-overlapped">Вы обретете профессию
<br>мастера Джйотиш:</h3>
      <ul>
        <li>Получите фундаментальный курс Ведической архитектуры Васту;</li>
        <li>Познаете 12 законов Васту;</li>
        <li>Изучите методы коррекции негативных влияний;</li>
        <li>Познакомитесь с использованием Васту во всех сферах жизни;</li>
        <li>Здоровье, деньги, путешествия, отношения;</li>
        <li>Изучите программы Васту;</li>
        <li>На практике создадите свой проект.</li>
      </ul>
    </div>
    <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
    <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    <div class="btn__wrapper btn__wrapper--center"><a href="/pricelist/#faculty-iii-vastu" class="btn btn--full">Стоимость курса</a></div>
  </div>
</div>
<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

$('.programm-more').click(function(){
    $('.programm-hidden-data').slideToggle(300, function(){
      if ($(this).is(':hidden')) {
        $('.programm-more').html('ПРОГРАММА ПОДРОБНЕЕ');
      } else {
        $('.programm-more').html('Скрыть ПРОГРАММУ');
      }             
    });
    return false;
});

$('.schedule-more').click(function(){
    $('.day-schedule').slideToggle(300, function(){
      if ($(this).is(':hidden')) {
        $('.schedule-more').html('РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ');
      } else {
        $('.schedule-more').html('Скрыть РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ');
      }             
    });
    return false;
});

</script>

<?php get_footer(); ?>
