<?php
/*
Template Name: page-26 (kyrs IV)
*/


 get_header(); ?>

<div class="banner banner-inner page-26">
  <div class="container">
    <div class="banner__logo">
      <div class="logo_om wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">курс IV</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">СОЗЕРЦАНИЕ МИРОЗДАНИЯ <br>«СОЗНАНИЕ АБСОЛЮТА»</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <h2 class="title__wrapper title__wrapper--how" >
            <div class="title__main">НАСТАВНИК</div>
          </h2>
      <div class="banner__text-subtitle">
        <span>Знание о Всевышнем   •   Любовь и Свобода   •   Успех наставника   •   Классическая Йога <br>Наставник Йоги</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Профессиональный курс Классической Йоги <br>Курс доступен только после III ступени<br></a>
    </div>
  </div>
</div>

<div class="mentors">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main">Опытные</div>
            <div class="title__main with-border"><span>Наставники</span></div>
            <div class="title__subtext">ИЗ РАЗНЫХ ДРЕВНЕЙШИХ ВЕДИЧЕСКИХ ШКОЛ <br> ПОДЕЛЯТСЯ С ВАМИ ЗНАНИЯМИ</div>
     </div>

     <div class="mentors-list">
      <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/small_lotous.png">
       <div class="title__subtext title__light--pink">Индия</div>
       <ul>
         <li>Алакх Ниранжан</li>
         <li>Самвел Саакян</li>
         <li>Марина Никитина</li>
         <li>Вина Ачария</li>
         <li>Ади Шакти</li>
         
       </ul>
       <div class="mentor-feature">
         <h3 class="title__light title__light--pink">Онлайн 21 день</h3>
         <h3 class="title__light title__light--pink">Очно 14 день</h3>
       </div>
     </div>

  </div>
</div>


<div class="course-info">
    <div class="container">
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <span class="course-info__user">В основе курса</span>
              <div class="divider"></div>
              <ul>  
                <li>Знание о Всевышнем (Веды);</li>
                <li>Знание Бхакти Йоги (Веды);</li>
                <li>Практики Йоги (Классическая и Раджа Йога).</li>
              </ul>  
              <div class="divider"></div>
              <span class="title__light--pink">Вы обретете:</span>
              <ul>
                <li>Инструменты достижения блаженства (Вигьяна-майа и Ананда);</li>
                <li>Непривязанность к Знанию;</li>
                <li>Навыки управления материальным миром;</li>
                <li>Путь к безусловной Любви;</li>
                <li>Практики служения и жертвоприношения;</li>
                <li>Инструменты для достижения свободы (Мокша);</li>
                <li>Сознание Наставника (Брамин);</li>
                <li>Посвящение от хранителей древнейших школ на Земле.</li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">Курс передает <br>фундаментальные знания</span>
              <div class="divider"></div>
              <span class="title__light--pink">Искусство жизни:</span>
              <ul>
                <li>О Боге в разных традициях, история Знания;</li>
                <li>Навыки созерцания Всевышнего (Бхакти-йога);</li>
                <li>Законы наставничества, путь наставника;</li>
                <li>Достижение успеха в наставничестве.</li>
              </ul>  
              <span class="title__light--pink">Классическая Йога:</span>
              <ul> 
                <li>Йога-терапия;</li>
                <li>Построение индивидуальных комплексов;</li>
                <li>Йога планет (уровень личной энергии);</li>
                <li>Специальные инструменты Раджа-йоги.</li>
                <li>Специальные знания для наставников Йоги.</li>
              </ul>
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <a class="btn btn--full " href="/pricelist/#faculty-iv-yoga">записатся на курс</a>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img4.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img5.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img6.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main">Программа</div>
            <div class="title__main with-border"><span>IV курса</span></div>
            <div class="title__subtext">Факультет Классической Йоги <br>и Йога-терапии</div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">Базовые <br>знания</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>О Боге в разных традициях</td><td class="hours">5ч</td></tr>
                    <tr><td>Любовь. Обращение</td><td class="hours">2ч</td></tr>  
                    <tr><td>Знаки в природе и на теле</td><td class="hours">2ч</td></tr>
                    <tr><td>Путь наставника</td><td class="hours">5ч</td></tr>
                    <tr><td>Остановленное сознание</td><td class="hours">5ч</td></tr>
                    <tr><td>Искусство наставничества</td><td class="hours">5ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">24 часа</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">&nbsp;<br>Специализация</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Кундалини Крийя Йога</td><td class="hours">6ч</td></tr>
                    <tr><td>Методика  занятий</td><td class="hours">6ч</td></tr>  
                    <tr><td>Заболевания, йога-терапия</td><td class="hours">6ч</td></tr>
                    <tr><td>Антаранга йога, Садхана</td><td class="hours">6ч</td></tr>
                    <tr><td>Свара йога, путь гармонии</td><td class="hours">6ч</td></tr>
                    <tr><td>Бхагти Йога, Этика учителя</td><td class="hours">6ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">36 часов</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">&nbsp;<br>Основы йоги</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Продвинутые асаны </td><td class="hours">6ч</td></tr>
                    <tr><td>Пранаяма с задержкой и пр.</td><td class="hours">6ч</td></tr>  
                    <tr><td>Мудры, Бандха</td><td class="hours">6ч</td></tr>
                    <tr><td>Пратьяхара (Нидра, Мауна)</td><td class="hours">6ч</td></tr>
                    <tr><td>Чидакаш Дхарана</td><td class="hours">6ч</td></tr>
                    <tr><td>Виньяса йога, Джапа</td><td class="hours">6ч</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">36 часов</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature">
                <span class="programm-course-iii-title title__light--pink">Практики <br>(сатсанги)</span>
                <div class="divider"></div>
                <div class="programm-hidden-data">
                  <table>
                    <tr><td>Нейропсихология</td><td class="hours">4ч</td></tr>
                    <tr><td>Психосоматика</td><td class="hours">4ч</td></tr>  
                    <tr><td>Секреты Кармы</td><td class="hours">4ч</td></tr>
                    <tr><td>&nbsp;</td><td class="hours">&nbsp;</td></tr>
                    <tr><td>&nbsp;</td><td class="hours">&nbsp;</td></tr>
                    <tr><td>&nbsp;</td><td class="hours">&nbsp;</td></tr>
                  </table>
                  <div class="divider"></div>
                </div>
                <span class="programm-course-iii-subtitle">12 часов</span>
              </div>
            </div>    

     </div>
     <span class="programm-course-iii-subtitle">Всего: 108 часа</span>
     <div class="day-schedule">
       <div class="title__wrapper">
          <div class="title__pink title__h2">Распорядок дня</div>
          <div class="with-border with-border__long"><span>IV курса</span></div>
        </div>
        <table>
          <tr class="first-line"><td class="schedule-hours">5:00 — 5:30</td><td>Подъем, прохладное омовение, прием воды с лимоном (0.6-1л) и трифалы</td></tr>
          <tr><td class="schedule-hours">5:30 — 6:20</td><td>Практики созерцания — медитации в храме, в классе, в зале</td></tr>
          <tr><td class="schedule-hours">6:20 — 7:00</td><td>Йога пробуждения: очистительные практики, спец. комплекс</td></tr>
          <tr><td class="schedule-hours">7:00 — 8:00</td><td>Завтрак</td></tr>
          <tr><td class="schedule-hours">8:00 — 13:00</td><td>Основная лекция по программе</td></tr>
          <tr><td class="schedule-hours">13:00 — 18:30</td><td>Обед и отдых</td></tr>
          <tr><td class="schedule-hours">18:30 — 20:00</td><td>Йога</td></tr>
          <tr class="last-line"><td class="schedule-hours">21:00</td><td>Приглашение ко сну</td></tr>
        </table>
     </div>
     <div class="button__list grid grid-top">
      <div class="col-1-2 col-sm-1-1"><div class="btn btn--full programm-more">ПРОГРАММА ПОДРОБНЕЕ</div></div>
      <div class="col-1-2 col-sm-1-1"><div class="btn btn--full schedule-more">РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ</div></div>
     </div>
    </div>
</div>

<div class="course-iii-descr">
  <div class="container">
    <div class="course-iii-descr-text">
      <p>В Академии студенты обретают не только востребованные профессии, но и уникальные навыки развития сознания, а также полное Мировоззрение</p>
    </div>  
      <div class="line-divider-short"></div>
      <div class="course-iii-descr-text">
      <p>Для результативного обучения до начала курса необходимо ознакомиться с рекомендованной литературой, аудио- и видео-записями в соответствии с вашим курсом</p>
    </div>
  </div>
</div>
<div class="btn__wrapper btn__wrapper--center-overlapped"><a href="/library/" class="btn btn--full">В библиотеку</a></div>


<div class="course-price">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Стоимость</span></div>
            <div class="title__subtext">обучения и практик</div>
     </div>
     <div class="table-inner table__third">
      <div class="table-price price-list-first">
            <table>
                  <tbody>
                  <tr>
                        <th>
                              <span>&nbsp;<br>IV курс</span>
                        </th>
                       <th>
                          <span>«Онлайн»</span></br>
                          <span class="extra-light-subtitle">Онлай/видео, 1 год</span>
                        </th>
                        <th>
                          <span>«Очно» </span></br>
                          <span class="extra-light-subtitle">Очно/видео, 1 год</span>
                        </th>
                        <th>
                          <span>«Эксклюзив»</span></br>
                          <span class="extra-light-subtitle">Онлай/очно/видео, 3 года</span>
                        </th>
                  </tr>
                  <tr class="table-subtitle">
                        <td>
                              <span>Программа</span>
                        </td>
                        <td>
                             <span>Видео уроки + практики,<br> час.</span>
                        </td>
                        <td >
                              <span>Видео уроки + практики,<br> час.</span>
                        </td>
                        <td>
                              <span>Видео уроки + практики,<br> час.</span>
                        </td>
                  </tr>
                  <tr>
                        <td>Видео записи основ курса</td>
                        <td>-</td>
                        <td>18</td>
                        <td>18</td>
                  </tr>
                  <tr>
                        <td>Искусство Жизни</td>
                        <td>-</td>
                        <td>24</td>
                        <td>24</td>
                  </tr>
                  <tr>
                        <td>Практики йоги</td>
                        <td>-</td>
                        <td>36</td>
                        <td>36</td>
                  </tr>
                  <tr>
                        <td>Специализация</td>
                        <td>-</td>
                        <td>48</td>
                        <td>48</td>
                  </tr>
                  <tr>
                        <td>Вебинары - Духовное развитие</td>
                        <td>-</td>
                        <td>9</td>
                        <td>9</td>
                  </tr>
                  <tr>
                        <td>Сопровождение куратора</td>
                        <td>-</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Сопровождение наставником</td>
                        <td>-</td>
                        <td>да</td>
                        <td>нет</td>
                  </tr>
                  <tr>
                        <td>Сопровождение основателем</td>
                        <td>-</td>
                        <td>нет</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Выполнение 11 ягий в Ашраме </td>
                        <td>-</td>
                        <td>нет</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Участие в консультации ИЖ</td>
                        <td>-</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Подготовка Диплома, спец-ть</td>
                        <td>-</td>
                        <td>27</td>
                        <td>27</td>
                  </tr>
                  <tr>
                        <td>Защита Диплома, специальность</td>
                        <td>&nbsp;</td>
                        <td>да</td>
                        <td>да</td>
                  </tr>
                  <tr>
                        <td>Размещение на сайте академии/td>
                        <td>-</td>
                        <td>да, красный диплом</td>
                        <td>да, красный диплом</td>
                  </tr>
                  <tr>
                        <td>Трудоустройство</td>
                        <td>-</td>
                        <td>да, красный диплом</td>
                        <td>да, красный диплом</td>
                  </tr>
                  <tr class="table-subtitle-blue">
                        <td>ИТОГО ЧАСОВ</td>
                        <td>-</td>
                        <td>162<br><span class="extra-light-subtitle"> (программа и видео записи)</span></td>
                        <td>162</td>
                  </tr>
                  <tr>
                        <td>Cтоимость</td>
                        <td>-</td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2165"]') ?> <a href="/cart/?add-to-cart=2165">купить</a></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2169"]') ?> <a href="/cart/?add-to-cart=2169">купить</a></td>
                  </tr>
                  <tr>
                        <td>Стоимость со скидкой для пар (с чел.) <br>*При оплате за двоих с одного счета.</td>
                        <td>-</td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2167"]') ?> <a href="/cart/?add-to-cart=2167">купить</a></td>
                        <td><?php echo do_shortcode('[woocommerce_price id="2171"]') ?> <a href="/cart/?add-to-cart=2171">купить</a></td>
                  </tr>
            </tbody></table>
            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>

    </div>
  </div>
</div>

<div class="course-conditions">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">учебы, проживания и отдыха</div>
            <div class="title__subtext title__light--pink">Для очных студентов</div>
     </div>
     <h3 class="title__wrapper title__light-no-bottom-margin">Расписание ближайших курсов смотрите на странице</h3>
    <div class="btn__wrapper btn__wrapper--center">
        <a href="/fakultety/#schedule" class="btn btn--full ">Факультеты</a>
    </div>
    <div class="course-conditions-place">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Программа IV курса проходит только в Индии:</h3>
      <h3 class="title__wrapper title__light-no-bottom-margin">Для Классической Йоги — в Наггаре, в Ашраме храма Кришны;<br>
Для Джйотиш и Васту — в Радхакунде, в Ашраме древней школы Джйотиш;<br>
Для факульта Аюрведы — в Бинаресе, в Ашраме древней аюрведической школы.</h3>
    </div>
    <div class="course-conditions-infos grid grid-top">
      <div class="col-1-2 col-sm-1-1">
        <div class="course-conditions-info">
          <span class="title__light--pink">Наггар – древняя столица Индии в Гималаях</span>
            <div class="divider"></div>
              <p>Курс проходит в ашраме Храма Кришны.
                Старейший духовный центр мира (более 5000 лет). 
                Уникальная природа. Геотермальные источники. 
                Ежедневные Арати и Киртаны. Вход в Шамбалу. 
                А также усадьба мистика и художника Н. Рериха.
              <br><br>Прибыть в аэропорт Нью Дели до 12:00 за день да начала.
                Самолёт из а/п Нью Дели должен отправляться после 15:00 
                на следующий день после окончания ретрита.
              </p>
       </div>
      </div>
      <div class="col-1-2 col-sm-1-1">
        <div class="course-conditions-info">
          <span class="title__light--pink">Радхакунд – деревня, где взрослел Кришна</span>
            <div class="divider"></div>
              <p>Курс проходит в ашраме Шринатджи.
                Это святые места, как Иерусалим или Мекка.
                Ежедневные Парикрамы, Арати, Ягии. 
                Гавардхан и Кунды, вокруг которых ежеминутно 
                читают мантры тысячи паломников.
                <br><br>Прибыть в аэропорт Нью Дели до 7:00 в день начала.
                Самолёт из а/п Нью Дели должен отправляться 
                после 21:00 в день окончания ретрита.
              </p>
       </div>
      </div>
    
  </div>

    <div class="course-conditions-image-slider grid grid-top">
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
       <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider4.png">
     </div>
     <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider5.png">
       </div> 
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider6.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider3.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider2.png">
      </div>
      <div class="col-1-3 col-sm-1-3 col-xs-1-1">
        <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/page-21-slider1.png">
      </div>
      
    </div>
    <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Программа предусматривает два выходных
<br>с возможностью организации экскурсий</h3>
  </div>
</div>
</div>

<div class="coast-place">
  <div class="container">
        <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Стоимость</span></div>
            <div class="title__subtext">размещения</div>
            <div class="title__subtext title__light--pink">оплата на месте</div>
     </div>
     <div class="coast-place-features grid grid-top">
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon1.png">
           <span class="title__light--pink">2-местное размещение</span>
           <small>15 дней, 14 ночей</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td style="text-align: center;"><span>Донейшин</span></td>
              </tr>
            </table>
           </div>
           </div>
         </div>
         <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon2.png">
           <span class="title__light--pink">Завтрак, обед</span>
           <small>Йогическое, здоровое питание</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td style="text-align: center;"><span>Донейшин</span></td>
              </tr>
            </table>
           </div>
           </div>
         </div>
         <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="coast-place-feature">
          <div class="coast-place-feature-top">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/cf-icon3.png">
           <span class="title__light--pink">Отдых</span>
           <small>Экскурсии, сопровождение</small>
         </div>
           <div class="divider"></div>
           <div class="place-pricing">
            <table width="100%">
              <tr>
                <td style="text-align: center;"><span>Донейшин</span></td>
              </tr>
            </table>
           </div>
           </div>
         </div>
       </div>
      <div class="coast-place-features__list">
        <ul>
          <li>* Возможно размещение членов семьи без участия в программе (условия проживания те же)</li>
          <li>* При существенном изменении курса доллара стоимость в рублях может измениться.</li>
        </ul>
      </div>
     </div>
  </div>
</div>

<div class="trip-coast">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main"><span>Стоимость</span></div>
            <div class="title__subtext">путешествия</div>
     </div>
     <div class="trip-coast-features grid grid-top">
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon1.png">
           <div class="divider"></div>
           <p>Перелет из Москвы в Нью Дели — ориентировочная стоимость</p>
           <p class="blue-text">$400 /чел.</p>
         </div>
       </div>
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon2.png">
           <div class="divider"></div>
           <p>Трансфер из аэропорта и обратно — ориентировочная стоимость</p>
           <p class="blue-text">$60/чел.</p>
         </div>
       </div>
       <div class="col-1-3 col-sm-1-3 col-xs-1-1">
         <div class="trip-coast-feature">
           <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/tc-icon3.png">
           <div class="divider"></div>
           <p>Электронная виза — <span class="blue-text">$100/чел.</span></p>
           <p>Виза в посольстве — <span class="blue-text">$60/чел.</span></p>
         </div>
       </div>
     </div>
     <small>* Полная стоимость путешествия и обучения — от $880</small>
  </div>
</div>
<div class="btn__wrapper btn__wrapper--center-overlapped"><a href="/pricelist/#faculty-iv-yoga" class="btn btn--full">оплатить КУРС</a></div>

<div class="course-sert">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
        <div class="title__subtext">По окончании курса</div>
            <div class="title__main with-border"><span>вы получите</span></div>
     </div>
     <div class="sertificates grid grid-top">
       <div class="col-1-2 col-sm-1-1">
         <div class="sertificate sert_four">
        <h3 class="title__light title__light--pink">Сертификат</h3>
        <span>Сертификат о прохождении курса</span>
     </div>
       </div>
       <div class="col-1-2 col-sm-1-1">
         <div class="sertificate sert_dip">
        <h3 class="title__light title__light--pink">Диплом</h3>
        <span>Диплом, подтверждённый в Индии</span>
     </div>
       </div>
     </div>
  </div>
</div>

<div class="course-result-title">
  <div class="container">
        <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Результаты</span></div>
            <div class="title__subtext">IV курса</div>
     </div>
    <h3 class="title__wrapper title__light-no-bottom-margin">Посвящение в соответствующую науку от величайших учителей
<br>современности. Сертификат о прохождении курса, задание на практику.
<br>После практики – Диплом, подтвержденный в Индии.</h3>
  </div>
</div>

<div class="course-result">
  <div class="container">
    <div class="you-can">
      <div class="you-can-text"> 
        <h3 class="title__light-no-bottom-margin title__light--pink">Вы сможете:</h3>
        <ul>
          <li>Приобрести навык мышления Наставника (Брамин);</li>
          <li>Использовать инструменты достижения блаженства (Вигьяна-майа и Ананда);</li>
          <li>Обрести непривязанность и навыки управления материальным миром;</li>
          <li>Принять Знание о Боге в разных традициях и путь к безусловной Любви;</li>
          <li>Осознать Законы наставничества и путь наставника;</li>
          <li>Обрести инструменты достижения успеха в наставничестве;</li>
          <li>Обрести инструменты достижения полной свободы (Мокша);</li>
          <li>Приобрести навыки Раджа-йоги и созерцания Всевышнего, Бхакти-йога.</li>
        </ul>
      </div>
    </div>
  </div>
</div>  

<div class="course-result-end">
  <div class="container">
    <div class="yoga-consult">
      <h3 class="title__wrapper title__light title__light--pink btn__wrapper--center-overlapped">Вы обретете профессию
<br>наставника Йоги:</h3>
      <ul>
        <li>Навыки Кундалини Крийя и Бхакти Йоги. </li>
        <li>Методики проведения занятий по йоге.</li>
        <li>Знание — как повышать уровень преподавания.  </li>
        <li>Заболевания и методы йога-терапии.</li>
        <li>Антаранга йога, Йога Садхана, </li>
        <li>Свара йога (Путь к гармонии человека и природы)</li>
        <li>Продвинутые асаны, Виньяса йога</li>
        <li>Чидакаш Дхарана, Медитации, Джапа.</li>
      </ul>
    </div>
    <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
    <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    <div class="btn__wrapper btn__wrapper--center"><a href="/pricelist/#faculty-iv-yoga" class="btn btn--full">Стоимость курса</a></div>
  </div>
</div>
<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

$('.programm-more').click(function(){
    $('.programm-hidden-data').slideToggle(300, function(){
      if ($(this).is(':hidden')) {
        $('.programm-more').html('ПРОГРАММА ПОДРОБНЕЕ');
      } else {
        $('.programm-more').html('Скрыть ПРОГРАММУ');
      }             
    });
    return false;
});

$('.schedule-more').click(function(){
    $('.day-schedule').slideToggle(300, function(){
      if ($(this).is(':hidden')) {
        $('.schedule-more').html('РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ');
      } else {
        $('.schedule-more').html('Скрыть РАСПОРЯДОК ДНЯ ОЧНЫХ СТУДЕНТОВ');
      }             
    });
    return false;
});

</script>

<?php get_footer(); ?>
