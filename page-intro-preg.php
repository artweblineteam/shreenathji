<?php
/*
Template Name: page-36 (intro-preg)
*/


 get_header(); ?>

<div class="banner banner-inner page-36">
  <div class="container">
    <div class="banner__logo">
      <div class="logo-logotype wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".1s"></div>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s">Подготовка, беременность и рождение</h2>
      <h1 class="banner__name wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".3s">ребёнка</h1>
      <h2 class="banner__subtext wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".5s" style="color: #00c0f3">Видео курс</h2>
    </div>
    <div class="banner__text wow animated fadeIn" data-wow-duration="1s" data-wow-delay=".8s">
      <div class="banner__text-subtitle">
        <span>Рождение и воспитание ребенка – высшая форма творчества человека. <br>Каждому родителю необходимо осознанно относиться к рождению детей. <br>Ведь традиции, связанные с беременностью и рождением, утеряны.</span>
      </div>    
    </div>
    <div class="btn__wrapper btn__wrapper--center">
      <a href="">Онлайн<br></a>
    </div>
  </div>
</div>

<div class="mentors mentors_intro">
  <div class="container">
    <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>ВЕДУЩИЙ</span></div>
     </div>
  </div>
</div>


<div class="course-info course-info_intro">
    <div class="container">
      <div class="teacher-intro">
          <div class="teacher-name">
            <h2>
              <div class="title__subtext">ЮЛИЯ РОЗЕНСОН</div>
            </h2>
            <span>Психолог, Акушер</span>
          </div>
          
          <span class="teacher-descr">Специализация Семейная, Перинатальная <br>и Нейропсихология. Опыт работы 25 лет. <br>Руководитель проекта родительской культуры «ДАР». <br>Автор проекта «Секреты гормонального здоровья»</span>
      </div>
      <div class="course-info__list grid grid-top">
        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">
            <div class="course-info__desc">
              <span class="course-info__user course-info__user-into">От того, как пройдут беременность и роды, во многом зависит здоровье и счастье вашего ребенка, а значит и всей семьи:</span>
              <div class="divider"></div>
              <p>Ведь как бы не помогала медицина в этом вопросе, вынашивать, рожать и растить малыша придется вам.</p>
              <p>А для этого нужны знания и навыки.</p>
              <p>Курс подготовки к рождению ребенка поможет вам в этом.</p>
            </div>
          </div>
        </div>

        <div class="col-1-2 col-sm-1-1">
          <div class="course-info__item">

            <div class="course-info__desc"><span class="course-info__user">Темы на курсе:</span>
              <div class="divider"></div>
              <ul>
                <li>Физиология беременности;</li>
                <li>Влияние беременности и рождения на судьбу;</li>
                <li>Начало родов;</li>
                <li>Периоды родов;</li>
                <li>Послеродовой период;</li>
                <li>Новорожденный, переходные состояния.</li>
              </ul>  
            </div>
          </div>
        </div>

      </div>
      <div class="btn__wrapper btn__wrapper--center with-border with-border__long">
        <div class="btn btn--full ">записатся на курс</div>
      </div>
      <div class="img__list grid grid-top">
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img28.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img29.png">
        </div>
        <div class="col-1-3 col-sm-1-3 col-xs-1-1">
          <img alt=""src="<?php bloginfo("template_directory"); ?>/assets/img/mentor_img30.png">
        </div>
        
      </div>
    </div>
</div>

<div class="programm-course-iii">
    <div class="container">
          <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>О курсе</span></div>
     </div>
     <div class="programm-course-iii__list grid grid-top">
       <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Форма Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Онлайн <br>Видео</span>
              </div>
            </div>
        <div class="col-1-4 col-sm-1-2 col-xs-1-1 block_height">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Объём Курса</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">6 тем — 7 видео <br>Всего 4 академ. часа</span>
              </div>
            </div>
            

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Сопровождение</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Контрольный тест <br>после каждой темы</span>
              </div>
            </div>

      <div class="col-1-4 col-sm-1-2 col-xs-1-1">
              <div class="programm-course-iii__feature programm-course-iii__feature-intro">
                <span class="programm-course-iii-title title__light--pink">Примечание</span>
                <div class="divider"></div>
                <span class="programm-course-iii-subtitle">Дополнительный <br>курс</span>
              </div>
            </div>    

     </div>
    </div>
</div>


<div class="course-price course-price_intro">
  <div class="container">
      <div class="title__wrapper wow  wow fadeIn animated" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeIn;">
            <div class="title__main with-border"><span>Условия</span></div>
            <div class="title__subtext">обучения</div>
     </div>
     <div class="table-inner table__intro">
      <div class="table-price">
            <!-- <table>
                  <tr class="table-header">
                        <td colspan="4">&nbsp;</td>
                  </tr>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">ЗПодготовка, беременность и рождение ребёнка</span></br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span></br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 4,5 часа, 6 тем</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td>Cтоимость</td>
                        <td>&nbsp;</td>
                        <td>8 000 <a href="/cart/?add-to-cart=1674">Купить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </table> -->
            <table>
                  <tbody>
                  <tr>
                        <th width="50%">
                              <span class="pink-table-subtitle">Подготовка, беременность и рождение ребёнка</span><br>
                              <span class="extra-light-subtitle">Онлайн курс</span>
                        </th>
                        <th colspan="3">
                              <span class="pink-table-subtitle">ВАРИАНТЫ ОБУЧЕНИЯ</span><br>
                        </th>
                  </tr>
                  <tr>
                        <td>Программа курса</td>
                        <td>«Лёгкий»</td>
                        <td>«Стандарт»</td>
                        <td>«Продвинутый»</td>
                  </tr>
                  <tr>
                        <td>Онлайн. Видео, 4,5 часа, 6 тем</td>
                        <td>-</td>
                        <td>да</td>
                        <td>-</td>
                  </tr>
                  <tr>
                        <td></td>
                        <td>&nbsp;</td>
                        <td><?php echo do_shortcode('[woocommerce_price id="1674"]');?> <a href="/cart/?add-to-cart=1674">купить</a></td>
                        <td>&nbsp;</td>
                  </tr>
            </tbody></table>
            <div class="small-note">
              <small>* Все цены действительны при курсе ЦБ 70-90 руб за $1</small>
            </div>
    </div>
  </div>
</div>


<div class="course-result-end course-result-end_intro">
  <div class="container">
    <div class="course-result-inner">
      <h3 class="title__wrapper title__light-no-bottom-margin title__light--pink">Более пяти тысяч студентов и учёных, людей искусства и банкиров, <br>владельцев малого бизнеса и мультимиллионеров, политиков <br> получили наши консультации и прослушали курсы лекций.</h3>
      <div class="divider"></div>
      <h3 class="title__wrapper title__light-no-bottom-margin">Присоединяйтесь к тем, кто уже осознал свои цели, обрел путь, достиг успеха,<br> исцелился от страшных недугов, создал прекрасные союзы, соприкоснулся<br> с бесконечным Источником энергии, силы, здоровья, идей, удачи!</h3>
    </div>
    <div class="btn__wrapper btn__wrapper--center btn__wrapper--center-overlapped"><div class="btn btn--full">Добро пожаловать</div></div>
  </div>
</div>


<script type="text/javascript">
  (function ($) {
$('.course-conditions-image-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3,
  arrows: false,
  dots: true
});
})(jQuery);

var boxes = document.getElementsByClassName('course-info__item');
var height = 0;
//Определяем максимальную высоту блока
for( var i = 0; i < boxes.length; i++ ){
    var current_height = boxes[i].offsetHeight;
    if(current_height > height) {
        height = current_height;
    }        
}
//Задаем максимальную высоту блока всем элементам
for( var i = 0; i < boxes.length; i++ ){
    boxes[i].style.height = height + 'px';       
}
</script>

<?php get_footer(); ?>
